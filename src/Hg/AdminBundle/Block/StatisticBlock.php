<?php
namespace Hg\AdminBundle\Block;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Loan;
use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Component\Templating\EngineInterface;

/**
 *
 * @author     Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class StatisticBlock extends BaseBlockService
{

    protected $em;

    public function __construct($name, EngineInterface $templating, EntityManager $em)
    {
        $this->name = $name;
        $this->templating = $templating;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'url' => false,
            'title' => 'Statistics for PlanB',
            'template' => 'AdminBundle:Block:statistic.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
//        $formMapper->add('settings', 'sonata_type_immutable_array', array(
//            'keys' => array(
//                array('url', 'url', array('required' => false)),
//                array('title', 'text', array('required' => false)),
//            )
//        ));
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        // nothing
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        // merge settings
        $settings = $blockContext->getSettings();

        $em = $this->em;
        $loanRepository = $em->getRepository("AppBundle:Loan");
        $notificationRepository = $em->getRepository("AppBundle:Notification");
        $lifebuoyRepository = $em->getRepository("AppBundle:Lifebuoy");

        $notificationsCount = $notificationRepository->countsByStatus();

        $draftCount = $loanRepository->countsByStatus(array(Loan::STATUS_AUCTION_APPROVED, Loan::STATUS_AUCTION_APPROVED, Loan::STATUS_AUCTION_REQUESTED, Loan::STATUS_DRAFT_REQUESTED, Loan::STATUS_AUCTION_SCORING));
        $draftCanceledCount = $loanRepository->countsByStatus(array(Loan::STATUS_AUCTION_CANCELED, Loan::STATUS_DRAFT_CANCELED));
        $auctionsCount = $loanRepository->countsByStatus(Loan::STATUS_AUCTION);
        $canceledCount = $loanRepository->countsByStatus(array(Loan::STATUS_CANCELED, Loan::STATUS_OVERDUE));
        $acceptedCount = $loanRepository->countsByStatus(Loan::STATUS_ACCEPTED, Loan::STATUS_REAUCTION);
        $payableCount = $loanRepository->countsByStatus(Loan::STATUS_PAYABLE);
        $loanCount = $loanRepository->countsByStatus(Loan::STATUS_LOAN);
        $repaidCount = $loanRepository->countsByStatus(Loan::STATUS_REPAID);

        $lifebuoy = $lifebuoyRepository->findLast()->getValue();

        $sumCollectedAmount = $loanRepository->sumCollectedAmount();

        return $this->renderResponse($blockContext->getTemplate(), array(
            'settings' => $settings,
            'notificationsCount' => $notificationsCount,
            'draftCount' => $draftCount,
            'draftCanceledCount' => $draftCanceledCount,
            'auctionsCount' => $auctionsCount,
            'canceledCount' => $canceledCount,
            'acceptedCount' => $acceptedCount,
            'payableCount' => $payableCount,
            'loanCount' => $loanCount,
            'repaidCount' => $repaidCount,
            'lifebuoy' => $lifebuoy,
            'sumCollectedAmount' => $sumCollectedAmount,
        ), $response);
    }
}
