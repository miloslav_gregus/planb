jQuery(function ($) {


    $(".sonata-ba-list-transaction-confirmation a").click(function () {
        var href = $(this).attr("href")
        var anchor = $(this)
        var objectId = $(this).closest("td").attr("objectId")
        var tr = $(this).closest("tr")
        var statusIcon = $("#status-icon-" + objectId)
        anchor.fadeOut().fadeIn().fadeOut().fadeIn()

        $.post(href,function (result) {
            anchor.stop().fadeOut()
            anchor.siblings().fadeOut()

            if(result.status == 0){
                statusIcon.attr("class", "icon-remove-sign")
            }
            if(result.status == 1){
                statusIcon.attr("class", "icon-ok")

            }
            statusIcon.fadeOut().fadeIn().fadeOut().fadeIn()

            console.log(tr)
            tr.find(".sonata-ba-list-notification-check a").click()

        }).fail(function () {
            alert("Hoops niečo sa posralo! skús neskôr...")
        })

        return false;
    })


    $(".sonata-ba-list-notification-check a").click(function () {
        var href = $(this).attr("href")
        var anchor = $(this)
        var tr = $(this).closest("tr")
        anchor.fadeOut().fadeIn().fadeOut().fadeIn()

        $.post(href,function (result) {
            tr.stop().fadeOut()

        }).fail(function () {
            alert("Hoops niečo sa posralo! skús neskôr...")
        })

        return false;
    })

    $(".file-confimration a.btn").click(function () {
        var href = $(this).attr("href")
        var anchor = $(this)
        var $parent = $(this).parent()
        anchor.fadeOut().fadeIn().fadeOut().fadeIn()



        $.post(href, function (result) {
            if(anchor.hasClass("approve")){
                $parent.find("[class*='icon-']").attr("class", "icon-envelope")
                $parent.find(".approve, .reject").fadeOut()
            }
            else if(anchor.hasClass("reject")){
                $parent.find("[class*='icon-']").attr("class", "icon-remove-sign")
                $parent.find(".approve, .reject, .delivered").fadeOut()
            }else{
                $parent.find("[class*='icon-']").attr("class", "icon-ok-sign")
                $parent.find(".approve, .reject, .delivered").fadeOut()
            }



        }).fail(function () {
            alert("Hoops niečo sa posralo! skús neskôr...")
        })

        return false;
    })

})
