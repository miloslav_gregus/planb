<?php

namespace Hg\AdminBundle\Controller;

use Hg\AdminBundle\Form\User\InvestorApproveType;
use Hg\AdminBundle\Form\User\UploadProfileFileType;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\HgEvents;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserCRUDController extends CRUDController
{

    public function showAction($id = null)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        /** @var User $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedHttpException();
        }

        $request = $this->getRequest();


        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->createForm(new InvestorApproveType(), $object);
        $form->setData($object);


        $isFormValid = true;
        $profileFile = new ProfileFile();
        $formUpload = $this->createForm(new UploadProfileFileType(), $profileFile);
        if ($request->isMethod('POST') && $request->request->has($formUpload->getName())) {

            $formUpload->submit($request);

            $isFormValid = $formUpload->isValid();

            if($isFormValid){
                $em = $this->getDoctrine()->getManager();
                $profile = $object->getProfile();

                $files = $em->getRepository("AppBundle:ProfileFile")->findBy(array(
                    "type" => $profileFile->getType(),
                    "status" => array(ProfileFile::STATUS_MISSING, ProfileFile::STATUS_DELETED),
                    "profile" => $profile
                ));
                foreach ($files AS $f) {
                    $em->remove($f);
                }

                $profileFile->setProfile($profile);
                $em->persist($profileFile);
                $em->flush();

                if($profileFile->getStatus() == ProfileFile::STATUS_DELIVERED){
                    $this->get("event_dispatcher")->dispatch(HgEvents::FILE_DELIVERED, new ProfileFileEvent($profile, $profileFile));
                }
                if($profileFile->getStatus() == ProfileFile::STATUS_MISSING){
                    $this->get("event_dispatcher")->dispatch(HgEvents::FILE_MISSING, new ProfileFileEvent($profile, $profileFile));
                }

                return $this->redirect($this->generateUrl("admin_hg_app_user_show", array("id" => $object->getId())) . "#upload-table");
            }


        }

        if ($request->isMethod('POST') && $request->request->has($form->getName())) {
            $form->submit($request);

            $isFormValid = $form->isValid();

            if($isFormValid){
                $object->setStatus(User::STATUS_DEFAULT);
                $this->admin->update($object);

                $this->get("event_dispatcher")->dispatch(HgEvents::INVESTOR_APPROVED, new UserEvent($object));
            }
        }

        // show an error message if the form failed validation
        if (!$isFormValid) {
            if (!$this->isXmlHttpRequest()) {
                $this->addFlash('sonata_flash_error', 'flash_edit_error');
            }
        }


        $view = $form->createView();
        $uploadView = $formUpload->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());
        $this->get('twig')->getExtension('form')->renderer->setTheme($uploadView, $this->admin->getFormTheme());


        return $this->render("AdminBundle:CRUD/User:show.html.twig", array(
            'action' => 'show',
            'form' => $view,
            'object' => $object,
            'formUpload' => $uploadView
        ));

    }

//    public function showAction($id = null)
//    {
//        $id = $this->get('request')->get($this->admin->getIdParameter());
//
//        $object = $this->admin->getObject($id);
//
//        if (!$object) {
//            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
//        }
//
//        if (false === $this->admin->isGranted('VIEW', $object)) {
//            throw new AccessDeniedHttpException();
//        }
//
//        $this->admin->setSubject($object);
//
//        return $this->render("AdminBundle:CRUD/Loan:show.html.twig", array(
//            'action'   => 'show',
//            'object'   => $object,
//            'elements' => $this->admin->getShow(),
//        ));
//    }


    public function redirectTo($object)
    {
        $url = false;

        if ($this->get('request')->get('btn_update_and_list')) {
            $url = $this->admin->generateUrl('list');
        }
        if ($this->get('request')->get('btn_cancel_scoring')) {
            $url = $this->admin->generateUrl('list');
        }

        if (!$url) {
            $url = $this->admin->generateObjectUrl('show', $object);
        }

        return new RedirectResponse($url);
    }

    public function profileFileApproveAction($id){
        $modelManager = $this->admin->getModelManager();

        /** @var ProfileFile $profileFile */
        $profileFile = $modelManager->find("AppBundle:ProfileFile", $id);
        $profileFile->setStatus(ProfileFile::STATUS_SUCCESS);
        $modelManager->update($profileFile);

        $event = new ProfileFileEvent($profileFile->getProfile(), $profileFile);
        $this->get("event_dispatcher")->dispatch(HgEvents::FILE_APPROVE, $event);

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }

    public function profileFileRejectAction($id){
        $modelManager = $this->admin->getModelManager();

        /** @var ProfileFile $profileFile */
        $profileFile = $modelManager->find("AppBundle:ProfileFile", $id);
        $profileFile->setStatus(ProfileFile::STATUS_DELETED);
        $modelManager->update($profileFile);

        $this->get("event_dispatcher")->dispatch(HgEvents::FILE_REJECT, new ProfileFileEvent($profileFile->getProfile(), $profileFile));

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }

    public function profileFileDeliveredAction($id){
        $modelManager = $this->admin->getModelManager();

        /** @var ProfileFile $profileFile */
        $profileFile = $modelManager->find("AppBundle:ProfileFile", $id);
        $profileFile->setStatus(ProfileFile::STATUS_DELIVERED);
        $modelManager->update($profileFile);

        $this->get("event_dispatcher")->dispatch(HgEvents::FILE_DELIVERED, new ProfileFileEvent($profileFile->getProfile(), $profileFile));

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }

}
