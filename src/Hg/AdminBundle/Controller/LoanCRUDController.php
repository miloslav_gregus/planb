<?php

namespace Hg\AdminBundle\Controller;

use Hg\AdminBundle\Form\Loan\AuctionApproveType;
use Hg\AdminBundle\Form\Loan\ScoringApproveType;
use Hg\AdminBundle\Form\Loan\UploadLoanFileType;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Lifebuoy;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\LoanFileEvent;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\HgEvents;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class LoanCRUDController extends CRUDController
{
    public function scoringAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        /** @var Loan $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedHttpException();
        }

        if($object->getStatus() != Loan::STATUS_DRAFT_REQUESTED ) {
            $url = $this->admin->generateObjectUrl('show', $object);
            return new RedirectResponse($url);
        }


        /** @var $form \Symfony\Component\Form\Form */
        //$form = $this->createForm(new ScoringApproveType(), $object);
        $form = $this->admin->getForm();

        $isFormValid = true;
        $form->setData($object);


        if ($this->getRestMethod() == 'POST') {

            $request = $this->get('request');
            if($request->get('btn_cancel_scoring')){
                $object->setStatus(Loan::STATUS_DRAFT_CANCELED);
                $object->getUser()->setStatus(User::STATUS_DRAFT_CANCELED)->addAuctionsPending(-1);
                $this->admin->update($object);

                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_DRAFT_CANCELED, new LoanEvent($object));

                return $this->redirectTo($object);
            }

            $form->submit($request);

            $isFormValid = $form->isValid();
            if($request->get('btn_update_and_edit') || $request->get('btn_update_and_list')){
                if ($isFormValid) {

                    $object->setStatus(Loan::STATUS_DRAFT_APPROVED);
                    $object->getUser()->setStatus(User::STATUS_DRAFT_APPROVED);
                    $this->admin->update($object);

                    $this->addFlash('sonata_flash_success', 'flash_edit_success');

                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_DRAFT_APPROVED, new LoanEvent($object));
                    // redirect to edit mode
                    return $this->redirectTo($object);
                }
            }

        }


        // show an error message if the form failed validation
        if (!$isFormValid) {
            if (!$this->isXmlHttpRequest()) {
                $this->addFlash('sonata_flash_error', 'flash_edit_error');
            }
        }

        $view = $form->createView();


        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());


        return $this->render("AdminBundle:CRUD/Loan:scoring_edit.html.twig", array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ));

    }
    public function scoring2Action()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        /** @var Loan $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedHttpException();
        }

        if($object->getStatus() != Loan::STATUS_AUCTION_REQUESTED ) {
            $url = $this->admin->generateObjectUrl('show', $object);
            return new RedirectResponse($url);
        }


        /** @var $form \Symfony\Component\Form\Form */
        //$form = $this->getScoringForm($this->admin->getForm(), $object);
        $form = $this->createForm(new ScoringApproveType(), $object);


        $isFormValid = true;
        $form->setData($object);


        if ($this->getRestMethod() == 'POST') {

            $request = $this->get('request');
            if($request->get('btn_cancel_scoring')){
                $object->setStatus(Loan::STATUS_DRAFT_CANCELED);
                $object->getUser()->setStatus(User::STATUS_DRAFT_CANCELED)->addAuctionsPending(-1);
                $this->admin->update($object);

                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_DRAFT_CANCELED, new LoanEvent($object));

                return $this->redirectTo($object);
            }

            $form->submit($request);

            $isFormValid = $form->isValid();
            if($request->get('btn_update_and_edit') || $request->get('btn_update_and_list')){
                if ($isFormValid) {

                    $object->setStatus(Loan::STATUS_AUCTION_SCORING);
                    $object->getUser()->setStatus(User::STATUS_AUCTION_SCORING);


                    $em = $this->getDoctrine()->getManager();


                    $files = $this->generatePdfFiles($object);


                    $em->flush();

                    $this->addFlash('sonata_flash_success', 'flash_edit_success');
                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_AUCTION_SCORING, new LoanEvent($object, $files));
                    // redirect to edit mode
                    return $this->redirectTo($object);
                }
            }

        }


        // show an error message if the form failed validation
        if (!$isFormValid) {
            if (!$this->isXmlHttpRequest()) {
                $this->addFlash('sonata_flash_error', 'flash_edit_error');
            }
        }

        $view = $form->createView();


        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());


        return $this->render("AdminBundle:CRUD/Loan:scoring_2_edit.html.twig", array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ));

    }



    public function auctionApproveAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedHttpException();
        }

        if($object->getStatus() != Loan::STATUS_AUCTION_SCORING ) {
            $url = $this->admin->generateObjectUrl('show', $object);
            return new RedirectResponse($url);
        }

        $request = $this->get('request');

        /** @var $form \Symfony\Component\Form\Form */
        //$form = $this->getScoringForm($this->admin->getForm(), $object);
        $form = $this->createForm(new AuctionApproveType(), $object);


        $loanFile = new LoanFile();
        $formUpload = $this->createForm(new UploadLoanFileType(), $loanFile);

        $form->setData($object);

        $isFormValid = true;
        if ($this->getRestMethod() == 'POST') {

            if($request->get('btn_cancel_scoring')){
                $object->setStatus(Loan::STATUS_AUCTION_CANCELED);
                $object->getUser()->setStatus(User::STATUS_AUCTION_APPROVED)->addAuctionsPending(-1);
                $this->admin->update($object);

                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_AUCTION_CANCELED, new LoanEvent($object));

                return $this->redirectTo($object);
            }

            $form->submit($request);

            $isFormValid = $form->isValid();
            if($request->get('btn_update_and_edit') || $request->get('btn_update_and_list')){
                if ($isFormValid) {

                    $object->setStatus(Loan::STATUS_AUCTION_APPROVED);
                    $object->getUser()->setStatus(User::STATUS_AUCTION_APPROVED);
                    $this->admin->update($object);

                    $this->addFlash('sonata_flash_success', 'flash_edit_success');

                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_AUCTION_APPROVED, new LoanEvent($object));
                    // redirect to edit mode
                    return $this->redirectTo($object);
                }
            }

        }


        // show an error message if the form failed validation
        if (!$isFormValid) {
            if (!$this->isXmlHttpRequest()) {
                $this->addFlash('sonata_flash_error', 'flash_edit_error');
            }
        }

        if ($request->isMethod('POST') && $request->request->has($formUpload->getName())) {

            $formUpload->submit($request);

            $isFormValid = $formUpload->isValid();

            if($isFormValid){
                $em = $this->getDoctrine()->getManager();

                $files = $em->getRepository("AppBundle:LoanFile")->findBy(array(
                    "title" => $loanFile->getTitle(),
                    "status" => array(LoanFile::STATUS_MISSING, LoanFile::STATUS_DELETED),
                    "loan" => $object
                ));
                foreach($files AS $f){
                    $em->remove($f);
                }

                $loanFile->setLoan($object);
                $em->persist($loanFile);
                $em->flush();

                if($loanFile->getStatus() == LoanFile::STATUS_DELIVERED){
                    $this->get("event_dispatcher")->dispatch(HgEvents::FILE_DELIVERED, new LoanFileEvent($object, $loanFile));
                }
                if($loanFile->getStatus() == LoanFile::STATUS_MISSING){
                    $this->get("event_dispatcher")->dispatch(HgEvents::FILE_MISSING, new LoanFileEvent($object, $loanFile));
                }

                return $this->redirect($this->generateUrl("admin_hg_app_loan_auction_approve", array("id" => $object->getId())) . "#upload-table");
            }
        }

        $view = $form->createView();
        $uploadView = $formUpload->createView();


        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());
        $this->get('twig')->getExtension('form')->renderer->setTheme($uploadView, $this->admin->getFormTheme());


        return $this->render("AdminBundle:CRUD/Loan:auction_approve_edit.html.twig", array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
            'formUpload' => $uploadView
        ));

    }


    public function loanApproveAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedHttpException();
        }


        if($object->getStatus() != Loan::STATUS_ACCEPTED ) {
            $url = $this->admin->generateObjectUrl('show', $object);
            return new RedirectResponse($url);
        }


        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        $isFormValid = true;

        if ($this->getRestMethod() == 'POST') {

            $request = $this->get('request');
            if($request->get('btn_cancel_scoring')){
                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_CANCELED, new LoanEvent($object));

                return $this->redirectTo($object);
            }

            if($request->get('btn_update_and_edit') || $request->get('btn_update_and_list')){
                if ($isFormValid) {

                    $object->setStatus(Loan::STATUS_PAYABLE);
                    $object->getUser()->setStatus(User::STATUS_AUCTION_PAYABLE);
                    $this->admin->update($object);

                    $this->addFlash('sonata_flash_success', 'flash_edit_success');

                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_PAYABLE, new LoanEvent($object));
                    // redirect to edit mode
                    return $this->redirectTo($object);
                }
            }

        }


        // show an error message if the form failed validation
        if (!$isFormValid) {
            if (!$this->isXmlHttpRequest()) {
                $this->addFlash('sonata_flash_error', 'flash_edit_error');
            }
        }


        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());


        return $this->render("AdminBundle:CRUD/Loan:loan_approve_edit.html.twig", array(
            'action' => 'edit',
            'form'   => $view,
            'object' => $object,
        ));
    }



    public function loanPayAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        /** @var Loan $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedHttpException();
        }


        if($object->getStatus() != Loan::STATUS_PAYABLE ) {
            $url = $this->admin->generateObjectUrl('show', $object);
            return new RedirectResponse($url);
        }


        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        $isFormValid = true;

        if ($this->getRestMethod() == 'POST') {

            $request = $this->get('request');
            if($request->get('btn_cancel_scoring')){
                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_CANCELED, new LoanEvent($object));

                return $this->redirectTo($object);
            }

            if($request->get('btn_update_and_edit') || $request->get('btn_update_and_list')){
                if ($isFormValid) {
                    $em = $this->getDoctrine()->getManager();
                    $admin = $em->getRepository("AppBundle:User")->findAdmin();

                    $object->setStatus(Loan::STATUS_LOAN);
                    $object->getUser()->setStatus(User::STATUS_DEFAULT);
                    $object->getUser()->addLoansActive(+1);

                    foreach($object->getInvestments() AS $investment){
                        /** @var Investment $investment */
                        if($investment->isStatus($investment::STATUS_PREPARED)){
                            $transaction = new Transaction(Transaction::TYPE_LOAN);
                            $transaction->setAmount($investment->getAmount());
                            $transaction->setVariableNumber($investment->getUser()->getFormattedId());
                            $transaction->setSpecificNumber($object->getFormattedId());
                            $transaction->setLoan($object);
                            $transaction->setStatus($transaction::STATUS_SUCCESS);
                            $transaction->setFromUser($investment->getUser());
                            $transaction->setToUser($object->getUser());
                            $em->persist($transaction);

                            $investment->setStatus($investment::STATUS_PAID);
                        }
                    }

                    // bolo treba ako sucet vsetkych, uz sa nepouziva
//                    $transaction = new Transaction(Transaction::TYPE_LOAN);
//                    $transaction->setAmount($object->getAuctionCollectedAmount());
//                    $transaction->setVariableNumber($object->getUser()->getFormattedId());
//                    $transaction->setSpecificNumber($object->getFormattedId());
//                    $transaction->setLoan($object);
//                    $transaction->setFromUser($admin);
//                    $transaction->setStatus($transaction::STATUS_SUCCESS);
//                    $transaction->setToUser($object->getUser());
//                    $em->persist($transaction);


                    $transaction = new Transaction(Transaction::TYPE_FEE);
                    $transaction->setAmount($object->getFeeAmount());
                    $transaction->setVariableNumber($object->getFormattedId());
                    $transaction->setLoan($object);
                    $transaction->setFromUser($object->getUser());
                    $transaction->setStatus($transaction::STATUS_SUCCESS);
                    $transaction->setToUser($admin);
                    $em->persist($transaction);

                    $calculations = $this->get("hg.services.planb.calculations");
                    $lifebuoyAdminFee = $calculations->recalculateAdminFee($object, array(Investment::STATUS_PREPARED, Investment::STATUS_PAID));
                    $lifebuoyFee = $calculations->recalculateLifebuoyFee($object, array(Investment::STATUS_PREPARED, Investment::STATUS_PAID));


                    $lifebuoy = $em->getRepository("AppBundle:Lifebuoy")->findLast();
                    $lifebuoyNew = new Lifebuoy();
                    $lifebuoyNew->setValue($lifebuoy->getValue() + $lifebuoyFee + $lifebuoyAdminFee);
                    $lifebuoyNew->setUser($this->getUser());
                    $em->persist($lifebuoyNew);

                    $transaction = new Transaction(Transaction::TYPE_FEE_LIFEBUOY);
                    $transaction->setAmount($lifebuoyFee);
                    $transaction->setVariableNumber($object->getFormattedId());
                    $transaction->setLoan($object);
                    $transaction->setFromUser($object->getUser());
                    $transaction->setStatus($transaction::STATUS_SUCCESS);
                    $transaction->setToUser($admin);
                    $em->persist($transaction);


                    $em->flush();
                    $this->addFlash('sonata_flash_success', 'flash_edit_success');
                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_LOAN, new LoanEvent($object));
                    // redirect to edit mode
                    return $this->redirectTo($object);
                }
            }

        }


        // show an error message if the form failed validation
        if (!$isFormValid) {
            if (!$this->isXmlHttpRequest()) {
                $this->addFlash('sonata_flash_error', 'flash_edit_error');
            }
        }


        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());


        return $this->render("AdminBundle:CRUD/Loan:loan_pay_edit.html.twig", array(
            'action' => 'edit',
            'form'   => $view,
            'object' => $object,
        ));
    }


    public function showAction($id = null)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('VIEW', $object)) {
            throw new AccessDeniedHttpException();
        }

        $this->admin->setSubject($object);

        return $this->render("AdminBundle:CRUD/Loan:show.html.twig", array(
            'action'   => 'show',
            'object'   => $object,
            'elements' => $this->admin->getShow(),
        ));
    }


    public function redirectTo($object)
    {
        $url = false;

        if ($this->get('request')->get('btn_update_and_list')) {
            $url = $this->admin->generateUrl('list');
        }
        if ($this->get('request')->get('btn_cancel_scoring')) {
            $url = $this->admin->generateUrl('list');
        }

        if (!$url) {
            $url = $this->admin->generateObjectUrl('show', $object);
        }

        return new RedirectResponse($url);
    }



    public function loanFileApproveAction($id){
        $modelManager = $this->admin->getModelManager();

        /** @var LoanFile $loanFile */
        $loanFile = $modelManager->find("AppBundle:LoanFile", $id);
        $loanFile->setStatus(LoanFile::STATUS_SUCCESS);
        $modelManager->update($loanFile);

        $this->get("event_dispatcher")->dispatch(HgEvents::FILE_APPROVE, new LoanFileEvent($loanFile->getLoan(), $loanFile));

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }

    public function loanFileRejectAction($id){
        $modelManager = $this->admin->getModelManager();

        /** @var LoanFile $loanFile */
        $loanFile = $modelManager->find("AppBundle:LoanFile", $id);
        $loanFile->setStatus(LoanFile::STATUS_DELETED);
        $modelManager->update($loanFile);

        $this->get("event_dispatcher")->dispatch(HgEvents::FILE_REJECT, new LoanFileEvent($loanFile->getLoan(), $loanFile));

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }
    public function loanFileDeliveredAction($id){
        $modelManager = $this->admin->getModelManager();

        /** @var LoanFile $loanFile */
        $loanFile = $modelManager->find("AppBundle:LoanFile", $id);
        $loanFile->setStatus(LoanFile::STATUS_DELIVERED);
        $modelManager->update($loanFile);

        $this->get("event_dispatcher")->dispatch(HgEvents::FILE_DELIVERED, new LoanFileEvent($loanFile->getLoan(), $loanFile));

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }


    private function generatePdfFiles(Loan $loan){

        $em = $this->getDoctrine()->getManager();
        /**
         * Generation of pdf file
         */
        $documentsGenerator = $this->get("hg.services.planb.documents_generator");


        $files = array();


        $file = $documentsGenerator->generate("terms-conditions", array(
            "user" => $loan->getUser()
        ));
        $loanFile = new LoanFile();
        $loanFile->setFileContent($file, $this->get("translator")->trans("file_type.terms_conditions"));
        $loanFile->setType($loanFile::TYPE_TERMS_CONDITIONS);
        $loanFile->setStatus($loanFile::STATUS_MISSING);
        $loanFile->setLoan($loan);
        $em->persist($loanFile);
        $files[] = $loanFile;



        $file = $documentsGenerator->generate("data-processing-customer", array(
            "user" => $loan->getUser()
        ));
        $loanFile = new LoanFile();
        $loanFile->setFileContent($file, $this->get("translator")->trans("file_type.data_processing"));
        $loanFile->setType($loanFile::TYPE_DATA_PROCESSING);
        $loanFile->setStatus($loanFile::STATUS_MISSING);
        $loanFile->setLoan($loan);
        $em->persist($loanFile);
        $files[] = $loanFile;


        $file = $documentsGenerator->generate("loan-request", array(
            "loan" => $loan,
            "user" => $loan->getUser()
        ));
        $loanFile = new LoanFile();
        $loanFile->setFileContent($file, $this->get("translator")->trans("file_type.loan_request"));
        $loanFile->setType($loanFile::TYPE_LOAN_REQUEST);
        $loanFile->setStatus($loanFile::STATUS_MISSING);
        $loanFile->setLoan($loan);
        $em->persist($loanFile);
        $files[] = $loanFile;


        $generate = array(
            LoanFile::TYPE_ID_CARD,
            LoanFile::TYPE_BANK_STATEMENT,
            LoanFile::TYPE_INCOME_DOCUMENT,
            LoanFile::TYPE_PERSONAL_DATA,
        );

        if($loan->getProfile()->getOtherIdType() == 1){
            $generate[] = LoanFile::TYPE_PASSPORT;
        }
        if($loan->getProfile()->getOtherIdType() == 2){
            $generate[] = LoanFile::TYPE_INSURANCE_CARD;
        }
        if($loan->getProfile()->getOtherIdType() == 3){
            $generate[] = LoanFile::TYPE_DRIVER_LICENCE;
        }

        foreach ($generate AS $type) {
            $file = new LoanFile();
            $em->persist($file);
            $file->setLoan($loan);
            $file->setStatus(LoanFile::STATUS_MISSING);
            $file->setType($type);
        }

        $file = new LoanFile();
        $em->persist($file);
        $file->setLoan($loan);
        $file->setStatus(LoanFile::STATUS_MISSING);
        $file->setType(LoanFile::TYPE_LOAN_REGISTER);
        $file->setMandatory(false);

        if(count($loan->getAdditionalObligations())){
            $file = new LoanFile();
            $em->persist($file);
            $file->setLoan($loan);
            $file->setStatus(LoanFile::STATUS_MISSING);
            $file->setType(LoanFile::TYPE_OBLIGATION_DOCUMENT);
        }


        return $files;
    }
}
