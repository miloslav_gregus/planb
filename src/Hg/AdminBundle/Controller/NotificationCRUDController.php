<?php

namespace Hg\AdminBundle\Controller;

use Hg\AppBundle\Entity\Notification;
use Hg\AppBundle\HgEvents;
use Sonata\AdminBundle\Controller\CRUDController;
use Hg\AppBundle\Entity\Transaction;
use Symfony\Component\HttpFoundation\JsonResponse;


class NotificationCRUDController extends CRUDController
{
    public function doneAction($id){
        $modelManager = $this->admin->getModelManager();
        /** @var Notification $notification */
        $notification = $modelManager->find($this->admin->getClass(), $id);
        $notification->setStatus(Notification::STATUS_DONE);
        $modelManager->update($notification);

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 1
        ));
    }


}
