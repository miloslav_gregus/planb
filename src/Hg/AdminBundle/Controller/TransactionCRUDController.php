<?php

namespace Hg\AdminBundle\Controller;

use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\HgEvents;
use Sonata\AdminBundle\Controller\CRUDController;
use Hg\AppBundle\Entity\Transaction;
use Symfony\Component\HttpFoundation\JsonResponse;


class TransactionCRUDController extends CRUDController
{
    public function acceptAction($id){
        $response = new JsonResponse();
        /** @var Transaction $transaction */

        $em = $this->getDoctrine()->getManager();

        $transaction = $em->getRepository("AppBundle:Transaction")->find($id);

        if(!$transaction && !$transaction->isType(array(
                Transaction::STATUS_FUTURE,
                Transaction::STATUS_PENDING,
                Transaction::STATUS_OVERDUE,
            ))){
            return $response->setData(array(
                'status' => 0
            ));
        }

        /*
         * repayments
         */
        if($transaction && $transaction->isType(Transaction::TYPE_REPAYMENT)){
            $dependentTransactions = $em->getRepository("AppBundle:Transaction")
                ->findDependentRepayments($transaction->getVariableNumber(), $transaction->getSpecificNumber(), array(
                    Transaction::STATUS_FUTURE,
                    Transaction::STATUS_PENDING,
                    Transaction::STATUS_OVERDUE,
                ));
            foreach($dependentTransactions AS $dependentTransaction){
                $dependentTransaction->setStatus(Transaction::STATUS_SUCCESS);
                $this->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_ACCEPTED, new TransactionEvent($dependentTransaction));
            }
        }

        $transaction->setStatus(Transaction::STATUS_SUCCESS);
        $em->flush();

        $this->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_ACCEPTED, new TransactionEvent($transaction));
        return $response->setData(array(
            'status' => 1
        ));
    }

    public function rejectAction($id){
        $modelManager = $this->admin->getModelManager();
        /** @var Transaction $transaction */
        $transaction = $modelManager->find($this->admin->getClass(), $id);
        $transaction->setStatus(Transaction::STATUS_CANCELED);
        $modelManager->update($transaction);

        $this->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_REJECTED, new TransactionEvent($transaction));

        $response = new JsonResponse();
        return $response->setData(array(
            'status' => 0
        ));
    }
}
