<?php

namespace Hg\AdminBundle\Controller;


use Hg\AdminBundle\Form\Loan\LifebuoyType;
use Hg\AppBundle\Entity\Lifebuoy;
use Sonata\AdminBundle\Controller\CRUDController;


class LifebuoyCRUDController extends CRUDController
{

    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $lifebuoy = $em->getRepository("AppBundle:Lifebuoy")->findLast();

        $request = $this->get("request");



        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->createForm(new LifebuoyType(), $lifebuoy);


        if ($this->getRestMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {

                $em->detach($lifebuoy);

                $lifebuoyNew = new Lifebuoy();
                $lifebuoyNew->setValue($form->get("value")->getData());
                $lifebuoyNew->setUser($this->getUser());
                $lifebuoyNew->setId(null);
                $em->persist($lifebuoyNew);
                $em->flush();

                $this->addFlash('sonata_flash_success', 'flash_edit_success');
            }
        }

        return $this->render("AdminBundle:CRUD:base_lifebuoy.html.twig", array(
            'action' => 'list',
            'form' => $form->createView()
        ));
    }
}
