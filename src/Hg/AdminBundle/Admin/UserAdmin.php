<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PayteR
 * Date: 2.9.2013
 * Time: 5:31
 * To change this template use File | Settings | File Templates.
 */

namespace Hg\AdminBundle\Admin;


use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use Hg\AppBundle\HgEvents;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Entity\Post;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserAdmin extends Admin
{
    protected $baseRoutePattern = 'users';

    protected $formOptions = array(
        'validation_groups' => 'Profile'
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', 'show'));
        $collection->add('profile_file_approve', $this->getRouterIdParameter() . '/profile-file-approve');
        $collection->add('profile_file_reject', $this->getRouterIdParameter() . '/profile-file-reject');
        $collection->add('profile_file_delivered', $this->getRouterIdParameter() . '/profile-file-delivered');
    }

    public function getFilterParameters()
    {
        $this->datagridValues['type'] = array(
            "type" => "",
            "value" => User::TYPE_INVESTOR
        );
        $this->datagridValues['_sort_order'] = "DESC";
        $this->datagridValues['_sort_by'] = "createdAt";
        return parent::getFilterParameters();
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere($query->getRootAlias() . '.id NOT IN (:ids)');
        $query->setParameter('ids', 1);
        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('username')
            ->add('type')
            ->add('email')
            ->add('enabled', null, array(
                'editable' => true,
                'template' => 'AdminBundle:CRUD:list_boolean.html.twig'
            ))
            ->add('locked', null, array(
                'editable' => true,
                'template' => 'AdminBundle:CRUD:list_boolean.html.twig'
            ))
            ->add('createdAt')
            ->add('_action', 'actions', array(
                "template" => 'AdminBundle:CRUD:list_user_actions.html.twig'
            ));

//        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
//            $listMapper
//                ->add('impersonating', 'string', array('template' => 'AdminBundle:Admin:Field/impersonating.html.twig'));
//        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('username')
            ->add('email')
            ->add('type', 'doctrine_orm_choice', array(), 'choice', array(
                'label' => 'loan.filter.status',
                "choices" => array(
                    User::TYPE_CUSTOMER => User::TYPE_CUSTOMER,
                    User::TYPE_INVESTOR => User::TYPE_INVESTOR,
                ),
            ))
            ->add('locked');
    }


    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var User $user */
        $user = $this->getSubject();

        $formMapper
            ->add('username', null, array(
                'disabled' => true,
            ))
            ->add('email', null, array());

        if ($user->getProfile()) {
            $formMapper
                ->add('profile.title', null, array(
                    "label" => "profile.title",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.name', null, array(
                    "label" => "profile.name",
                    "translation_domain" => 'messages',
                ))
                ->add('profile.surname', null, array(
                    "label" => "profile.surname",
                    "translation_domain" => 'messages',
                ))
                ->add('profile.birthSurname', null, array(
                    "label" => "profile.birth_surname",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.gender', 'choice', array(
                    "label" => "profile.gender",
                    'empty_value' => "empty_value",
                    "choices" => array(
                        1 => "profile.gender_1",
                        2 => "profile.gender_2",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.familyStatus', 'choice', array(
                    "label" => "profile.family_status",
                    'empty_value' => "empty_value",
                    "choices" => array(
                        1 => "profile.family_status_1",
                        2 => "profile.family_status_2",
                        3 => "profile.family_status_3",
                        4 => "profile.family_status_4",
                        5 => "profile.family_status_5",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.education', 'choice', array(
                    'empty_value' => "empty_value",
                    "label" => "profile.education",
                    "choices" => array(
                        1 => "profile.education_1",
                        2 => "profile.education_2",
                        3 => "profile.education_3",
                        4 => "profile.education_4",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.student', 'checkbox', array(
                    "label" => "profile.is_student",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.birthDate', "birthday", array(
                    "label" => "profile.birth_date",
                    "years" => range(date("Y") - 18, date("Y") - 80),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.personalId', null, array(
                    "label" => "profile.personal_id",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.idCard', null, array(
                    "label" => "profile.id_card",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.otherIdType', 'choice', array(
                    "label" => "profile.additional_other_id",
                    "empty_value" => "profile.additional_other_id_empty",
                    "choices" => array(
                        1 => "profile.additional_other_id_1",
                        2 => "profile.additional_other_id_2",
                        3 => "profile.additional_other_id_3",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.otherIdNumber', null, array(
                    "label" => "profile.additional_other_id_number",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressStreet', null, array(
                    "label" => "profile.address_street",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressNumber', null, array(
                    "label" => "profile.address_number",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressCity', null, array(
                    "label" => "profile.address_city",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressZip', null, array(
                    "label" => "profile.address_zip",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressAccomodationType', 'choice', array(
                    "label" => "profile.address_accomodation_type",
                    'empty_value' => "empty_value",
                    "choices" => array(
                        1 => "profile.address_accomodation_type_1",
                        2 => "profile.address_accomodation_type_2",
                        3 => "profile.address_accomodation_type_3",
                        4 => "profile.address_accomodation_type_4",
                        5 => "profile.address_accomodation_type_5",
                        6 => "profile.address_accomodation_type_6",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactNotSame', 'checkbox', array(
                    "label" => "profile.address_contact_not_same",
                    "translation_domain" => 'messages',
                    "required" => false
                ))
                ->add('profile.addressCountry', null, array(
                    "label" => "profile.address_country",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressRegion', 'choice', array(
                    "label" => "profile.address_region",
                    'empty_value' => "profile.address_region_empty",
                    "choices" => array(
                        1 => "profile.address_region_1",
                        2 => "profile.address_region_2",
                        3 => "profile.address_region_3",
                        4 => "profile.address_region_4",
                        5 => "profile.address_region_5",
                        6 => "profile.address_region_6",
                        7 => "profile.address_region_7",
                        8 => "profile.address_region_8",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactStreet', null, array(
                    "label" => "profile.address_street",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactNumber', null, array(
                    "label" => "profile.address_number",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactCity', null, array(
                    "label" => "profile.address_city",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactZip', null, array(
                    "label" => "profile.address_zip",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactCountry', null, array(
                    "label" => "profile.address_country",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressContactRegion', 'choice', array(
                    "label" => "profile.address_region",
                    'empty_value' => "profile.address_region_empty",
                    "choices" => array(
                        1 => "profile.address_region_1",
                        2 => "profile.address_region_2",
                        3 => "profile.address_region_3",
                        4 => "profile.address_region_4",
                        5 => "profile.address_region_5",
                        6 => "profile.address_region_6",
                        7 => "profile.address_region_7",
                        8 => "profile.address_region_8",
                    ),
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressTel', null, array(
                    "label" => "profile.address_tel",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.addressMobil', null, array(
                    "label" => "profile.address_mobil",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.bankPrefix', null, array(
                    "label" => "profile.additional_bank_prefix",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.bankNumber', null, array(
                    "label" => "profile.additional_bank_number",
                    "translation_domain" => 'messages',
                    "required" => false,
                ))
                ->add('profile.bankCode', null, array(
                    "label" => "profile.additional_bank_code",
                    "translation_domain" => 'messages',
                    "required" => false,
                ));
        }

    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }


}