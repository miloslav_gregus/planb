<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PayteR
 * Date: 2.9.2013
 * Time: 5:31
 * To change this template use File | Settings | File Templates.
 */

namespace Hg\AdminBundle\Admin;


use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\Transaction;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TransactionAdmin extends Admin
{
    protected $baseRoutePattern = 'transactions';
    protected $translationDomain = 'messages';
    protected $maxPerPage = 25;
    protected $classnameLabel = 'Transactions';


    public function configure()
    {
        parent::configure();
        $this->datagridValues['status'] = array(
            "type" => "",
            "value" => array(
                Transaction::STATUS_OVERDUE,
                Transaction::STATUS_PENDING,
            )
        );
        //var_dump($this->datagridValues);exit;
        $this->datagridValues['_sort_by'] = 'createdAt';
        $this->datagridValues['_sort_order'] = 'DESC';
        return parent::getFilterParameters();
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere($query->getRootAlias() . ".type NOT IN(:type)");
        $query->setParameter('type', array(
            Transaction::TYPE_INTEREST,
            Transaction::TYPE_PRINCIPAL,
        )); // eg get from
        return $query;
    }

    //protected $supportsPreviewMode = true;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit'));
        $collection->add('accept', $this->getRouterIdParameter() . '/accept');
        $collection->add('reject', $this->getRouterIdParameter() . '/reject');
    }

    public function getTemplate($name)
    {
        return parent::getTemplate($name);
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setStatus(1);

        return $instance;
    }


    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user')
            ->add('user')
            ->add('user')
        ;
    }

    //Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('variableNumber', null, array(
                'label' => 'transactions.filter.variable_number',
            ))
            ->add('status', 'doctrine_orm_choice', array(), 'choice',array(
                'label' => 'transactions.filter.status',
                "multiple" => true,
                "choices" => array(
                    Transaction::STATUS_OVERDUE => "transactions.status." . Transaction::STATUS_OVERDUE,
                    Transaction::STATUS_FUTURE => "transactions.status." . Transaction::STATUS_FUTURE,
                    Transaction::STATUS_PENDING => "transactions.status." . Transaction::STATUS_PENDING,
                    Transaction::STATUS_CANCELED => "transactions.status." . Transaction::STATUS_CANCELED,
                    Transaction::STATUS_SUCCESS => "transactions.status." . Transaction::STATUS_SUCCESS,
                ),
                "data" => array(
                    Loan::STATUS_DRAFT_REQUESTED
                )
            ))
            ->add('type', 'doctrine_orm_choice', array(), 'choice',array(
                'label' => 'transactions.filter.type',
                "multiple" => true,
                "choices" => array(
                    Transaction::TYPE_DEPOSIT => "transactions.type." . Transaction::TYPE_DEPOSIT,
                    Transaction::TYPE_WITHDRAW => "transactions.type." . Transaction::TYPE_WITHDRAW,
                    Transaction::TYPE_INVESTMENT => "transactions.type." . Transaction::TYPE_INVESTMENT,
                    Transaction::TYPE_LOAN => "transactions.type." . Transaction::TYPE_LOAN,
                    Transaction::TYPE_REPAYMENT => "transactions.type." . Transaction::TYPE_REPAYMENT,
                    Transaction::TYPE_PRINCIPAL => "transactions.type." . Transaction::TYPE_PRINCIPAL,
                    Transaction::TYPE_INTEREST => "transactions.type." . Transaction::TYPE_INTEREST,
                    Transaction::TYPE_FEE => "transactions.type." . Transaction::TYPE_FEE
                ),
                "data" => array(
                    Loan::STATUS_DRAFT_REQUESTED
                )
            ))
            ->add('fromUser', null, array(
                'label' => 'transactions.filter.from_user',
                "multiple" => true,
            ))
            ->add('fromType', 'doctrine_orm_choice', array(), 'choice',array(
                'label' => 'transactions.filter.from_type',
                "multiple" => true,
                "choices" => array(
                    Transaction::ACCOUNT_TYPE_PLANB => "transactions.account_type." . Transaction::ACCOUNT_TYPE_PLANB,
                    Transaction::ACCOUNT_TYPE_INVESTOR_REAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_INVESTOR_REAL,
                    Transaction::ACCOUNT_TYPE_INVESTOR_VIRTUAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_INVESTOR_VIRTUAL,
                    Transaction::ACCOUNT_TYPE_CUSTOMER_REAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_CUSTOMER_REAL,
                    Transaction::ACCOUNT_TYPE_LOAN_VIRTUAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_LOAN_VIRTUAL,
                ),
                "data" => array(
                    Loan::STATUS_DRAFT_REQUESTED
                )
            ))
            ->add('toUser', null, array(
                'label' => 'transactions.filter.to_user',
                "multiple" => true,
            ))
            ->add('toType', 'doctrine_orm_choice', array(), 'choice',array(
                'label' => 'transactions.filter.to_type',
                "multiple" => true,
                "choices" => array(
                    Transaction::ACCOUNT_TYPE_PLANB => "transactions.account_type." . Transaction::ACCOUNT_TYPE_PLANB,
                    Transaction::ACCOUNT_TYPE_INVESTOR_REAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_INVESTOR_REAL,
                    Transaction::ACCOUNT_TYPE_INVESTOR_VIRTUAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_INVESTOR_VIRTUAL,
                    Transaction::ACCOUNT_TYPE_CUSTOMER_REAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_CUSTOMER_REAL,
                    Transaction::ACCOUNT_TYPE_LOAN_VIRTUAL => "transactions.account_type." . Transaction::ACCOUNT_TYPE_LOAN_VIRTUAL,
                ),
                "data" => array(
                    Loan::STATUS_DRAFT_REQUESTED
                )
            ))
        ;
    }


    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('dueDate')
            ->add('type', null, array('template' => 'AdminBundle:CRUD:list_transaction_type.html.twig'))
            ->add('fromUser.username', null, array('template' => 'AdminBundle:CRUD:list_transaction_from_user.html.twig'))
            ->add('toUser.username', null, array('template' => 'AdminBundle:CRUD:list_transaction_to_user.html.twig'))
            ->add('amount', null, array('template' => 'AdminBundle:CRUD:list_transaction_amount.html.twig'))
            ->add('variableNumber', null, array('template' => 'AdminBundle:CRUD:list_transaction_variable_number.html.twig'))
            ->add('status', null, array('template' => 'AdminBundle:CRUD:list_transaction_status.html.twig'))

            ->add('_action', 'actions', array(
                'template' => 'AdminBundle:CRUD:list_transaction_actions.html.twig',
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                )));

    }
}