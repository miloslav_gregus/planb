<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PayteR
 * Date: 2.9.2013
 * Time: 5:31
 * To change this template use File | Settings | File Templates.
 */

namespace Hg\AdminBundle\Admin;


use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\Notification;
use Hg\AppBundle\Entity\Transaction;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class NotificationAdmin extends Admin
{
    protected $baseRoutePattern = 'notifications';
    protected $translationDomain = 'messages';
    protected $maxPerPage = 25;
    protected $classnameLabel = 'Notifications';


    public function configure()
    {
        parent::configure();

        $this->datagridValues['_sort_by'] = 'createdAt';
        $this->datagridValues['_sort_order'] = 'DESC';
    }

    //protected $supportsPreviewMode = true;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit'));
        $collection->add('done', $this->getRouterIdParameter() . '/done');
    }

    public function getTemplate($name)
    {
        return parent::getTemplate($name);
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setStatus(1);

        return $instance;
    }


    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user')
        ;
    }

    public function getFilterParameters()
    {
        $this->datagridValues['status'] = array(
            "type" => "",
            "value" => Notification::STATUS_PENDING
        );
        return parent::getFilterParameters();
    }

    //Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('status', 'doctrine_orm_choice', array(), 'choice',array(
                'label' => 'transactions.filter.status',
                "choices" => array(
                    Notification::STATUS_DONE => "notifications.status." . Notification::STATUS_DONE,
                    Notification::STATUS_PENDING => "notifications.status." . Notification::STATUS_PENDING,
                )
            ))
        ;
    }


    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('check', null, array(
                'template' => 'AdminBundle:CRUD:list_notification_check.html.twig',
                "label" => " "
            ))
            ->add('message', null, array(
                'template' => 'AdminBundle:CRUD:list_notification_message.html.twig',
            ))
            ->add('createdAt', null, array(

            ))
            ->add('_action', 'actions', array(
                'template' => 'AdminBundle:CRUD:list_notification_actions.html.twig',
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                )));

    }
}