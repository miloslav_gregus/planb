<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PayteR
 * Date: 2.9.2013
 * Time: 5:31
 * To change this template use File | Settings | File Templates.
 */

namespace Hg\AdminBundle\Admin;


use Doctrine\ORM\EntityRepository;
use Hg\AppBundle\Entity\Loan;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Entity\Post;
use Sonata\AdminBundle\Show\ShowMapper;

class LoanAdmin extends Admin
{
    protected $baseRoutePattern = 'loans';
    protected $translationDomain = 'messages';
    protected $maxPerPage = 25;
    protected $classnameLabel = 'Loans';


    public function configure()
    {
        parent::configure();

        $this->datagridValues['_sort_by'] = 'createdAt';
        $this->datagridValues['_sort_order'] = 'DESC';
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', "show"));
        $collection->add('scoring', $this->getRouterIdParameter() . '/scoring');
        $collection->add('scoring_2', $this->getRouterIdParameter() . '/scoring-2');
        $collection->add('auction_approve', $this->getRouterIdParameter() . '/auction-approve');
        $collection->add('loan_approve', $this->getRouterIdParameter() . '/loan-approve');
        $collection->add('loan_pay', $this->getRouterIdParameter() . '/loan-pay');
        $collection->add('loan_file_approve', $this->getRouterIdParameter() . '/loan-file-approve');
        $collection->add('loan_file_reject', $this->getRouterIdParameter() . '/loan-file-reject');
        $collection->add('loan_file_delivered', $this->getRouterIdParameter() . '/loan-file-delivered');
    }


    //Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'loan.filter.id',
            ))
            ->add('auctionTitle', null, array(
                'label' => 'loan.filter.title'
            ))
            ->add('status', 'doctrine_orm_choice', array(), 'choice',array(
                'label' => 'loan.filter.status',
                "multiple" => true,
                "choices" => array(
                    Loan::STATUS_DRAFT => "loan.status." . Loan::STATUS_DRAFT,
                    Loan::STATUS_DRAFT_REQUESTED => "loan.status." . Loan::STATUS_DRAFT_REQUESTED,
                    Loan::STATUS_DRAFT_APPROVED => "loan.status." . Loan::STATUS_DRAFT_APPROVED,
                    Loan::STATUS_DRAFT_CANCELED => "loan.status." . Loan::STATUS_DRAFT_CANCELED,
                    Loan::STATUS_AUCTION_DRAFT => "loan.status." . Loan::STATUS_AUCTION_DRAFT,
                    Loan::STATUS_AUCTION_REQUESTED => "loan.status." . Loan::STATUS_AUCTION_REQUESTED,
                    Loan::STATUS_AUCTION_CANCELED => "loan.status." . Loan::STATUS_AUCTION_CANCELED,
                    Loan::STATUS_AUCTION_APPROVED => "loan.status." . Loan::STATUS_AUCTION_APPROVED,
                    Loan::STATUS_AUCTION => "loan.status." . Loan::STATUS_AUCTION,
                    Loan::STATUS_CANCELED => "loan.status." . Loan::STATUS_CANCELED,
                    Loan::STATUS_OVERDUE => "loan.status." . Loan::STATUS_OVERDUE,
                    Loan::STATUS_ACCEPTED => "loan.status." . Loan::STATUS_ACCEPTED,
                    Loan::STATUS_PAYABLE => "loan.status." . Loan::STATUS_PAYABLE,
                    Loan::STATUS_LOAN => "loan.status." . Loan::STATUS_LOAN,
                    Loan::STATUS_REPAID => "loan.status." . Loan::STATUS_REPAID,
                ),
                "data" => array(
                    Loan::STATUS_DRAFT_REQUESTED
                )
            ));
    }


    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array('template' => 'AdminBundle:CRUD:list_loan_id.html.twig'))
            ->add('status', null, array('template' => 'AdminBundle:CRUD:list_loan_status.html.twig'))
            ->add('auctionTitle', null, array('template' => 'AdminBundle:CRUD:list_loan_title.html.twig'))
            ->add('loanMaturity', null, array('template' => 'AdminBundle:CRUD:list_loan_maturity.html.twig'))
            ->add('loanPurpose', null, array('template' => 'AdminBundle:CRUD:list_loan_purpose.html.twig'))
            ->add('user.username')
            ->add('createdAt')

            ->add('_action', 'actions', array(
                'template' => 'AdminBundle:CRUD:list_loan_actions.html.twig',
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                )));

    }
}