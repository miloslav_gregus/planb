<?php


namespace Hg\AdminBundle\Form\User;

use Hg\AppBundle\Entity\ProfileFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class InvestorApproveType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\User',
            'validation_groups' => 'hg_form_investor_approve',
            "attr" => array(
                "id" => "form-upload-investor-approve"
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_upload_investor_approve';
    }
}
