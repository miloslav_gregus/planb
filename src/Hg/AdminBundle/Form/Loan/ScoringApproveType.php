<?php

namespace Hg\AdminBundle\Form\Loan;

use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Loan;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class ScoringApproveType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('auctionTitle', null, array(
            "label" => "loan.title"
        ))->add("auctionRating", "choice", array(
                "label" => "loan.rating",
                "choices" => array(
                    Loan::RATING_AA => "loan.rating_" . Loan::RATING_AA,
                    Loan::RATING_A => "loan.rating_" . Loan::RATING_A,
                    Loan::RATING_B => "loan.rating_" . Loan::RATING_B,
                    Loan::RATING_C => "loan.rating_" . Loan::RATING_C,
                    Loan::RATING_D => "loan.rating_" . Loan::RATING_D,
                )
            ))
            ->add("auctionApprovedAmount", null, array(
                "label" => "loan.approved_amount",
            ))
            ->add("auctionInterestRecommemded", "percent", array(
                "label" => "loan.interest_recommemded",
                "data" => 0.1150,
                "precision" => 2
            ))->add('loanMaturity', 'choice', array(
                'empty_value' => "empty_value",
                "label" => "loan.maturity",
                "choices" => array(
                    12 => "loan.maturity_12",
                    24 => "loan.maturity_24",
                    36 => "loan.maturity_36",
                    48 => "loan.maturity_48",
                    60 => "loan.maturity_60",
                ),
            ))
            ->add('loanPurpose', 'choice', array(
                'empty_value' => "empty_value",
                "label" => "loan.purpose",
                "choices" => array(
                    1 => "loan.purpose_1",
                    2 => "loan.purpose_2",
                    3 => "loan.purpose_3",
                    4 => "loan.purpose_4",
                    5 => "loan.purpose_5",
                ),
            ));
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'validation_groups' => array('form_auction_approve'),
            "attr" => array(
                "id" => "form-answer-add"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_auction_approve';
    }
}
