<?php

namespace Hg\AdminBundle\Form\Loan;

use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Loan;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class LifebuoyType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('value', null, array(
            "label" => "lifebuoy.value"
        ))->add('save', "submit", array(
                "label" => "lifebuoy.submit"
            ));
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Lifebuoy',
            "attr" => array(
                "id" => "form-lifebuoy-edit"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_lifebuoy_edit';
    }
}
