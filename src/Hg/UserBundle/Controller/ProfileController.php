<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\LoanFileEvent;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\HgEvents;
use Hg\UserBundle\Form\Profile\UploadLoanFilesType;
use Hg\UserBundle\Form\Profile\UploadProfileFilesType;
use Hg\UserBundle\Form\Type\ChangeBankNumberType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\User;

/**
 * Controller managing the user profile
 *
 */
class ProfileController extends Controller
{


    public function indexAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new NotFoundHttpException('User not found');
        }

        if ($user->getType() == $user::TYPE_PLANB) return $this->redirect("/");


        $return = array();
        $return['user'] = $user;
        $return["sidebarMenuPoint"] = 101;
        $template = "index";

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        if ($user->getType() == $user::TYPE_CUSTOMER) {
            /** @var Loan $loan */
            $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user);
            $return['loan'] = $loan;

            if (!$user->getDoneAlready()) {
                return $this->redirect($this->generateUrl("fos_user_profile_pending"));
            } else {
                $loans = $em->getRepository("AppBundle:Loan")->findByUser($user, array('createdAt' => 'DESC'));

                $transactions = $em->getRepository("AppBundle:Transaction")->findCustomerQuery($user, array(
                    Transaction::STATUS_PENDING,
                    Transaction::STATUS_OVERDUE,
                ))->getQuery()->getResult();

                if ($loan->isStatus(array($loan::STATUS_AUCTION_CANCELED, $loan::STATUS_DRAFT_CANCELED))) {
                    $finishedAt = clone $loan->getUpdatedAt();
                    $finishedAt->add(new \DateInterval('P91D'));
                    $now = new \DateTime();
                    $days = $now->diff($finishedAt)->format('%R%a');
                    $return['days'] = intval($days);
                }
                $return['loans'] = $loans;
                $return['transactions'] = $transactions;
            }


        }

        if ($user->getType() == $user::TYPE_INVESTOR) {

            $profile = $user->getProfile();
            $files = $em->getRepository("AppBundle:ProfileFile")->findBy(array(
                "profile" => $profile
            ));
            $inProgress = $em->getRepository("AppBundle:ProfileFile")->findBy(array(
                "profile" => $profile,
                "status" => array(ProfileFile::STATUS_SUCCESS)
            ));
            $inProgress = count($inProgress) == count($files);

            $file = new ProfileFile();
            $form = $this->createForm(new UploadProfileFilesType(), $file);

            if ($request->isMethod('POST')) {
                $form->submit($request);

                if ($form->isValid()) {

                    $files = $em->getRepository("AppBundle:ProfileFile")->findBy(array(
                        "type" => $file->getType(),
                        "status" => array(ProfileFile::STATUS_MISSING, ProfileFile::STATUS_DELETED),
                        "profile" => $profile
                    ));
                    foreach ($files AS $f) {
                        $em->remove($f);
                    }

                    $em->persist($file);
                    $file->setProfile($profile);
                    $em->flush();

                    $this->get("event_dispatcher")->dispatch(HgEvents::FILE_PENDING, new ProfileFileEvent($profile, $file));

                    return $this->redirect($this->generateUrl("fos_user_profile_index") . "#upload-table");
                }
            }

            $return['form'] = $form->createView();
            $return['files'] = $files;
            $return['inProgress'] = $inProgress;
        }


        return $this->container->get('templating')->renderResponse(
            'FOSUserBundle:Profile:' . $user->getType() . "/" . $template . '.html.twig',
            $return
        );
    }


    /**
     * @Template
     */
    public function editAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.change_password.form.factory');

        $formPassword = $formFactory->createForm();
        $formPassword->setData($user);

        $formBankNumber = $this->createForm(new ChangeBankNumberType() ,$user->getProfile());

        if ($request->isMethod('POST') && $request->request->get('fos_user_change_password_form')) {
            $formPassword->submit($request);
            if ($formPassword->isValid()) {
                /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
                $userManager = $this->container->get('fos_user.user_manager');

                $event = new FormEvent($formPassword, $request);
                $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_profile_my_info', array('username' => $user->getUsername()));
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            } else {
                $request->getSession()->getFlashBag()->add('danger', 'profile.password_error');
            }
        }

        if($user->isType($user::TYPE_INVESTOR) && $request->isMethod('POST') && $request->request->get('hg_change_bank_number')){
            $formBankNumber->submit($request);
            if ($formBankNumber->isValid()) {

                $this->getDoctrine()->getManager()->flush();

                $request->getSession()->getFlashBag()->add('success', 'profile.bank_success');

                $url = $this->generateUrl('fos_user_profile_my_info', array('username' => $user->getUsername()));
                return new RedirectResponse($url);
            } else {
                $request->getSession()->getFlashBag()->add('danger', 'profile.bank_error');
            }
        }

        return array(
            'formPassword' => $formPassword->createView(),
            'formBankNumber' => $formBankNumber->createView(),
            "sidebarMenuPoint" => 301
        );
    }

    /**
     * @Template
     */
    public function uploadAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface || !$user->isType($user::TYPE_CUSTOMER)) {
            throw new NotFoundHttpException('User not found');
        }

        $em = $this->getDoctrine()->getManager();

        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user);

        if (!$loan->isStatus(Loan::STATUS_AUCTION_SCORING)) return $this->redirect($this->generateUrl("fos_user_profile_pending"));

        $files = $em->getRepository("AppBundle:LoanFile")->findBy(array(
            "loan" => $loan
        ));
        $inProgress = $em->getRepository("AppBundle:LoanFile")->findBy(array(
            "loan" => $loan,
            "status" => array(LoanFile::STATUS_SUCCESS)
        ));
        $inProgress = count($inProgress) == count($files);

        $file = new LoanFile();
        $form = $this->createForm(new UploadLoanFilesType(), $file);

        if ($request->isMethod('POST')) {
            $form->submit($request);

            if ($form->isValid()) {
                /** @var Loan $loan */

                $files = $em->getRepository("AppBundle:LoanFile")->findBy(array(
                    "type" => $file->getType(),
                    "status" => array(LoanFile::STATUS_MISSING, LoanFile::STATUS_DELETED),
                    "loan" => $loan
                ));
                foreach ($files AS $f) {
                    $em->remove($f);
                }

                $em->persist($file);
                $file->setLoan($loan);
                $em->flush();

                $this->get("event_dispatcher")->dispatch(HgEvents::FILE_PENDING, new LoanFileEvent($loan, $file));

                return $this->redirect($this->generateUrl("fos_user_profile_upload") . "#upload-table");
            }

        }

        return array(
            "form" => $form->createView(),
            "files" => $files,
            "inProgress" => $inProgress,
            "sidebarMenuPoint" => 103
        );
    }


    public function pendingAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface || !$user->isType($user::TYPE_CUSTOMER)) {
            throw new NotFoundHttpException('User not found');
        }

        $return = array();
        $return['user'] = $user;
        $return["sidebarMenuPoint"] = 103;
        $template = "index";

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Loan $loan */
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user);

        if (!$loan) {
            $template = "loan_validate_1";
        } else {
            $return['loan'] = $loan;
            if ($loan->isStatus($loan::STATUS_DRAFT)) {
                return $this->redirect($this->generateUrl('fos_user_profile_loan_validate'));
            }

            if ($loan->isStatus(array($loan::STATUS_AUCTION_DRAFT, $loan::STATUS_DRAFT_APPROVED))) {
                return $this->redirect($this->generateUrl('fos_user_profile_auction_validate'));
            }

            if ($loan->isStatus($loan::STATUS_DRAFT_REQUESTED)) {
                $template = "loan_validate_3";
            }

            if ($loan->isStatus(array($loan::STATUS_AUCTION_CANCELED, $loan::STATUS_DRAFT_CANCELED))) {
                $template = "loan_canceled";

                $finishedAt = clone $loan->getUpdatedAt();
                $finishedAt->add(new \DateInterval('P91D'));
                $now = new \DateTime();

                $days = $now->diff($finishedAt)->format('%R%a');


                $return['days'] = intval($days);
            }


            if ($loan->isStatus($loan::STATUS_AUCTION_APPROVED)) {
                $template = "auction_approved";
                if ($request->query->has("launch")) {
                    $loan->setStatus(Loan::STATUS_AUCTION);
                    $loan->setAuctionStartAt(new \DateTime());
                    $loan->setAuctionEndAt(new \DateTime("+30days"));
                    $user->setStatus(User::STATUS_AUCTION);
                    $user->setDoneAlready(true);
                    $user->addAuctionsPending(-1)->addAuctionsActive(1);
                    $em->flush();

                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_AUCTION_LAUNCH, new LoanEvent($loan));
                    return $this->redirect($this->generateUrl("hg_app_auction_detail", array("slug" => $loan->getSlug())));
                }
            }

            if ($loan->isStatus($loan::STATUS_AUCTION_REQUESTED)) {
                return $this->redirect($this->generateUrl('fos_user_profile_auction_validate_success'));
            }
            if ($loan->isStatus($loan::STATUS_AUCTION_SCORING)) {
                return $this->redirect($this->generateUrl('fos_user_profile_upload'));
            }
        }

        return $this->container->get('templating')->renderResponse(
            'FOSUserBundle:Profile:' . $user->getType() . "/" . $template . '.html.twig',
            $return
        );
    }

}
