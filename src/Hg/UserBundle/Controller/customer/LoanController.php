<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Customer;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Repository\InvestmentRepository;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\HgEvents;
use Hg\UserBundle\Form\Type\AuctionStopType;
use Hg\UserBundle\Form\Type\LoanAcceptType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\User;

/**
 * Controller managing the user profile
 *
 */
class LoanController extends Controller
{

    /**
     * @Template
     */
    public function activeAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        $loans = $em->getRepository("AppBundle:Loan")->findBy(array(
            "status" => array(
                Loan::STATUS_LOAN,
            ),
            "user" => $user
        ), array(
            "createdAt" => "DESC"
        ));

        return array(
            "loans" => $loans,
            "sidebarMenuPoint" => 203
        );
    }

    /**
     * @Template
     */
    public function inactiveAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        $loans = $em->getRepository("AppBundle:Loan")->findBy(array(
            "status" => array(
                Loan::STATUS_REPAID,
            ),
            "user" => $user
        ), array(
            "createdAt" => "DESC"
        ));


        return array(
            "loans" => $loans,
            "sidebarMenuPoint" => 204
        );
    }


}
