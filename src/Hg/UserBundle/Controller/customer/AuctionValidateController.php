<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Customer;

use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class AuctionValidateController extends Controller
{
    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();


        if (!$user) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Loan $loan */
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user);


        if (!$user->isType($user::TYPE_CUSTOMER) || !$loan->isStatus(array(Loan::STATUS_AUCTION_DRAFT, Loan::STATUS_DRAFT_APPROVED))) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $flow = $this->get('hg_user.form.flow.auction_validate'); // must match the flow's service id
        $flow->bind($loan);

        // form of the current step
        $form = $flow->createForm();

        if ($flow->isValid($form)) {


            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {

                // form for the next step
                $em->flush();
                $form = $flow->createForm();
            } else {
                // flow finished

                //$files = $this->generatePdfFiles($loan);

                $loan->setStatus($loan::STATUS_AUCTION_REQUESTED);
                $user->setStatus($user::STATUS_AUCTION_REQUESTED);
                $em->flush();

                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_AUCTION_REQUESTED, new LoanEvent($loan));

                return $this->redirect($this->generateUrl('fos_user_profile_auction_validate_success')); // redirect when done
            }
        }

        return array(
            'user' => $user,
            "sidebarMenuPoint" => 103,
            'form' => $form->createView(),
            'flow' => $flow,
        );
    }

    /**
     * @Template
     */
    public function successAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user);
        if (!$loan->isStatus(Loan::STATUS_AUCTION_REQUESTED)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        return array(
            "sidebarMenuPoint" => 103
        );
    }




}
