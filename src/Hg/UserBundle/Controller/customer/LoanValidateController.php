<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Customer;

use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\LoanObligation;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Entity\Profile;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class LoanValidateController extends Controller
{


    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Loan $loan */
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user);

        // For new validate if last was canceled before and 30 days exceeded
        if ($loan && $loan->isStatus(array(
                $loan::STATUS_AUCTION_CANCELED,
                $loan::STATUS_DRAFT_CANCELED,
                $loan::STATUS_OVERDUE,
                $loan::STATUS_CANCELED,
                $loan::STATUS_LOAN,
                $loan::STATUS_REPAID,
            ))) {

            $finishedAt = clone $loan->getUpdatedAt();
            $finishedAt->add(new \DateInterval('P91D'));
            $now = new \DateTime();

            $days = $now->diff($finishedAt)->format('%R%a');

            if ($days < 1 || $loan->isStatus(array(
                    $loan::STATUS_OVERDUE,
                    $loan::STATUS_CANCELED,
                    $loan::STATUS_LOAN,
                    $loan::STATUS_REPAID
                )) ) {
                $loanNew = new Loan();
                $loanNew->setUser($user);
                $loanNew->setProfile($user->getProfile());
                $loanNew->setLoanMaturity($loan->getLoanMaturity());
                $loanNew->setLoanPurpose($loan->getLoanPurpose());
                $loanNew->setAuctionTitle($loan->getAuctionTitle());
                $user->setStatus(User::STATUS_PENDING);
                $user->addAuctionsPending(1);
                $em->persist($loanNew);

                $loan = $loanNew;
            }
        }

        if(!$user->isType($user::TYPE_CUSTOMER) || (isset($loan) && !$loan->isStatus(Loan::STATUS_DRAFT))) return $this->redirect($this->generateUrl("fos_user_profile_index"));


        if (!$profile = $user->getProfile()) {
            $profile = new Profile();
            $user->setProfile($profile);
            $em->persist($profile);
        };
        if (!$loan) {
            $loan = new Loan();
            $loan->setUser($user);
            $user->setAuctionsPending(1);
            $em->persist($loan);
        }
        $loan->setProfile($profile);

        $incomeEmploymentType = $loan->getIncomeEmploymentType();
        $incomeEmployerName = $loan->getIncomeEmployerName();
        $incomeEmploymentPosition = $loan->getIncomeEmploymentPosition();
        $incomeStartAt = $loan->getIncomeStartAt();
        $incomeEndAt = $loan->getIncomeEndAt();
        $incomeOther = $loan->getIncomeOther();
        $incomeType = $loan->getIncomeType();
        $incomeSector = $loan->getIncomeSector();

        $flow = $this->get('hg_user.form.flow.loan_validate'); // must match the flow's service id
        $flow->bind($loan);

        // form of the current step
        $form = $flow->createForm();

        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            // fix pre refinancovanie
            if($flow->getCurrentStep() == 1){
                if($loan->getLoanPurpose() == 2 && !$loan->getAdditionalObligations()->count()) {
                    $obligation = new LoanObligation();
                    $obligation->setTitle(" ");
                    $obligation->setAmount(0);
                    $obligation->setStart(new \Datetime("-1 year"));
                    $obligation->setEnd(new \Datetime("+1 year"));
                    $obligation->setMonthly(0);
                    $obligation->setLoan($loan);
                    $em->persist($obligation);
                }
            }

            if ($flow->nextStep()) {
                // form for the next step
                $em->flush();
                $form = $flow->createForm();
            } else {
                // flow finished

                $loan->setStatus($loan::STATUS_DRAFT_REQUESTED);
                $loan->setAuctionApprovedAmount($loan->getAuctionRequestedAmount());
                $loan->setIncomeEmploymentType($incomeEmploymentType);
                $loan->setIncomeEmployerName($incomeEmployerName);
                $loan->setIncomeEmploymentPosition($incomeEmploymentPosition);
                $loan->setIncomeStartAt($incomeStartAt);
                $loan->setIncomeEndAt($incomeEndAt);
                $loan->setIncomeOther($incomeOther);
                $loan->setIncomeType($incomeType);
                $loan->setIncomeSector($incomeSector);

                $user->setStatus(User::STATUS_DRAFT_REQUESTED);


                $em->flush();

                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_DRAFT_REQUESTED, new LoanEvent($loan));

                return $this->redirect($this->generateUrl('fos_user_profile_index')); // redirect when done
            }
        }

        return array(
            'user' => $user,
            "sidebarMenuPoint" => 103,
            'form' => $form->createView(),
            'flow' => $flow,
        );
    }

}
