<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Customer;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\Repository\InvestmentRepository;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\HgEvents;
use Hg\UserBundle\Form\Type\AuctionStopType;
use Hg\UserBundle\Form\Type\LoanAcceptType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\User;

/**
 * Controller managing the user profile
 *
 */
class AuctionController extends Controller
{

    /**
     * @Template
     */
    public function indexAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        /** @var Loan $loan */
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user, array(Loan::STATUS_AUCTION));

        if (!$loan) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        return array(
            "loan" => $loan,
            "sidebarMenuPoint" => 201
        );
    }

    /**
     * @Template
     */
    public function inactiveAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        $loans = $em->getRepository("AppBundle:Loan")->findBy(array(
            "status" => array(
                Loan::STATUS_CANCELED,
                Loan::STATUS_OVERDUE,
            ),
            "user" => $user
        ), array(
            "createdAt" => "DESC"
        ));


        return array(
            "loans" => $loans,
            "sidebarMenuPoint" => 202
        );
    }


    /**
     * @Template
     */
    public function stopAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Loan $loan */
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user, array(Loan::STATUS_AUCTION, Loan::STATUS_REAUCTION));

        if (!$loan) return $this->redirect($this->generateUrl("fos_user_profile_index"));



        /** @var InvestmentRepository $investmentsRepository */
        $investmentsRepository = $em->getRepository('AppBundle:Investment');
        $loansQuery = $investmentsRepository->findInvestmentsByLoanIdQuery($loan->getId(), array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED));
        $investments = $loansQuery->getQuery()->getResult();

        $calculations = $this->get("hg.services.planb.calculations")->recalculateSummary($loan, array(Investment::STATUS_INSERTED, Investment::STATUS_PREPARED));

        $form = $this->createForm(new AuctionStopType(), $loan);

        $form->submit($request);
        if ($form->isValid() && $calculations['isAcceptable']) {

            /**
             * zmena stavu investicii
             */
            $preparedInvestments = array();
            $deletedInvestments = array();
            foreach ($investments AS $investment) {
                if ($investment->isStatus(Investment::STATUS_LIMBO)) {
                    $investment->setStatus(Investment::STATUS_DELETED);
                    $deletedInvestments[] = $investment;
                }
                if ($investment->isStatus(Investment::STATUS_INSERTED)) {
                    $investment->setStatus(Investment::STATUS_PREPARED);
                    $preparedInvestments[] = $investment;
                }
            }

            /**
             * odstranenie pripadnych niekdajsich transakcii
             */
            $transactions = $em->getRepository("AppBundle:Transaction")->findByLoan($loan);
            foreach ($transactions AS $transaction) {
                $em->remove($transaction);
            }

            $loan->setStatus(Loan::STATUS_ACCEPTED);
            $loan->setFeeAmount($calculations['fee']);
            $loan->setFeeLifebuoy($calculations['lifebuoy_fee']);
            $user->setStatus(User::STATUS_A_ACCEPTED);
            $user->addAuctionsActive(-1);

            /** @var User $admin */
            $admin = $em->getRepository("AppBundle:User")->findAdmin();

            $calculation = $this->get("hg.services.planb.calculations");
            $loanTransactions = $calculation->recalculateLoanTransactionsSummary($loan);

            // nastavenie transakcii pre splacanie
            foreach ($loanTransactions['transactions'] AS $loanTransaction) {
                $transaction = new Transaction(Transaction::TYPE_REPAYMENT);
                $transaction->setAmount($loanTransaction['repayment']);
                $transaction->setVariableNumber($loan->getFormattedId());
                $transaction->setSpecificNumber($loanTransaction["month"]);
                $transaction->setLoan($loan);
                $transaction->setFromUser($loan->getUser());
                $transaction->setToUser($admin);
                $transaction->setDueDate($loanTransaction["date"]);
                $em->persist($transaction);
            }
            $lastTransaction = end($loanTransactions['transactions']);
            $loan->setLoanEndAt($lastTransaction['date']);

            // nastavenie transakcii pre vyplacanie investicii
            foreach ($investments AS $investment) {
                if ($investment->isStatus(array(Investment::STATUS_PREPARED))) {
                    $investmentTransactions = $calculation->recalculateInvestmentTransactionsSummary($investment);

                    foreach ($investmentTransactions['transactions'] AS $investmentTransaction) {
                        $transaction = new Transaction(Transaction::TYPE_PRINCIPAL);
                        $transaction->setAmount($investmentTransaction['principal']);
                        $transaction->setVariableNumber($loan->getFormattedId());
                        $transaction->setSpecificNumber($investmentTransaction["month"]);
                        $transaction->setLoan($loan);
                        $transaction->setFromUser($loan->getUser());
                        $transaction->setToUser($investment->getUser());
                        $transaction->setDueDate($investmentTransaction["date"]);
                        $em->persist($transaction);

                        $transaction = new Transaction(Transaction::TYPE_INTEREST);
                        $transaction->setAmount($investmentTransaction['interest']);
                        $transaction->setVariableNumber($loan->getFormattedId());
                        $transaction->setSpecificNumber($investmentTransaction["month"]);
                        $transaction->setLoan($loan);
                        $transaction->setFromUser($loan->getUser());
                        $transaction->setToUser($investment->getUser());
                        $transaction->setDueDate($investmentTransaction["date"]);
                        $em->persist($transaction);
                    }
                }
            }



            $calculation->recalculateLoan($loan, $status = array(Investment::STATUS_PREPARED), false);

            /**
             * Generation of pdf file
             */
            $documentsGenerator = $this->get("hg.services.planb.documents_generator");

            $files = array();

            $file = $documentsGenerator->generate("loan-agreement", array(
                "user" => $loan->getUser(),
                "loan" => $loan,
                "loanTransactions" => $loanTransactions,
            ));
            $loanFile = new LoanFile();
            $loanFile->setFileContent($file, $this->get("translator")->trans("file_type.loan_agreement"));
            $loanFile->setType($loanFile::TYPE_LOAN_AGREEMENT);
            $loanFile->setStatus($loanFile::STATUS_MISSING);
            $loanFile->setLoan($loan);
            $em->persist($loanFile);
            $files[] = $loanFile;


            $em->flush();
            $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_ACCEPTED, new LoanEvent($loan, $files));
            foreach ($deletedInvestments AS $investment) {
                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_INVESTMENT_DELETE, new InvestmentEvent($investment));
            }
            foreach ($preparedInvestments AS $investment) {
                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_INVESTMENT_PREPARED, new InvestmentEvent($investment));
            }

            return $this->redirect($this->generateUrl("fos_user_profile_index"));
        }

        return array(
            "loan" => $loan,
            "sidebarMenuPoint" => 201,
            "investments" => $investments,
            "calculations" => $calculations,
            "form" => $form->createView(),
        );
    }

    /**
     * @Template
     */
    public function cancelAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_CUSTOMER)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();
        /** @var Loan $loan */
        $loan = $em->getRepository("AppBundle:Loan")->findLastByUser($user, array(Loan::STATUS_AUCTION));

        if (!$loan) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_CANCELED, new LoanEvent($loan));

        return array(
            "loan" => $loan,
            "sidebarMenuPoint" => 202,
        );
    }


}
