<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Investor;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Hg\AppBundle\Entity\Contact;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Repository\TransactionRepository;
use Hg\AppBundle\Entity\Transaction;
use Hg\UserBundle\Form\Contact\ContactType;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class InvestmentController extends Controller
{


    /**
     * @Template
     */
    public function insertedAction(Request $request)
    {
        $user = $this->getUser();
        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        /** @var Investment $transactionsQuery */
        $transactionsQuery = $em->getRepository("AppBundle:Investment")->findByUserQuery($user, array(
            Investment::STATUS_INSERTED,
            Investment::STATUS_LIMBO,
            Investment::STATUS_PREPARED,
        ));

        $paginator = $this->get('knp_paginator');
        $investments = $paginator->paginate(
            $transactionsQuery,
            $request->query->get("paged", 1),
            10,
            array('distinct' => false)
        );


        return array(
            'investments' => $investments,
            "sidebarMenuPoint" => 401,
        );
    }

    /**
     * @Template
     */
    public function deletedAction(Request $request)
    {
        $user = $this->getUser();
        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        /** @var Investment $transactionsQuery */
        $transactionsQuery = $em->getRepository("AppBundle:Investment")->findByUserQuery($user, array(
            Investment::STATUS_CANCELED,
            Investment::STATUS_DELETED,
        ));

        $paginator = $this->get('knp_paginator');
        $investments = $paginator->paginate(
            $transactionsQuery,
            $request->query->get("paged", 1),
            10,
            array('distinct' => false)
        );


        return array(
            'investments' => $investments,
            "sidebarMenuPoint" => 402,
        );
    }

    /**
     * @Template
     */
    public function paidAction(Request $request)
    {
        $user = $this->getUser();
        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        /** @var Investment $transactionsQuery */
        $transactionsQuery = $em->getRepository("AppBundle:Investment")->findByUserQuery($user, array(
            Investment::STATUS_PAID,
        ));

        $paginator = $this->get('knp_paginator');
        $investments = $paginator->paginate(
            $transactionsQuery,
            $request->query->get("paged", 1),
            10,
            array('distinct' => false)
        );

        return array(
            'investments' => $investments,
            "sidebarMenuPoint" => 501,
        );
    }

    /**
     * @Template
     */
    public function repaidAction(Request $request)
    {
        $user = $this->getUser();
        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        $em = $this->getDoctrine()->getManager();

        /** @var Investment $transactionsQuery */
        $transactionsQuery = $em->getRepository("AppBundle:Investment")->findByUserQuery($user, array(
            Investment::STATUS_REPAID,
        ));

        $paginator = $this->get('knp_paginator');
        $investments = $paginator->paginate(
            $transactionsQuery,
            $request->query->get("paged", 1),
            10,
            array('distinct' => false)
        );

        return array(
            'investments' => $investments,
            "sidebarMenuPoint" => 502,
        );
    }


}
