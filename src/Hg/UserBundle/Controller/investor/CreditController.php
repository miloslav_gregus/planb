<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Investor;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\LoanObligation;
use Hg\AppBundle\Entity\Profile;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\HgEvents;
use Hg\UserBundle\Form\Credit\DespositType;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class CreditController extends Controller
{

    /**
     * @Template
     */
    public function withdrawAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $transaction = new Transaction(Transaction::TYPE_WITHDRAW);
        $transaction->setToUser($user);
        $form = $this->createForm("hg_withdraw_from", $transaction);

        if ($request->isMethod('POST')) {
            $form->submit($request);

            if($form->isValid()) {

                if($user->getCreditFree() >= 25){

                    $transaction->setStatus(Transaction::STATUS_PENDING);
                    $transaction->setFromUser($user);
                    $transaction->setAmount($form->get("amount")->getData());
                    $transaction->setVariableNumber("");
                    $em->persist($transaction);
                    $em->flush();

                    $transaction->setVariableNumber($transaction->getFormattedId());
                    $em->flush();

                    $request->getSession()->getFlashBag()->add('success', 'credit.withdraw.success');

                    $this->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_ADD, new TransactionEvent($transaction));
                }
                return $this->redirect($this->generateUrl("fos_user_investor_withdraw"));
            }
        }

        return array(
            'form' => $form->createView(),
            "sidebarMenuPoint" => 102,
        );
    }


    /**
     * @Template
     */
    public function depositAction(Request $request)
    {

        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new DespositType());

        if ($request->isMethod('POST')) {
            $form->submit($request);

            if($form->isValid()) {
                $transaction = new Transaction(Transaction::TYPE_DEPOSIT);
                $transaction->setStatus(Transaction::STATUS_PENDING);
                $transaction->setFromUser($user);
                $transaction->setToUser($user);
                $transaction->setVariableNumber("");
                $transaction->setAmount($form->get("amount")->getData());
                $em->persist($transaction);
                $em->flush();

                $transaction->setVariableNumber($transaction->getFormattedId());
                $em->flush();

                $this->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_ADD, new TransactionEvent($transaction));

                $request->getSession()->getFlashBag()->add('success', 'credit.deposit.success');

                return $this->redirect($this->generateUrl("fos_user_investor_deposit"));
            }
        }

        $lastTransaction = $em->getRepository("AppBundle:Transaction")->findOneBy(array(
            "toUser" => $user,
            "type" => Transaction::TYPE_DEPOSIT,
            "status" => Transaction::STATUS_PENDING
        ));

        return array(
            'form' => $form->createView(),
            "lastTransaction" => $lastTransaction,
            "sidebarMenuPoint" => 103,
        );
    }

}
