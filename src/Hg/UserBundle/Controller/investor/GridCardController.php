<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Investor;

use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class GridCardController extends Controller
{


    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        return array(
            'user' => $user,
            "sidebarMenuPoint" => 302,
        );
    }

    public function generateAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        if ($user->getValidationHash() !== null) {
            return $this->redirect($this->generateUrl("fos_user_grid_card"));
        }


        $em = $this->getDoctrine()->getManager();
        $user->setValidationHash(new \DateTime());
        $em->flush();

        $table = array();
        $rows = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        $cols = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J");

        foreach ($rows AS $r) {
            $table[$r] = array();
            foreach ($cols AS $c) {
                $table[$r][$c] = $user->getValidateHashByGrid($c . $r);
            }
        }

        $template = $this->get("twig")->render('UserBundle:Investor\GridCard:pdf.html.twig', array(
            "user" => $user,
            "table" => $table
        ));


        $mPDF = new \mPDF('', 'A4', 0, 'opensans', 10, 10, 7, 7, 80, 80);
        $mPDF->SetAuthor('planb.sk');
        $mPDF->WriteHTML($template);

        $nonziped = $mPDF->Output('planB-grid-karta.pdf', 'S');

        $nonzipedName = sys_get_temp_dir() . "/grid-karta-{$user->getUsername()}.pdf";
        $zipedName = sys_get_temp_dir() . "/" . uniqid() . ".zip";


        $password = $this->randomPassword();
        file_put_contents($nonzipedName, $nonziped);
        shell_exec("zip -P $password $zipedName $nonzipedName");
        $ziped = file_get_contents($zipedName);

        $files = array();
        $profileFile = new ProfileFile();
        $profileFile->setFileContent($ziped, 'planB-grid-karta.zip');
        $profileFile->setType($profileFile::TYPE_TERMS_CONDITIONS);
        $profileFile->setStatus($profileFile::STATUS_MISSING);
        $profileFile->setProfile($user->getProfile());
        $profileFile->prePersist();
        $profileFile->preUpload();
        $profileFile->upload();

        $files[] = $profileFile;

        $this->get("event_dispatcher")->dispatch(HgEvents::GRID_CARD_GENERATED, new UserEvent($user, $files));

        $request->getSession()->getFlashBag()->add("password", $password);
        //$profileFile->removeUpload();

        return $this->redirect($this->generateUrl("fos_user_grid_card"));
    }


    public function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}
