<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Investor;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Profile;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class WatchlistController extends Controller
{
    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        $paginator = $this->get('knp_paginator');
        $watchlistsQuery = $em->getRepository("AppBundle:Watchlist")->findByUserQuery($user);

        $paged = intval($request->query->get('paged', 1));
        $watchlists = $paginator->paginate(
            $watchlistsQuery,
            $paged /* page number */,
            10,
            array('distinct' => false)
        );


        return array(
            "watchlists" => $watchlists,
            "sidebarMenuPoint" => 305,
        );
    }

}
