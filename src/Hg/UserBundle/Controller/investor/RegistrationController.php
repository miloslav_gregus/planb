<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Investor;

use Hg\AppBundle\Entity\Profile;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class RegistrationController extends Controller
{
    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if(!$user->isType($user::TYPE_INVESTOR) ||  !$user->isStatus(array(User::STATUS_PENDING))) return $this->redirect($this->generateUrl("fos_user_profile_index"));


        $em = $this->getDoctrine()->getManager();

        if (!$profile = $user->getProfile()) {
            $profile = new Profile();
            $user->setProfile($profile);
        }

        //var_dump($profile->getTitle());
        $flow = $this->get('hg_user.form.flow.profile_registration'); // must match the flow's service id
        $flow->bind($profile);
        //var_dump($profile->getTitle());exit;


        // form of the current step
        $form = $flow->createForm();

        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $em->flush();
                $form = $flow->createForm();
            } else {
                // flow finished
                $user->setDoneAlready(true);
                $user->setStatus(User::STATUS_DOCUMENTS_WAITING);
                $profile->setUser($user);
                $em->persist($profile);


                $files = array( );

                /**
                 * Generation of pdf file
                 */
                $documentsGenerator = $this->get("hg.services.planb.documents_generator");

                $file = $documentsGenerator->generate("terms-conditions", array(
                    "user" => $user
                ));
                $loanFile = new ProfileFile();
                $loanFile->setFileContent($file, $this->get("translator")->trans("file_type.terms_conditions"));
                $loanFile->setType($loanFile::TYPE_TERMS_CONDITIONS);
                $loanFile->setStatus($loanFile::STATUS_MISSING);
                $loanFile->setProfile($user->getProfile());
                $em->persist($loanFile);
                $files[] = $loanFile;


                $file = $documentsGenerator->generate("data-processing-investor", array(
                    "user" => $user
                ));
                $profileFile = new ProfileFile();
                $profileFile->setFileContent($file, $this->get("translator")->trans("file_type.data_processing"));
                $profileFile->setType($profileFile::TYPE_DATA_PROCESSING);
                $profileFile->setStatus($profileFile::STATUS_MISSING);
                $profileFile->setProfile($user->getProfile());
                $em->persist($profileFile);
                $files[] = $profileFile;


                $file = $documentsGenerator->generate("framework-contract", array(
                    "user" => $user
                ));
                $profileFile = new ProfileFile();
                $profileFile->setFileContent($file, $this->get("translator")->trans("file_type.framework_contract"));
                $profileFile->setType($profileFile::TYPE_FRAMEWORK_CONTRACT);
                $profileFile->setStatus($profileFile::STATUS_MISSING);
                $profileFile->setProfile($user->getProfile());
                $em->persist($profileFile);
                $files[] = $profileFile;



                $em->flush();

                $this->get("event_dispatcher")->dispatch(HgEvents::INVESTOR_REGISTRATION, new UserEvent($user, $files));

                return $this->redirect($this->generateUrl('fos_user_investor_profile_registration_success')); // redirect when done
            }
        }

        return array(
            'user' => $user,
            "sidebarMenuPoint" => 101,
            'form' => $form->createView(),
            'flow' => $flow,
        );
    }


    /**
     * @Template
     */
    public function successAction(Request $request)
    {
        $user = $this->getUser();

        if(!$user->isStatus(array(User::STATUS_DOCUMENTS_WAITING))) return $this->redirect($this->generateUrl("fos_user_profile_index"));

        return array();
    }

}
