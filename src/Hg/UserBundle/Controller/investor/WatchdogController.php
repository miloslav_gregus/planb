<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller\Investor;

use Hg\AppBundle\Entity\Profile;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Entity\Repository\WatchdogRepository;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Entity\Watchdog;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class WatchdogController extends Controller
{
    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if(!$user->isType($user::TYPE_INVESTOR)) return $this->redirect($this->generateUrl("fos_user_profile_index"));


        $em = $this->getDoctrine()->getManager();

        $filter = $request->request->get('filter');
        if($filter){
            $filtersCount = $em->getRepository("AppBundle:Watchdog")->countByUser($user);

            $watchdog = new Watchdog();
            $em->persist($watchdog);
            $watchdog
                ->setUser($user)
                ->setName($filter['name'] ? $filter['name'] : "Filter " . ($filtersCount + 1))
                ->setAmountMin($filter['amountMin'])
                ->setAmountMax($filter['amountMax'])
                ->setAuctionRating($filter['rating'])
                ->setLoanMaturity($filter['maturity'])
                ->setInterestAverageMin($filter['interestAverageMin'] / 100)
                ->setInterestAverageMax($filter['interestAverageMax'] / 100)
                ->setCollectedPercentageMin($filter['collectedPercentageMin'] / 100)
                ->setCollectedPercentageMax($filter['collectedPercentageMax'] / 100)
                ->setDaysEndMin($filter['daysEndMin'])
                ->setDaysEndMax($filter['daysEndMax'])
            ;
            $em->flush();

            $request->getSession()->getFlashBag()->add("success", "watchdog.flash.success");

            return $this->redirect($this->generateUrl("fos_user_investor_watchdog"));
        }


        $paginator = $this->get('knp_paginator');
        /** @var WatchdogRepository $watchdogsQuery */
        $watchdogsQuery = $em->getRepository("AppBundle:Watchdog")->findByUserQuery($user);

        $paged = intval($request->query->get('paged', 1));
        $watchdogs = $paginator->paginate(
            $watchdogsQuery,
            $paged /* page number */,
            10,
            array('distinct' => false)
        );


        return array(
            "sidebarMenuPoint" => 304,
            "watchdogs" => $watchdogs,
        );
    }

}
