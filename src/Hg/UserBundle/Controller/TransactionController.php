<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Hg\AppBundle\Entity\Contact;
use Hg\AppBundle\Entity\Repository\TransactionRepository;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Entity\User;
use Hg\UserBundle\Form\Contact\ContactType;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class TransactionController extends Controller
{


    /**
     * @Template
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();


        if($user->isType($user::TYPE_CUSTOMER)){
            /** @var TransactionRepository $transactionsQuery */
            $transactionsQuery = $em->getRepository("AppBundle:Transaction")->findCustomerQuery($user);
        }
        /** @var TransactionRepository $transactionsQuery */
        if($user->isType($user::TYPE_INVESTOR)){
            $transactionsQuery = $em->getRepository("AppBundle:Transaction")->findInvestorQuery($user);
        }

        $paginator = $this->get('knp_paginator');
        $transactions = $paginator->paginate(
            $transactionsQuery,
            $request->query->get("paged", 1),
            10,
            array('distinct' => false)
        );


        return array(
            'transactions' => $transactions,
            "sidebarMenuPoint" => 104,
        );
    }


}
