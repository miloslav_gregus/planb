<?php

namespace Hg\UserBundle\Form\AuctionValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Test\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class AuctionValidateStep3Form extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
//            ->add('additionalFiles', 'collection', array(
//                "label" => "upload.your_files",
//                'type' => new AuctionValidateStep3Sub1(),
//                'allow_add'    => true,
//                'allow_delete' => true,
//                'prototype' => true,
//                'widget_add_btn' => array(
//                    'label' => "upload.file_add",
//                    "icon" => "icon-plus",
//                    "class" => "btn btn-customer"
//                ),
//                'options' => array(
//                    'label_render' => false,
//                    'widget_remove_btn' => array(
//                        'label' => "upload.file_remove",
//                        "icon" => "icon-remove",
//                        'attr' => array('class' => 'btn btn-danger')),
//                ),
//                "required" => false,
//                'show_legend' => true,
//                'by_reference' => false,
//
//            ))
            ->add('additionalWhyBorrowInfo', null, array(
                "label" => "loan.additional_why_borrow"
            ))
            ->add('additionalWhySecureInfo', null, array(
                "label" => "loan.additional_why_secure"
            ))
        ;

    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {


        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'cascade_validation' => true,
            "attr" => array(
                "id" => "form-auction-validate"
            ),
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_auction_validate_step3';
    }
}
