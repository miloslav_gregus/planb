<?php

namespace Hg\UserBundle\Form\AuctionValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AuctionValidateStep2Form extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('additionalNoObligations', "checkbox", array(
                "label" => "loan.additional_obligations_no_checkbox",
                "mapped" => false,
                "required" => false
            ))
            ->add('additionalObligations', 'collection', array(
                "label" => "loan.additional_your_obligations",
                'type' => new AuctionValidateStep2Sub1(),
                'allow_add'    => true,
                'allow_delete' => true,
                'prototype' => true,
                'widget_add_btn' => array(
                    'label' => "loan.obligation_add",
                    "icon" => "icon-plus",
                    "class" => "btn btn-customer"
                ),
                'options' => array(
                    'label_render' => false,
                    'widget_remove_btn' => array(
                        'label' => "loan.obligation_remove",
                        "icon" => "icon-remove",
                        'attr' => array('class' => 'btn btn-danger')),
                ),
                "required" => false,
                'show_legend' => true,
                'by_reference' => false,

            ))
            ->add('additionalIncomes', null, array(
                "label" => "loan.additional_incomes",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "0",
                    "data-max" => "10000"
                )
            ))
            ->add('additionalObligationInfo', null, array(
                "label" => "loan.additional_obligation_info",
                "required" => false,
            ))
            ->add('additionalOutcomesInfo', null, array(
                "label" => "loan.additional_outcomes_info",
                "required" => false,
            ))
            ->add('additionalIncomesOthers', null, array(
                "label" => "loan.additional_incomes_others",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "0",
                    "data-max" => "10000"
                )
            ))
            ->add('additionalIncomesPartner', null, array(
                "label" => "loan.additional_incomes_partner",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "0",
                    "data-max" => "10000"
                )
            ))
            ->add('additionalOutcomesHousehold', null, array(
                "label" => "loan.additional_outcomes_household",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "0",
                    "data-max" => "10000"
                )
            ))
            ->add('additionalKids', null, array(
                "label" => "loan.additional_kids",
                'widget_addon_append' => array(
                    'text' => '?',
                ),
                "attr" => array(
                    "data-min" => "0",
                    "data-max" => "12"
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'cascade_validation' => true,
            "attr" => array(
                "id" => "form-auction-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_auction_validate_step2';
    }
}
