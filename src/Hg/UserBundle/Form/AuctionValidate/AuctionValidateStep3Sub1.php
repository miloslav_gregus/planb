<?php

namespace Hg\UserBundle\Form\AuctionValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AuctionValidateStep3Sub1 extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', "choice", array(
                "label" => "upload.file_type",
                "choices" => array(
                    "upload.file_type_1" => "upload.file_type_1",
                    "upload.file_type_2" => "upload.file_type_2",
                ),
            ))
            ->add('file', "file", array(
                "label" => "upload.file_file",
                "horizontal_input_wrapper_class" => "form-control",
                'help_block' => 'upload.file_help',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\LoanFile',
            "attr" => array(
                "id" => "form-auction-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_auction_validate_step3_sub1';
    }
}
