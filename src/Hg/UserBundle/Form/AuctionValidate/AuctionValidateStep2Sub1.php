<?php

namespace Hg\UserBundle\Form\AuctionValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AuctionValidateStep2Sub1 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                "label" => "loan.obligation_title"
            ))
            ->add('amount', null, array(
                "label" => "loan.obligation_amount",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
            ))
            ->add('monthly', null, array(
                "label" => "loan.obligation_monthly",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
            ))
            ->add('start', null, array(
                "label" => "loan.obligation_start",
                "years" => range(date("Y"), date("Y") - 30)
            ))
            ->add('end', null, array(
                "label" => "loan.obligation_end",
                "years" => range(date("Y"), date("Y") + 30)
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\LoanObligation',
            "attr" => array(
                "id" => "form-auction-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_auction_validate_step2_sub1';
    }
}
