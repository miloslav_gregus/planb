<?php

namespace Hg\UserBundle\Form\AuctionValidate;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowEvents;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Craue\FormFlowBundle\Event\PostBindSavedDataEvent;
use Symfony\Component\Form\Test\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AuctionValidateFlow extends FormFlow
{


    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'auction_validate.step_1',
                'type' => new AuctionValidateStep1Form(),
            ),
            array(
                'label' => 'auction_validate.step_2',
                'type' => new AuctionValidateStep2Form(),
            ),
            array(
                'label' => 'auction_validate.step_3',
                'type' => new AuctionValidateStep3Form(),
            ),
            array(
                'label' => 'auction_validate.step_4',
                'type' => new AuctionValidateStep4Form(),
            ),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_auction_validate';
    }


    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);
        $options['validation_groups'] = array(
            $options['flow_step_key'] . $options['flow_step']
        );

        return $options;
    }


}
