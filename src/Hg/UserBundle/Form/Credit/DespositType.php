<?php

namespace Hg\UserBundle\Form\Credit;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;


class DespositType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('amount', "text", array(
            "label" => "credit.deposit.amount",
            'widget_addon_append' => array(
                'text' => '€',
            ),
            "attr" => array(
                "data-min" => 25,
                "data-max" => 10000
            ),
            "mapped" => false,
            "data" => 25,
            "constraints" => array(
                new Assert\NotBlank(),
                new Assert\Range(array(
                    "min" => 25,
                    "max" => 10000,
                ))
            )))
            ->add('submit', "submit", array(
                "label" => "credit.deposit.submit",
                "attr" => array("class" => "btn-brand")
            ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            "attr" => array(
                "id" => "form-deposit",
                "class" => "form-vertical-basic"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_deposit_from';
    }
}
