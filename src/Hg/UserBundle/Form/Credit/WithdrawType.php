<?php

namespace Hg\UserBundle\Form\Credit;

use Hg\AppBundle\Entity\Investment;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;


class WithdrawType extends AbstractType
{
    public $gridCardHandler;
    public $translator;

    public function __construct(GridCardHandler $gridCardHandler, $translator)
    {
        $this->gridCardHandler = $gridCardHandler;
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $withdrawType = $this;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($withdrawType){

            $form = $event->getForm();
            $data = $event->getData();

            $max = $withdrawType->calculateMaxAmount($data);
            $gridCardHandler = $withdrawType->gridCardHandler;
            $translator = $withdrawType->translator;

            /** @var Investment $data */
            $form->add('amount', "text", array(
                "label" => "credit.withdraw.amount",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "25",
                    "data-max" => $max
                ),
                'help_block' => $withdrawType->translator->trans("credit.withdraw.amount_help", array("%from%" => 25, "%to%" => $max)),
                "data" => 25,
                "constraints" => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        "min" => 25,
                        "max" => $max,
                    ))
                )
            ));


            $validationCodeLabel = $translator->trans("investments.grid_card");

            $form->add('validationCode',"text", array(
                "label" => $gridCardHandler->setLabel($validationCodeLabel),
                'widget_addon_append' => array(
                    'text' => '<span class="icon-key"></span>',
                ),
                "mapped" => false,
                "constraints" => array(
                    new Assert\NotBlank(),
                    new HgAssert\ValidationCode(array(), $withdrawType->gridCardHandler),
                )
            ));

            $form->add('submit', "submit", array(
                "label" => "credit.withdraw.submit",
                "attr" => array("class" => "btn-investor")
            ));
        });

    }

    public function calculateMaxAmount($data)
    {

        $maxAmount = $data->getToUser()->getCreditFree();

        return $maxAmount;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            "attr" => array(
                "id" => "form-withdraw",
                "class" => "form-vertical-basic"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_withdraw_from';
    }
}
