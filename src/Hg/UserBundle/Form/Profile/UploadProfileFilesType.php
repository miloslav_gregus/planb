<?php

namespace Hg\UserBundle\Form\Profile;

use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\ProfileFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadProfileFilesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', "choice", array(
                "label" => "upload.file_type",
                "choices" => array(
                    ProfileFile::TYPE_DATA_PROCESSING => "file_type." . ProfileFile::TYPE_DATA_PROCESSING,
                    ProfileFile::TYPE_FRAMEWORK_CONTRACT => "file_type." . ProfileFile::TYPE_FRAMEWORK_CONTRACT,
                    ProfileFile::TYPE_TERMS_CONDITIONS => "file_type." . ProfileFile::TYPE_TERMS_CONDITIONS,
                ),
            ))
            ->add('file', "file", array(
                "label" => "upload.file_file",
                "horizontal_input_wrapper_class" => "form-control",
                'help_block' => 'upload.file_help',
            ))
            ->add('pages', "choice", array(
                "label" => "upload.pages",
                "choices" => array_combine(range(1,30), range(1,30)),
                "multiple" => true,
                "required" => false,
                'help_block' => 'upload.pages_help',
            ))
            ->add('submit', "submit", array(
                "label" => "upload.file_submit",
                "attr" => array("class" => "btn btn-brand")
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\ProfileFile',
            'validation_groups' => 'hg_form_upload_profile_files',
            "attr" => array(
                "id" => "form-upload-profile-files"
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_upload_profile_files';
    }
}
