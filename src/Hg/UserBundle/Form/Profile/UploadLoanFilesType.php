<?php

namespace Hg\UserBundle\Form\Profile;

use Hg\AppBundle\Entity\LoanFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadLoanFilesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('type', "choice", array(
                "label" => "upload.file_type",
                "choices" => array(
                    LoanFile::TYPE_INSURANCE_CARD => "file_type." . LoanFile::TYPE_INSURANCE_CARD,
                    LoanFile::TYPE_ID_CARD => "file_type." . LoanFile::TYPE_ID_CARD,
                    LoanFile::TYPE_PASSPORT => "file_type." . LoanFile::TYPE_PASSPORT,
                    LoanFile::TYPE_DRIVER_LICENCE => "file_type." . LoanFile::TYPE_DRIVER_LICENCE,
                    LoanFile::TYPE_OBLIGATION_DOCUMENT => "file_type." . LoanFile::TYPE_OBLIGATION_DOCUMENT,
                    LoanFile::TYPE_PERSONAL_DATA => "file_type." . LoanFile::TYPE_PERSONAL_DATA,
                    LoanFile::TYPE_INCOME_DOCUMENT => "file_type." . LoanFile::TYPE_INCOME_DOCUMENT,
                    LoanFile::TYPE_DATA_PROCESSING => "file_type." . LoanFile::TYPE_DATA_PROCESSING,
                    LoanFile::TYPE_TERMS_CONDITIONS => "file_type." . LoanFile::TYPE_TERMS_CONDITIONS,
                    LoanFile::TYPE_BANK_STATEMENT => "file_type." . LoanFile::TYPE_BANK_STATEMENT,
                    LoanFile::TYPE_LOAN_REGISTER => "file_type." . LoanFile::TYPE_LOAN_REGISTER,
                    LoanFile::TYPE_LOAN_REQUEST => "file_type." . LoanFile::TYPE_LOAN_REQUEST,
                ),

            ))
            ->add('file', "file", array(
                "label" => "upload.file_file",
                "horizontal_input_wrapper_class" => "form-control",
                'help_block' => 'upload.file_help',
            ))
            ->add('pages', "choice", array(
                "label" => "upload.pages",
                "choices" => array_combine(range(1,30), range(1,30)),
                "multiple" => true,
                'help_block' => 'upload.pages_help',
            ))
            ->add('submit', "submit", array(
                "label" => "upload.file_submit",
                "attr" => array("class" => "btn btn-brand")
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\LoanFile',
            'validation_groups' => 'hg_form_upload_loan_files',
            "attr" => array(
                "id" => "form-upload-loan-files"
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_upload_loan_files';
    }
}
