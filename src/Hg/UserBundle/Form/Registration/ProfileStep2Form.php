<?php

namespace Hg\UserBundle\Form\Registration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileStep2Form extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('addressStreet', null, array(
                "label" => "profile.address_street",
            ))
            ->add('addressNumber', null, array(
                "label" => "profile.address_number",
            ))
            ->add('addressCity', null, array(
                "label" => "profile.address_city",
            ))
            ->add('addressZip', null, array(
                "label" => "profile.address_zip",
            ))
            ->add('addressCountry', "country", array(
                "label" => "profile.address_country",
            ))
            ->add('addressRegion', "choice", array(
                "label" => "profile.address_region",
                'empty_value' => "profile.address_region_empty",
                "choices" => array(
                    1 => "profile.address_region_1",
                    2 => "profile.address_region_2",
                    3 => "profile.address_region_3",
                    4 => "profile.address_region_4",
                    5 => "profile.address_region_5",
                    6 => "profile.address_region_6",
                    7 => "profile.address_region_7",
                    8 => "profile.address_region_8",
                ),
            ))
            ->add('addressContactNotSame', null, array(
                "label" => "profile.address_contact_not_same",
                "required" => false
            ))
            ->add('addressContactStreet', null, array(
                "label" => "profile.address_street",
            ))
            ->add('addressContactNumber', null, array(
                "label" => "profile.address_number",
            ))
            ->add('addressContactCity', null, array(
                "label" => "profile.address_city",
            ))
            ->add('addressContactZip', null, array(
                "label" => "profile.address_zip",
            ))
            ->add('addressContactCountry', "country", array(
                "label" => "profile.address_country",
            ))
            ->add('addressContactRegion', "choice", array(
                "label" => "profile.address_region",
                "label" => "profile.address_region",
                'empty_value' => "profile.address_region_empty",
                "choices" => array(
                    1 => "profile.address_region_1",
                    2 => "profile.address_region_2",
                    3 => "profile.address_region_3",
                    4 => "profile.address_region_4",
                    5 => "profile.address_region_5",
                    6 => "profile.address_region_6",
                    7 => "profile.address_region_7",
                    8 => "profile.address_region_8",
                ),
            ))
            ->add('addressTel', null, array(
                "label" => "profile.address_tel",
                "required" => false
            ))
            ->add('addressMobil', null, array(
                "label" => "profile.address_mobil",
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Profile',
            "attr" => array(
                "id" => "form-profile-registration"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_profile_registration_step2';
    }
}
