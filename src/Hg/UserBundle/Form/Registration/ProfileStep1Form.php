<?php

namespace Hg\UserBundle\Form\Registration;

use Hg\AppBundle\Form\DataTransformer\IdCardTransformer;
use Hg\AppBundle\Form\DataTransformer\PersonalIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileStep1Form extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $personalIdTransformer = new PersonalIdTransformer();
        $idCardTransformer = new IdCardTransformer();
        $builder
            ->add('title', null, array(
                "label" => "profile.title",
                "required" => false
            ))
            ->add('name', null, array(
                "label" => "profile.name"
            ))
            ->add('surname', null, array(
                "label" => "profile.surname"
            ))
            ->add('birthSurname', null, array(
                "label" => "profile.birth_surname",
                "required" => false
            ))
            ->add('gender', 'choice', array(
                "label" => "profile.gender",
                'empty_value' => "empty_value",
                "choices" => array(
                    1 => "profile.gender_1",
                    2 => "profile.gender_2",
                ),
            ))
            ->add('birthDate', "birthday", array(
                "label" => "profile.birth_date",
                "years" => range(date("Y") - 18, date("Y") - 80)
            ))
            ->add(
                $builder->create('personalId', null, array(
                    "label" => "profile.personal_id",
                    "attr" => array(
                        "placeholder" => "RRMMDDXXXX"
                    )))->addModelTransformer($personalIdTransformer)
            )
            ->add('citizenship', null, array(
                "read_only" => true,
                "data" => "Slovenské",
                "label" => "profile.citizenship"
            ))
            ->add($builder->create('idCard', null, array(
                    "label" => "profile.id_card",
                    "attr" => array(
                        "placeholder" => "AA123456"
                    )))->addModelTransformer($idCardTransformer)
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Profile',
            "attr" => array(
                "id" => "form-profile-registration"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_profile_registration_step1';
    }
}
