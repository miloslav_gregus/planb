<?php

namespace Hg\UserBundle\Form\Registration;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowEvents;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Craue\FormFlowBundle\Event\PostBindSavedDataEvent;
use Symfony\Component\Form\Test\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileRegistrationFlow extends FormFlow
{


    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'profile_registration.step_1',
                'type' => new ProfileStep1Form(),
            ),
            array(
                'label' => 'profile_registration.step_2',
                'type' => new ProfileStep2Form(),
            ),
            array(
                'label' => 'profile_registration.step_3',
                'type' => new ProfileStep3Form(),
            ),
            array(
                'label' => 'profile_registration.step_4',
            ),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_profile_registration';
    }


    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);
        $options['validation_groups'] = array(
            $options['flow_step_key'] . $options['flow_step']
        );

        if ($step === 2) {

            $options['validation_groups'] = function ($form) use ($options) {
                $data = $form->getData();

                if ($form->getData()->getAddressCountry() == "SK") {
                    $options['validation_groups'][] = "flow_hg_profile_registration_step2_has_country";
                }

                $isSame = $form->getData()->getAddressContactNotSame();

                if ($isSame) {
                    $options['validation_groups'][] = "flow_hg_profile_registration_step2_extra";
                    if ($form->getData()->getAddressContactCountry() == "SK") {
                        $options['validation_groups'][] = "flow_hg_profile_registration_step2_has_contact_country";
                    }
                }
                return $options;
            };
        }

        return $options;
    }


}
