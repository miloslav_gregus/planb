<?php

namespace Hg\UserBundle\Form\Type;

use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Form\DataTransformer\DateStringTransformer;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class AuctionStopType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $dateStringTransformer = new DateStringTransformer();

        $startDates = array();
        for ($i = 7; $i <= 30; $i++) {
            $date = strtotime("+$i days");
            $dayOfWeek = date("N", $date);
            if ($dayOfWeek < 6) {
                $startDates[date("Y-m-d", $date)] = date("d.m.Y", $date);
            }
        }

        $builder->add(
            $builder->create('loanStartAt', "choice", array(
                "label" => "loan.loan_start_at",
                "choices" => $startDates,
                'help_block' => 'loan.loan_start_at_help',
            ))->addModelTransformer($dateStringTransformer)
        );

        $builder->add('repaymentDay', "choice", array(
            "label" => "loan.repayment_day",
            "choices" => array_combine(range(1, 28), range(1, 28)),
            'help_block' => 'loan.repayment_day_help',
        ));

    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            //   'csrf_protection' => false,
            "attr" => array(
                "id" => "form-auction-stop"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_auction_stop';
    }
}
