<?php

namespace Hg\UserBundle\Form\Type;

use Hg\AppBundle\Entity\Investment;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class LoanAcceptType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
         //   'csrf_protection' => false,
            "attr" => array(
                "id" => "form-loan-accept"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_loan_accept';
    }
}
