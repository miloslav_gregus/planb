<?php

namespace Hg\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangeBankNumberType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bankPrefix', null, array(
                "label" => "profile.additional_bank_prefix"
            ))
            ->add('bankNumber', null, array(
                "label" => "profile.additional_bank_number"
            ))
            ->add('bankCode', "choice", array(
                "label" => "profile.additional_bank_code",
                "empty_value" => "profile.additional_bank_code_empty",
                "choices" => array(
                    "0200" => "profile.additional_bank_code_0200",
                    "0900" => "profile.additional_bank_code_0900",
                    "0720" => "profile.additional_bank_code_0720",
                    "1100" => "profile.additional_bank_code_1100",
                    "1111" => "profile.additional_bank_code_1111",
                    "3000" => "profile.additional_bank_code_3000",
                    "3100" => "profile.additional_bank_code_3100",
                    "4900" => "profile.additional_bank_code_4900",
                    "5200" => "profile.additional_bank_code_5200",
                    "5600" => "profile.additional_bank_code_5600",
                    "5900" => "profile.additional_bank_code_5900",
                    "6500" => "profile.additional_bank_code_6500",
                    "7300" => "profile.additional_bank_code_7300",
                    "7500" => "profile.additional_bank_code_7500",
                    "7930" => "profile.additional_bank_code_7930",
                    "8020" => "profile.additional_bank_code_8020",
                    "8050" => "profile.additional_bank_code_8050",
                    "8100" => "profile.additional_bank_code_8100",
                    "8120" => "profile.additional_bank_code_8120",
                    "8130" => "profile.additional_bank_code_8130",
                    "8170" => "profile.additional_bank_code_8170",
                    "8160" => "profile.additional_bank_code_8160",
                    "8180" => "profile.additional_bank_code_8180",
                    "8191" => "profile.additional_bank_code_8191",
                    "8300" => "profile.additional_bank_code_8300",
                    "8400" => "profile.additional_bank_code_8400",
                    "8320" => "profile.additional_bank_code_8320",
                    "8330" => "profile.additional_bank_code_8330",
                    "8410" => "profile.additional_bank_code_8410",
                )
            ))->add('savePassword', 'submit', array(
                'label' => 'save',
                'attr' => array('class' => 'btn-brand')));;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Profile',
            "attr" => array(
                "id" => "form-change-bank-number"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_change_bank_number';
    }
}
