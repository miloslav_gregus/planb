<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Form\Type;

use Hg\AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType AS RegistrationFormTypeBase;
use Symfony\Component\Validator\Constraints\True;

class RegistrationFormType extends RegistrationFormTypeBase
{

    private $type;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array(
                'label' => 'form.username',
                "attr" => array("placeholder" => "form.username_placeholder")
            ))
            ->add('email', 'email', array(
                'label' => 'form.email',
                "attr" => array("placeholder" => "form.email_placeholder")
            ))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'first_options' => array(
                    'label' => 'form.password',
                    "attr" => array("placeholder" => "form.password_placeholder")
                ),
                'second_options' => array(
                    'label' => 'form.password_confirmation',
                    "attr" => array("placeholder" => "form.password_confirmation_placeholder")
                ),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add("terms", "checkbox", array(
                "mapped" => false,
                "widget_type" => 'inline',
                "label" => "registration.terms_of_use",
                "constraints" => new True(array(
                        "message" => "fos_user.registration.terms_error")
                )
            ))
            ->add("data_process", "checkbox", array(
                "mapped" => false,
                "widget_type" => 'inline',
                "label" => "registration.process_personal_data",
                "constraints" => new True(array(
                        "message" => "fos_user.registration.data_error")
                )
            ))
            ->add("framework_contract", "checkbox", array(
                "mapped" => false,
                "widget_type" => 'inline',
                "label" => "registration.framework_contract",
                "constraints" => new True(array(
                        "message" => "fos_user.registration.framework_contract_error")
                )
            ))
            ->add("submit", 'submit', array(
                'label' => 'form.submit_investor',
                'icon' => 'icon-user-arrow-out',
                "attr" => array("class" => "btn-lg btn-"))
            )
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($builder) {

            $form = $event->getForm();
            $data = $event->getData();

            if($data){
                $field = $form->get('submit');         // get the field
                $options = $field->getConfig()->getOptions();            // get the options
                $type = $field->getConfig()->getName();       // get the name of the type
                $options['attr']['class'] =  "btn-lg btn-" . $data->getType();           // change the label

                $options['label'] = "form.submit_" . $data->getType();

                $form->add('submit', $type, $options);
            }

        });

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'translation_domain' => 'FOSUserBundle'
        ));
    }

    public function getName()
    {
        return 'hg_user_registration';
    }

}
