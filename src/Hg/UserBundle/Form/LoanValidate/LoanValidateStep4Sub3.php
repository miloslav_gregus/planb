<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoanValidateStep4Sub3 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('incomeEmploymentType', "choice", array(
                'empty_value' => "empty_value",
                'label' => "loan.income_employment_type_sub3",
                "choices" => array(
                    7 => "loan.income_employment_type_7",
                    8 => "loan.income_employment_type_8",
                    9 => "loan.income_employment_type_9",
                    10 => "loan.income_employment_type_10",
                    11 => "loan.income_employment_type_11",
                    12 => "loan.income_employment_type_12",
                    13 => "loan.income_employment_type_13",
                )))
            ->add('incomeOther', null, array(
                "label" => "loan.income_business_other"
            ))
            ->add('incomeStartAt', null, array(
                "label" => "loan.income_employed_business_start_at",
                "years" => range(date("Y"), date("Y") - 30),
            ))
            ->add('incomeSector', "choice", array(
                "label" => "loan.income_sector",
                'empty_value' => "empty_value",
                "choices" => LoanValidateStep4Form::getSectorValues()
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'inherit_data' => true,
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step4';
    }
}
