<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class LoanValidateStep4Form extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('incomeType', "choice", array(
                "label" => "loan.income_type",
                'empty_value' => "loan.income_type_empty",
                "choices" => array(
                    1 => "loan.income_type_1",
                    2 => "loan.income_type_2",
                    3 => "loan.income_type_3",
                    4 => "loan.income_type_4",
                    5 => "loan.income_type_5",
                    6 => "loan.income_type_6",
                    7 => "loan.income_type_7",
                    8 => "loan.income_type_8",
                )));


        //$builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, "eventSetSubforms"));
        //$builder->addEventListener(FormEvents::SUBMIT, array($this, "eventSetSubforms"));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, "eventSetSubforms"));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step4';
    }

    public function eventSetSubforms($event)
    {
        $form = $event->getForm();
        $formData = $event->getData();

        if ($event->getName() != FormEvents::PRE_SUBMIT) {

                $form
                    ->add('incomeType1', new LoanValidateStep4Sub1(), array(
                        "label" => false
                    ))
                    ->add('incomeType2', new LoanValidateStep4Sub2(), array(
                        "label" => false
                    ))
                    ->add('incomeType3', new LoanValidateStep4Sub3(), array(
                        "label" => false
                    ))
                    ->add('incomeType4', new LoanValidateStep4Sub4(), array(
                        "label" => false
                    ))
                    ->add('incomeType5', new LoanValidateStep4Sub5(), array(
                        "label" => false
                    ))
                    ->add('incomeType6', new LoanValidateStep4SubEmpty(), array(
                        "label" => false
                    ))
                    ->add('incomeType7', new LoanValidateStep4Sub7(), array(
                        "label" => false
                    ))
                    ->add('incomeType8', new LoanValidateStep4Sub8(), array(
                        "label" => false
                    ));

                //   $incomeType = $formData['incomeType'];


            /* Yebem na to, musi to byt takto */
            if(isset($_POST['hg_loan_validate_step4']) && isset($_POST['hg_loan_validate_step4']['incomeType'])){

                $incomeType = $_POST['hg_loan_validate_step4']['incomeType'];


                if(!$incomeType) return;

                $field = "incomeType" . $incomeType;
                $class = __NAMESPACE__ . '\\' . "LoanValidateStep4Sub" . $incomeType;

                $form->remove($field);
                $form->add($field, new $class, array(
                    'data_class' => 'Hg\AppBundle\Entity\Loan',
                    "label" => false
                ));
            }

        } else {

//            $incomeType = $_POST['hg_loan_validate_step4']['incomeType'];
//            for ($i = 1; $i < 9; $i++) {
//
//                if ($incomeType != $i) {
//                    $form
//                        ->add('incomeType' . $i, new LoanValidateStep4SubEmpty(), array(
//                                'data_class' => 'Hg\AppBundle\Entity\Loan'
//                            ));
//                }
//            }
        }
    }


    public static function getSectorValues()
    {
        return array(
            1 => "loan.income_sector_1",
            2 => "loan.income_sector_2",
            3 => "loan.income_sector_3",
            4 => "loan.income_sector_4",
            5 => "loan.income_sector_5",
            6 => "loan.income_sector_6",
            7 => "loan.income_sector_7",
            8 => "loan.income_sector_8",
            9 => "loan.income_sector_9",
            10 => "loan.income_sector_10",
            11 => "loan.income_sector_11",
            12 => "loan.income_sector_12",
            13 => "loan.income_sector_13",
            14 => "loan.income_sector_14",
            15 => "loan.income_sector_15",
            16 => "loan.income_sector_16",
            17 => "loan.income_sector_17",
            18 => "loan.income_sector_18",
            19 => "loan.income_sector_19",
            20 => "loan.income_sector_20",
            21 => "loan.income_sector_21",
            22 => "loan.income_sector_22",
            23 => "loan.income_sector_23",
        );
    }
}
