<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoanValidateStep1Sub1 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('auctionTitle', null, array(
                "label" => "loan.title"
            ))
            ->add('auctionRequestedAmount', "integer", array(
                "label" => "loan.requested_amount",

                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "300",
                    "data-max" => "10000"
                )
            ))
            ->add('loanMaturity', 'choice', array(
                'empty_value' => "empty_value",
                "label" => "loan.maturity",
                "choices" => array(
                    12 => "loan.maturity_12",
                    24 => "loan.maturity_24",
                    36 => "loan.maturity_36",
                    48 => "loan.maturity_48",
                    60 => "loan.maturity_60",
                ),
            ))
            ->add('loanPurpose', 'choice', array(
                'empty_value' => "empty_value",
                "label" => "loan.purpose",
                "choices" => array(
                    1 => "loan.purpose_1",
                    2 => "loan.purpose_2",
                    3 => "loan.purpose_3",
                    4 => "loan.purpose_4",
                    5 => "loan.purpose_5",
                ),
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step1';
    }
}
