<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoanValidateStep4Sub1 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('incomeEmploymentType', "choice", array( "label" => "loan.income_employment_type",
                'empty_value' => "empty_value",
                'label' => "loan.income_employment_type_sub1",
                "choices" => array(
                    1 => "loan.income_employment_type_1",
                    2 => "loan.income_employment_type_2",
                )))
            ->add('incomeEmployerName', null, array(
                "label" => "loan.income_employer_name"
            ))
            ->add('incomeEmploymentPosition', null, array(
                "label" => "loan.income_employer_position"
            ))
            ->add('incomeStartAt', null, array(
                "label" => "loan.income_employed_start_at",
                "years" => range(date("Y"), date("Y") - 30),
            ))
            ->add('incomeEndAt', null, array(
                "label" => "loan.income_employed_aggreement_end_at",
                "years" => range(date("Y"), date("Y") + 10),
            ))
            ->add('incomeSector', "choice", array(
                "label" => "loan.income_sector",
                'empty_value' => "empty_value",
                "choices" => LoanValidateStep4Form::getSectorValues()
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'inherit_data' => true,
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step4';
    }
}
