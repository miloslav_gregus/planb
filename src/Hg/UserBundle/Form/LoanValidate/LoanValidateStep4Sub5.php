<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoanValidateStep4Sub5 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('incomeEmploymentType', "choice", array( "label" => "loan.income_employment_type",
                'label' => "loan.income_employment_type_sub5",
                'empty_value' => "empty_value",
                "choices" => array(
                    18 => "loan.income_employment_type_18",
                    19 => "loan.income_employment_type_19",
                    20 => "loan.income_employment_type_20",
                    21 => "loan.income_employment_type_21",
                    22 => "loan.income_employment_type_22",
                )))
            ->add('incomeStartAt', null, array(
                "years" => range(date("Y"), date("Y") - 30),
                "label" => "loan.income_employed_support_start_at"
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'inherit_data' => true,
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step4';
    }
}
