<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoanValidateStep2Form extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('profile',new LoanValidateStep2Sub1(), array("label" => false));


    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'cascade_validation' => true,
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step2';
    }
}
