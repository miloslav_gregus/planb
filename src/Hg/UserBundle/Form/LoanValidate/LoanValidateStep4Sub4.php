<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoanValidateStep4Sub4 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('incomeEmploymentType', "choice", array( "label" => "loan.income_employment_type",
                'label' => "loan.income_employment_type_sub4",
                'empty_value' => "empty_value",
                "choices" => array(
                    14 => "loan.income_employment_type_14",
                    15 => "loan.income_employment_type_15",
                    16 => "loan.income_employment_type_16",
                    17 => "loan.income_employment_type_17",
                )))
            ->add('incomeOther', null, array(
                "label" => "loan.income_business_other"
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'inherit_data' => true,
            "attr" => array(
                "id" => "form-loan-validate"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate_step4';
    }
}
