<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Test\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\True;

class LoanValidateStep5Form extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('acceptRiskAuction', "checkbox", array(
                "label" => "loan.additional_accept_risk_auction",
                "mapped" => false,
                "required" => true,
                'constraints' => array(
                    new NotBlank(array('groups' => array('flow_hg_loan_validate_step5'))),
                )
            ))
        ;

    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {


        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Loan',
            'cascade_validation' => true,
            "attr" => array(
                "id" => "form-auction-validate"
            ),
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_auction_validate_step4';
    }
}
