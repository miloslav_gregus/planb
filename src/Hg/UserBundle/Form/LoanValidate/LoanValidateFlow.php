<?php

namespace Hg\UserBundle\Form\LoanValidate;

use Craue\FormFlowBundle\Form\FormFlow;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class LoanValidateFlow extends FormFlow
{


    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'loan_validate.step_1',
                'type' => new LoanValidateStep1Form(),
            ),
            array(
                'label' => 'loan_validate.step_2',
                'type' => new LoanValidateStep2Form(),
            ),
            array(
                'label' => 'loan_validate.step_3',
                'type' => new LoanValidateStep3Form(),
            ),
            array(
                'label' => 'loan_validate.step_4',
                'type' => new LoanValidateStep4Form(),
            ),
            array(
                'label' => 'loan_validate.step_5',
                'type' => new LoanValidateStep5Form(),
            ),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hg_loan_validate';
    }


    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);
        $options['validation_groups'] = array(
            $options['flow_step_key'] . $options['flow_step']
        );


        if ($step === 3) {

            $options['validation_groups'] = function ($form) use ($options) {
                $data = $form->getData();

                if ($data->getProfile()->getAddressCountry() == "SK") {
                    $options['validation_groups'][] = "flow_hg_loan_validate_step3_has_country";
                }

                $isSame = $data->getProfile()->getAddressContactNotSame();

                if ($isSame) {
                    $options['validation_groups'][] = "flow_hg_loan_validate_step3_extra";
                    if ($data->getProfile()->getAddressContactCountry() == "SK") {
                        $options['validation_groups'][] = "flow_hg_loan_validate_step3_has_contact_country";
                    }
                }

                return $options;
            };
        }

        if ($step === 4) {

            $options['validation_groups'] = function ($form) use ($options) {
                $data = $form->getData();

                $options['validation_groups'][] = "flow_hg_loan_validate_step4_sub" . $data->getIncomeType();

                if ($data->getIncomeEmploymentType() == 13) {
                    $options['validation_groups'][] = "flow_hg_loan_validate_step4_sub3_others";
                }

                if ($data->getIncomeEmploymentType() == 17) {
                    $options['validation_groups'][] = "flow_hg_loan_validate_step4_sub4_others";
                }

                return $options;
            };
        }

        return $options;
    }


}
