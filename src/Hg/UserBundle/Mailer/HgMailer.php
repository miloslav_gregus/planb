<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\UserBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @author Christophe Coevoet <stof@notk.org>
 */
class HgMailer implements MailerInterface
{
    protected $eventDispatcher;

    public function __construct(EventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        //$url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);


        $this->eventDispatcher->dispatch(HgEvents::USER_REGISTRATION, new UserEvent($user));
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {
        //$url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);

        $this->eventDispatcher->dispatch(HgEvents::USER_PASSWORD_RESET, new UserEvent($user));

    }

}
