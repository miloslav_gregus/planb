<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Planb;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * User listener : redirect user depending on his informations
 */
class Credit
{
    /**
     * @var User
     */
    protected $em;


    /**
     * Constructor
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function recalculateUser(User $user){

        if(!$user->isType(User::TYPE_INVESTOR)) return;

        $em = $this->em;

        $creditAuctions = $em->getRepository("AppBundle:Investment")->sumByUser($user);
        $creditWithdraw = $em->getRepository("AppBundle:Transaction")->sumWithdrawByUser($user);
        $creditFree = $em->getRepository("AppBundle:Transaction")->sumFreeByUser($user);
        $creditLoans = $em->getRepository("AppBundle:Transaction")->sumLoansByUser($user);

        $creditAll = $creditAuctions + $creditWithdraw + $creditFree + $creditLoans;

        $user->setCreditWithdraw($creditWithdraw);
        $user->setCreditAuctions($creditAuctions);
        $user->setCreditFree($creditFree);
        $user->setCreditLoans($creditLoans);
        $user->setCreditAll($creditAll);

        $this->flush();
    }

    public function addWithdrawUser($amount, User $user){
        $user->addCreditWithdraw($amount);
        $user->addCreditFree(-$amount);
        $this->flush();
    }

    public function acceptWithdrawUser($amount, User $user){
        $user->addCreditWithdraw(-$amount);
        $user->addCreditAll(-$amount);
        $this->flush();
    }
    public function rejectWithdrawUser($amount, User $user){
        $user->addCreditWithdraw(-$amount);
        $user->addCreditFree($amount);
        $this->flush();
    }

    public function addAuctionsUser($amount, User $user){
        $user->addCreditAuctions($amount);
        $user->addCreditFree(-$amount);
        $this->flush();
    }

    public function rejectAuctionsUser($amount, User $user){
        $user->addCreditAuctions(-$amount);
        $user->addCreditFree(+$amount);
        $this->flush();
    }

    public function addInterestUser($amount, User $user){
        $user->addCreditFree($amount);
        $user->addCreditAll($amount);
    }
    public function addPrincipalUser($amount, User $user){
        $user->addCreditFree($amount);
        $user->addCreditLoans(-$amount);
    }

    public function addLoanUser($amount, User $user){
        $user->addCreditLoans($amount);
        $user->addCreditAuctions(-$amount);
        $this->flush();
    }

    public function subLoanUser($amount, User $user){
        $user->addCreditLoans(-$amount);
        $user->addCreditFree($amount);
        $this->flush();
    }

    public function acceptFreeUser($amount, User $user){
        $user->addCreditAll($amount);
        $user->addCreditFree($amount);
        $this->flush();
    }
    public function subFreeUser($amount, User $user){
        $user->addCreditAll(-$amount);
        $user->addCreditFree(-$amount);
        $this->flush();
    }


    public function flush(){
        $this->em->flush();
    }

}