<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Planb;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\User;

/**
 * User listener : redirect user depending on his informations
 */
class Calculations
{
    /**
     * @var User
     */
    protected $em;

    /**
     * Constructor
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function recalculateInvestmentTransactionsSummary(Investment $investment){

        $loan = $investment->getLoan();
        $loanStartAt = $loan->getLoanStartAt();
        $repaymentDay = $loan->getRepaymentDay();
        $repaymentDate = $this->closestPossibleRepaymentDay($loanStartAt, $repaymentDay);


        $investmentAmount = $investment->getAmount();
        $loanMaturity = $loan->getLoanMaturity();
        $investmentInterest = FinancialFunctions::monthlyInterest($investment->getInterest());

        $transactions = array();
        $transactionsSummary = array(
            "principalTotal" => 0,
            "interestTotal" => 0,
            "repaymentTotal" => 0,
        );
        for($month = 1; $month <= $loanMaturity; $month++){

            $principal = FinancialFunctions::PPMT($investmentInterest, $month, $loanMaturity, $investmentAmount);
            $interest = FinancialFunctions::IPMT($investmentInterest, $month, $loanMaturity, $investmentAmount);
            $repayment = $interest + $principal;
            $date = clone $repaymentDate;
            $date->modify("+5 days");

            if($month == $loanMaturity){
                $principal = $investmentAmount - $transactionsSummary['principalTotal'];
            }

            $transactions[] = array(
                "month" => $month,
                "date" => $date,
                "principal" => $principal,
                "interest" => $interest,
                "repayment" => $repayment,
            );

            $transactionsSummary['principalTotal'] += $principal;
            $transactionsSummary['interestTotal'] += $interest;
            $transactionsSummary['repaymentTotal'] += round($repayment, 2);

            $repaymentDate->modify("+1 month");
        }


        return array(
            "transactions" => $transactions,
            "transactionsSummary" => $transactionsSummary
        );

    }


    public function recalculateLoanTransactionsSummary(Loan $loan){

        $loanStartAt = $loan->getLoanStartAt();
        $repaymentDay = $loan->getRepaymentDay();
        $repaymentDate = $this->closestPossibleRepaymentDay($loanStartAt, $repaymentDay);

        $summary = $this->recalculateSummary($loan);

        $loanAmount = $summary['amount'];
        $loanMaturity = $summary['maturity'];
        $loanInterest = $summary['monthlyInterest'];

        $transactions = array();
        $transactionsSummary = array(
            "principalTotal" => 0,
            "interestTotal" => 0,
            "repaymentTotal" => 0,
        );
        for($month = 1; $month <= $loanMaturity; $month++){

            $principal = FinancialFunctions::PPMT($loanInterest, $month, $loanMaturity, $loanAmount);
            $interest = FinancialFunctions::IPMT($loanInterest, $month, $loanMaturity, $loanAmount);
            $repayment = $interest + $principal;

            $transactions[] = array(
                "month" => $month,
                "date" => clone $repaymentDate,
                "principal" => $principal,
                "interest" => $interest,
                "repayment" => $repayment,
            );
            $transactionsSummary['principalTotal'] += $principal;
            $transactionsSummary['interestTotal'] += $interest;
            $transactionsSummary['repaymentTotal'] += round($repayment, 2);

            $repaymentDate->modify("+1 month");
        }

        return array(
            "transactions" => $transactions,
            "transactionsSummary" => $transactionsSummary
        );
    }

    /*
     *
     * @return \DateTime
     */
    public function closestPossibleRepaymentDay(\DateTime $loanStartAt, $repaymentDay){

        $firstDayOfRepayment = clone $loanStartAt;
        $firstDayOfRepayment->add(new \DateInterval('P28D'));
        if($repaymentDay < $firstDayOfRepayment->format("d")){
            $firstDayOfRepayment->setDate(
                $firstDayOfRepayment->format("Y"),
                (int) $firstDayOfRepayment->format("m") + 1,
                $firstDayOfRepayment->format($repaymentDay)
            );
        }else{
            $firstDayOfRepayment->setDate(
                $firstDayOfRepayment->format("Y"),
                $firstDayOfRepayment->format("m"),
                $firstDayOfRepayment->format($repaymentDay)
            );
        }

        return $firstDayOfRepayment;
    }

    public function calculateLoanMonthlyPrincipal(Loan $loan, $month) {
        $amount = $loan->getAuctionApprovedAmount();
        $maturity = $loan->getLoanMaturity();
        $interest = $loan->getAuctionInterestAverage();

        return FinancialFunctions::PPMT($interest, $month,$maturity, $amount);
    }

    public function recalculateLoanMonthlyRepayment(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_PREPARED)){
        /** @var Investment $investment */
        if (!count($investments = $loan->getInvestments())) {
            return 0;
        }

        $pmtTotal = 0;

        $months = $loan->getLoanMaturity();
        foreach ($investments AS $investment) {
            if ($investment->isStatus($status)) {

                $amount = $investment->getAmount();
                $interest = FinancialFunctions::monthlyInterest($investment->getInterest());
                $pmtTotal += FinancialFunctions::PMT($interest, $months, $amount);
            }
        }

        return $pmtTotal;
    }

    public function recalculateLoanInterestAverage(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED))
    {
        /** @var Investment $investment */
        if (!count($investments = $loan->getInvestments())) {
            $loan->setAuctionInterestAverage($loan->getAuctionInterestRecommemded());
            return;
        }


        $amountTotal = 0;
        $weightTotal = 0;
        foreach ($investments AS $investment) {
            if ($investment->isStatus($status)) {
                $amount = $investment->getAmount();
                $interest = $investment->getInterest();

                $amountTotal += $amount;
                $weightTotal += $amount * $interest;
            }
        }

        $amountTotal = $amountTotal == 0 ? 1 : $amountTotal;

        return $weightTotal / $amountTotal;
    }

    public function recalculateLoanCollectedAmount(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED))
    {
        $amountTotal = 0;

        $investments = $loan->getInvestments();

        foreach ($investments AS $investment) {
            if ($investment->isStatus($status)) {
                $amount = $investment->getAmount();
                $amountTotal += $amount;
            }
        }

        return $amountTotal;
    }

    public function recalculateLoanCollectedPercentage(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED))
    {
        $amountTotal = 0;

        $investments = $loan->getInvestments();

        foreach ($investments AS $investment) {
            if ($investment->isStatus($status)) {
                $amount = $investment->getAmount();
                $amountTotal += $amount;
            }
        }

        return $amountTotal / $loan->getAuctionApprovedAmount();
    }

    public function recalculateLoanFee(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED))
    {
        $fee = round($this->recalculateLoanCollectedAmount($loan, $status) * 0.05, 2);
        $fee = $fee < 30 ? 30 : $fee;
        return $fee;
    }

    public function recalculateLifebuoyFee(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED))
    {
        $fee = round($this->recalculateLoanCollectedAmount($loan, $status)) * ($loan->getAuctionRating() * 0.005);
        return $fee;
    }


    public function recalculateAdminFee(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED))
    {
        $fee = round($this->recalculateLoanCollectedAmount($loan, $status)) * 0.005;
        return $fee;
    }

    public function recalculateLoan(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED), $flush = true)
    {
        $loan->setAuctionInterestAverage($this->recalculateLoanInterestAverage($loan, $status));
        $loan->setAuctionCollectedAmount($this->recalculateLoanCollectedAmount($loan, $status));
        $loan->setAuctionCollectedPercentage($this->recalculateLoanCollectedPercentage($loan, $status));

        if($flush){
            $this->flush();
        }
    }

    public function recalculateSummary(Loan $loan, $status = array(Investment::STATUS_INSERTED, Investment::STATUS_PREPARED))
    {
        $return = array();

        $return['amount'] = $this->recalculateLoanCollectedAmount($loan, $status);
        $return['percentage'] = $this->recalculateLoanCollectedPercentage($loan, $status);
        $return['interest'] = $this->recalculateLoanInterestAverage($loan, $status);
        $return['monthlyInterest'] = FinancialFunctions::monthlyInterest($return['interest']);
        $return['monthlyRepayment'] = $this->recalculateLoanMonthlyRepayment($loan, $status);
        $return['maturity'] = $loan->getLoanMaturity();
        $return['repaymentTotal'] = $return['monthlyRepayment'] * $return['maturity'];
        $return['fee'] = $this->recalculateLoanFee($loan, $status);
        $return['lifebuoy_fee'] = $this->recalculateLifebuoyFee($loan, $status);
        $return['amountTotal'] = $return['amount'] - $return['fee'] - $return['lifebuoy_fee'];
        $return['isAcceptable'] = $return['percentage'] > 0 && $return['percentage'] <= 1 ? true : false;

        return $return;
    }

    public function flush()
    {
        $this->em->flush();
    }

}