<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Planb;

use Hg\AppBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * User listener : redirect user depending on his informations
 */
class DocumentsGenerator
{
    /** @var \Twig_Environment */
    private $twig;
    /** @var User */
    private $user;
    private $type;
    private $kernelRootDir;


    /**
     * Constructor
     */
    public function __construct(\Twig_Environment $twig,  SecurityContext $securityContext, $kernelRootDir)
    {
        $this->twig = $twig;
        $this->user = $securityContext->getToken()->getUser();
        $this->kernelRootDir = $kernelRootDir;
        define("_MPDF_TTFONTPATH", __DIR__ . "/fonts/");
    }


    public function generate($templateName, $data = array())
    {
        $template = $this->twig->render('AppBundle:pdf:' . $templateName . '.html.twig', $data);
        $header = $this->twig->render('AppBundle:pdf:header.html.twig', array());
        $footer = $this->twig->render('AppBundle:pdf:footer.html.twig', array());
        $mPDF = new \mPDF('', 'A4', 0, "opensans", 12, 12, 32, 24, 9, 9);
        $mPDF->fontdata["opensans"] = array(
            "R" => "OpenSans-Regular.ttf",
            "B" => "OpenSans-Bold.ttf",
            "I" => "OpenSans-Italic.ttf",
            "BI" => "OpenSans-BoldItalic.ttf",
        );
        $mPDF->SetHTMLHeader($header);
        $mPDF->SetHTMLFooter($footer);
        $mPDF->SetAuthor('planb.sk');
        $mPDF->WriteHTML($template);

        return $mPDF->Output('', 'S');
    }

}