<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Planb;

use Hg\AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * User listener : redirect user depending on his informations
 */
class EmailHandler
{
    /**
     * @var User
     */
    private $user;
    private $twig;
    private $mailer;
    private $templateName;


    /**
     * Constructor
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }


    public function send($templateName, User $user = null, $data, $fromEmail = "planb@planb.sk"){

        $this->setTemplateName($templateName);
        $this->setUser($user);

        $data['user_type'] = $this->getUserType();
        $data['basecolor'] = $this->getUserType() == "investor" ? "#22bbea" : "#8dc03c";


        $data = $this->twig->mergeGlobals($data);
        $template = $this->twig->loadTemplate($this->getTemplate());
        $subject = $template->renderBlock('subject', $data);
        $htmlBody = $template->renderBlock('body_html', $data);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($this->getToEmail())
        ;



        $message->setBody($htmlBody, 'text/html');

        if(isset($data['files']) && count($data['files'])){

            foreach($data['files'] AS $file){
                $attachment = \Swift_Attachment::fromPath($file->getAbsolutePath())
                    ->setFilename($file->getNiceFilename());

                $message->attach($attachment);
            }
        }

        $this->mailer->send($message);


    }

    public function getTemplate(){
        return "AppBundle:email:" . $this->templateName . ".html.twig";
    }

    /**
     * @return \Hg\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getToEmail()
    {
        if( $this->user instanceof User ) {
            return $this->user->getEmail();
        }
        return "planb@planb.sk";
    }

    /**
     * @param \Hg\AppBundle\Entity\User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     *
     */
    public function getUserType()
    {
        if( $this->user instanceof User ) {
            return $this->user->getType();
        }
        return "investor";
    }

    /**
     * @param mixed $templateName
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
    }


}