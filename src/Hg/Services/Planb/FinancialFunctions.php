<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Planb;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * User listener : redirect user depending on his informations
 */
class FinancialFunctions
{

    public static function interest($months, $payment, $amount)
    {
        if ($amount <= 0) return;

        // make an initial guess
        $error = 0.0000001;
        $high = 1.00;
        $low = 0.00;
        $rate = (2.0 * ($months * $payment - $amount)) / ($amount * $months);

        while (true) {
            // check for error margin
            $calc = pow(1 + $rate, $months);
            $calc = ($rate * $calc) / ($calc - 1.0);
            $calc -= $payment / $amount;

            if ($calc > $error) {
                // guess too high, lower the guess
                $high = $rate;
                $rate = ($high + $low) / 2;
            } elseif ($calc < -$error) {
                // guess too low, higher the guess
                $low = $rate;
                $rate = ($high + $low) / 2;
            } else {
                // acceptable guess
                break;
            }
        }

        return $rate * 12;
    }


    /**
     * Future value interest factor of annuities
     *
     *                   nper
     *          (1 + rate)    - 1
     * FVIFA = -------------------
     *               rate
     *
     * @param  float   $rate is the interest rate per period.
     * @param  integer $nper is the total number of periods.
     * @return float  the present value interest factor of annuities
     */
    public static function _calculate_fvifa ($rate, $nper)
    {
        // Removable singularity at rate == 0
        if ($rate == 0)
            return $nper;
        else
            // FIXME: this sucks for very small rates
            return (pow(1 + $rate, $nper) - 1) / $rate;
    }

    /**
     * Present value interest factor
     *
     *                 nper
     * PVIF = (1 + rate)
     *
     * @param  float   $rate is the interest rate per period.
     * @param  integer $nper is the total number of periods.
     * @return float  the present value interest factor
     */
    public static function _calculate_pvif ($rate, $nper)
    {
        return (pow(1 + $rate, $nper));
    }

    public static function _calculate_pmt ($rate, $nper, $pv, $fv, $type)
    {
        // Calculate the PVIF and FVIFA
        $pvif = self::_calculate_pvif ($rate, $nper);
        $fvifa = self::_calculate_fvifa ($rate, $nper);

        return ((-$pv * $pvif - $fv ) / ((1.0 + $rate * $type) * $fvifa));
    }

    /**
     * PMT
     * Calculates the payment for a loan based on constant payments
     * and a constant interest rate.
     *
     * For a more complete description of the arguments in PMT, see the PV function.
     *
     * Rate: is the interest rate for the loan.
     * Nper: is the total number of payments for the loan.
     * Pv: is the present value, or the total amount that a series of future payments
     *  is worth now; also known as the principal.
     * Fv: is the future value, or a cash balance you want to attain after the last
     *  payment is made. If fv is omitted, it is assumed to be 0 (zero), that is,
     *  the future value of a loan is 0.
     * Type: is the number 0 (zero) or 1 and indicates when payments are due.
     *  0 or omitted, At the end of the period
     *  1, At the beginning of the period
     *
     * If rate = 0:
     *        -(FV + PV)
     * PMT = ------------
     *           nper
     *
     * Else
     *
     *                                      nper
     *                   FV + PV * (1 + rate)
     * PMT = --------------------------------------------
     *                             /             nper \
     *                            | 1 - (1 + rate)     |
     *        (1 + rate * type) * | ------------------ |
     *                             \        rate      /
     *
     */
    public static function PMT($rate, $nper, $pv, $fv = 0.0, $type = 0)
    {
        $pmt = self::_calculate_pmt ($rate, $nper, $pv, $fv, $type);
        return -(is_finite($pmt) ? $pmt: null);
    }

    /**
     * IPMT
     * Returns the interest payment for a given period for an investment based
     * on periodic, constant payments and a constant interest rate.
     *
     * For a more complete description of the arguments in IPMT, see the PV function.
     *
     */
    public static function IPMT($rate, $per, $nper, $pv, $fv = 0.0, $type = 0)
    {
        if (($per < 1) || ($per >= ($nper + 1)))
            return null;
        else {
            $pmt = self::_calculate_pmt ($rate, $nper, $pv, $fv, $type);
            $ipmt = self::_calculate_interest_part ($pv, $pmt, $rate, $per - 1);
            return -(is_finite($ipmt) ? $ipmt: null);
        }
    }

    public static function _calculate_interest_part ($pv, $pmt, $rate, $per)
    {
        return -($pv * pow(1 + $rate, $per) * $rate +
            $pmt * (pow(1 + $rate, $per) - 1));
    }

    /**
     * PPMT
     * Returns the payment on the principal for a given period for an
     * investment based on periodic, constant payments and a constant
     * interest rate.
     *
     */
    public static function PPMT($rate, $per, $nper, $pv, $fv = 0.0, $type = 0)
    {

        if (($per < 1) || ($per >= ($nper + 1))){

            return null;
        }else {
            $pmt = self::_calculate_pmt ($rate, $nper, $pv, $fv, $type);
            $ipmt = self::_calculate_interest_part ($pv, $pmt, $rate, $per - 1);
            return -((is_finite($pmt) && is_finite($ipmt)) ? $pmt - $ipmt: null);
        }
    }

    /*
     * Monthly interest
     */
    public static function monthlyInterest($interest){
       return (pow((1+$interest),(1/12))-1);
    }

    /*
     * Repayment amount
     */
    public static function repaymentAmount($pmt, $months)
    {
        return $pmt * $months;
    }

    /*
     * Repayment amount from pthm
     */
    public static function repaymentTotalAmount($interest, $months, $PV)
    {
        $pmt = self::pmt($interest, $months, $PV);
        return $pmt * $months;
    }

}