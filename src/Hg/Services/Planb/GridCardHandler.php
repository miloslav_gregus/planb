<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Planb;

use Hg\AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * User listener : redirect user depending on his informations
 */
class GridCardHandler
{
    /**
     * @var User
     */
    private $user;
    private $session;
    private $label;


    /**
     * Constructor
     */
    public function __construct(SessionInterface $session, SecurityContext $securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
        $this->session = $session;
    }

    public function checkValue($value){
        $position = $this->getPosition();

        return $this->checkValidateHash($value, $position);
    }

    public function checkValidateHash($value, $grid){
        return $this->user->checkValidateHash($value, $grid);
    }

    public function getPosition() {
        if(!($position = $this->session->get("validationCodePosition"))){
            $position = $this->generateNewPosition();
        }
        return $position;
    }

    public function generateNewPosition(){

        $number = rand(1,10);

        $letters = array("A","B","C","D","E","F","G","H","I","J");
        $letter = $letters[array_rand($letters, 1)];

        $position = $letter . $number;

        $this->session->set("validationCodePosition", $position);

        return $position;
    }

    public function setLabel($label){
        $this->label = $label;
        return $this;
    }


    public function __toString(){
        $this->generateNewPosition();
        return $this->label . " " . $this->getPosition();
    }


}