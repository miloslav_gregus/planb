<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 4.11.2013
 * Time: 5:21
 */

namespace Hg\Services\Listener;

use Hg\AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * User listener : redirect user depending on his informations
 */
class UserListener
{
    /**
     * @var SecurityContext
     */
    private $securityContext;


    /**
     * Constructor
     *
     * @param SecurityContext $securityContext
     * @param Router $router
     */
    public function __construct(SecurityContext $securityContext, Router $router)
    {
        $this->securityContext = $securityContext;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {

        if ($this->securityContext->getToken()
            && $this->securityContext->isGranted("ROLE_USER")
            && HttpKernel::MASTER_REQUEST == $event->getRequestType()
        ) {
            $user = $this->securityContext->getToken()->getUser();

            if($user->getDoneAlready()){
                return;
            }

            $route = $event->getRequest()->get('_route');

            if ($user->isType(User::TYPE_INVESTOR)) {
                if (!in_array($route, array(
                    'fos_user_investor_profile_registration',
                    'fos_user_registration_confirmed',
                    'fos_user_investor_profile_registration_success',
                    'hg_app_uploads_profile ',
                    'hg_app_contact',
                    'hg_app_page',
                    "_profiler",
                    "_wdt"
                ))
                ) {
                    $event->setResponse(new RedirectResponse($this->router->generate('fos_user_investor_profile_registration')));
                }
            }
            if ($user->isType(User::TYPE_CUSTOMER)) {

                if (!in_array($route, array(
                    'fos_user_investor_profile_registration_success',
                    'fos_user_registration_confirmed',
                    'fos_user_profile_index',
                    'fos_user_profile_pending',
                    'fos_user_profile_loan_validate',
                    'fos_user_profile_auction_validate',
                    'fos_user_profile_auction_validate_success',
                    'hg_app_uploads_loan',
                    'fos_user_profile_upload',
                    'hg_app_contact',
                    'hg_app_page',
                    "_profiler",
                    "_wdt"
                ))
                ) {
                    $event->setResponse(new RedirectResponse($this->router->generate('fos_user_profile_index')));
                }
            }
        }
    }
}