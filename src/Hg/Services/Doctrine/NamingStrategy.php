<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 5.11.2013
 * Time: 19:35
 */

namespace Hg\Services\Doctrine;


use Doctrine\ORM\Mapping\DefaultNamingStrategy as BaseDefaultNamingStrategy;
use Doctrine\Common\Util\Inflector;

class NamingStrategy extends BaseDefaultNamingStrategy
{
    /**
     * {@inheritdoc}
     */
    public function classToTableName($className)
    {
        return Inflector::tableize(parent::classToTableName($className));
    }

    /**
     * {@inheritdoc}
     */
    public function propertyToColumnName($propertyName)
    {
        return Inflector::tableize(parent::propertyToColumnName($propertyName));
    }

    /**
     * {@inheritdoc}
     */
    public function joinColumnName($propertyName)
    {
        return Inflector::tableize($propertyName) . '_' . $this->referenceColumnName();
    }
}