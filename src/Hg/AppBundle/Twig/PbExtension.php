<?php

namespace Hg\AppBundle\Twig;

use Symfony\Component\Validator\Constraints\DateTime;

class PbExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            'age' => new \Twig_Filter_Method($this, 'ageFilter'),
            'countdown' => new \Twig_Filter_Method($this, 'countdownFilter'),
            'interest' => new \Twig_Filter_Method($this, 'interestFilter'),
            'currency' => new \Twig_Filter_Method($this, 'currencyFilter'),
            'percentage' => new \Twig_Filter_Method($this, 'percentageFilter'),
        );
    }

    public function ageFilter($datetime) {
        $now = new \DateTime();
        $interval = $now->diff($datetime);
        return $interval->format('%Y');
    }

    public function countdownFilter($datetime, $days = "dní") {
        $now = new \DateTime();
        if($now >= $datetime){
            $interval = $now->diff($now);
        }else{
            $interval = $now->diff($datetime);
        }
        return $interval->format('<strong><span class="days">%d</span> ' . $days . ' </strong><span class="hours">%H</span>:<span class="minutes">%I</span>:<span class="seconds">%S</span>');
    }

    public function interestFilter($text,  $decimals = 2,$symbol = " % p.a.", $decPoint = ",", $thousansPoint = " ") {
        $text = $this->percentageFilter($text, $decimals, "");
        $number = number_format($text,$decimals, $decPoint, $thousansPoint);
        if($symbol) {
            $number = $number . $symbol;
        }
        return $number;
    }

    public function currencyFilter($text, $decimals = 2, $currency = "€" ,$decPoint = ",", $thousantsPoint = " ") {
        $number = number_format($text,$decimals, $decPoint, $thousantsPoint);
        if($currency) {
            $number = $number . " " . $currency;
        }
        return $number;
    }

    public function percentageFilter($text, $decimal = 2 ,$char = "%") {
        $text = $text * 100;
        return round($text, $decimal) . $char;
    }



    public function getName() {
        return 'pb_extension';
    }
}
