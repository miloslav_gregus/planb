<?php

namespace Hg\AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DateStringTransformer implements DataTransformerInterface
{

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  \DateTime $value
     * @return string
     */
    public function transform($value)
    {
        if(!$value) $value = new \DateTime();
        return $value->format("Y-m-d");
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $value
     *
     * @return \DateTime
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($value)
    {
        return new \DateTime($value);
    }
} 