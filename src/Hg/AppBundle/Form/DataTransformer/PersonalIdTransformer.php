<?php

namespace Hg\AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class PersonalIdTransformer implements DataTransformerInterface
{

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Issue|null $value
     * @return string
     */
    public function transform($value)
    {
        $number = preg_replace('/[^0-9+]/', '', $value);

        return $number;
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $number
     *
     * @return Issue|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($value)
    {
        $number = preg_replace('/[^0-9+]/', '', $value);

        return $number;
    }
} 