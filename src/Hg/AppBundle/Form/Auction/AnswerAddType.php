<?php

namespace Hg\AppBundle\Form\Auction;

use Hg\AppBundle\Entity\Investment;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class AnswerAddType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', "textarea", array(
            "label" => "comments.answer_add",
        ));
        $builder->add('submit', "submit", array(
            "label" => "comments.answer_add_submit",
            "attr" => array("class" => "btn-customer")
        ));
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Comment',
         //   'csrf_protection' => false,
            "attr" => array(
                "id" => "form-answer-add"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_answer_add';
    }
}
