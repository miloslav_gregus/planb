<?php

namespace Hg\AppBundle\Form\Auction;

use Hg\AppBundle\Entity\Investment;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

class InvestmentAddType extends AbstractType
{
    public $gridCardHandler;
    public $translator;

    public function __construct(GridCardHandler $gridCardHandler, $translator)
    {
        $this->gridCardHandler = $gridCardHandler;
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $investmentAddType = $this;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($investmentAddType){

            $form = $event->getForm();
            $data = $event->getData();

            $max = $investmentAddType->calculateMaxAmount($data);
            $gridCardHandler = $investmentAddType->gridCardHandler;
            $translator = $investmentAddType->translator;

            /** @var Investment $data */
            $form->add('amount', "text", array(
                "label" => "investments.amount",
                'widget_addon_append' => array(
                    'text' => '€',
                ),
                "attr" => array(
                    "data-min" => "25",
                    "data-max" => $max
                ),
                'help_block' => $investmentAddType->translator->trans("investments.amount_help", array("%from%" => 25, "%to%" => $max)),
                "data" => 25,
                "constraints" => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        "min" => 25,
                        "max" => $max,
                    ))
                )
            ));

            $form->add('interest', "percent", array(
                "label" => "investments.interest",
                "help_block" => $investmentAddType->translator->trans("investments.interest_help", array(
                            "%interest%" => $data->getLoan()->getAuctionInterestRecommemded(true))
                    ),
                "precision" => 2,
                'widget_addon_append' => array(
                    'text' => '%',
                ),
                "attr" => array(
                    "data-min" => "0",
                    "data-max" => "29.99"
                ),
                "data" =>  $data->getLoan()->getAuctionInterestRecommemded(),
                "constraints" => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        "min" => 0,
                        "max" => 0.2999,
                        "maxMessage" => "Max interest rate is 29,99%"
                    ))
                )
            ));


            $validationCodeLabel = $translator->trans("investments.grid_card");

            $form->add('validationCode',"text", array(
                "label" => $gridCardHandler->setLabel($validationCodeLabel),
                'widget_addon_append' => array(
                    'text' => '<span class="icon-key"></span>',
                ),
                "mapped" => false,
                "constraints" => array(
                    new Assert\NotBlank(),
                    new HgAssert\ValidationCode(array(), $investmentAddType->gridCardHandler),
                )
            ));

            $form->add("terms", "checkbox", array(
                "mapped" => false,
                "widget_type" => 'inline',
                "label" => "investments.terms",
                "constraints" => new Assert\True(array(
                            "message" => "investments.terms_error")
                    )
            ));


            $form->add('maturity',"hidden", array(
                "mapped" => false,
                "data" => $data->getLoan()->getLoanMaturity()
            ));

            $form->add('submit', "submit", array(
                "label" => "investments.investment_add_submit",
                "attr" => array("class" => "btn-investor")
            ));
        });

    }

    public function calculateMaxAmount(Investment $data)
    {

        $loan = $data->getLoan();
        $user = $data->getUser();

        $maxAmount = $loan->getAuctionApprovedAmount() * 1;

        //$maxAmount = $maxAmount > 500 ? 500 : $maxAmount;

        $maxAmount = $user->getCreditFree() < $maxAmount ? $user->getCreditFree() : $maxAmount;

        $maxAmount = floor($maxAmount);

        return $maxAmount;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Investment',
            "attr" => array(
                "id" => "form-investment-add"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_investment_add';
    }
}
