<?php

namespace Hg\AppBundle\Form\Contact;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

class ContactType extends AbstractType
{
    private $user;

    public function __construct(SecurityContext $securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$this->user || $this->user == "anon.") {
            $builder->add('username', null, array(
                "label" => "contact.username"
            ))
            ->add('email', null, array(
                "label" => "contact.email",
            ));
        }


        $builder
            ->add('subject', null, array(
                "label" => "contact.subject"
            ))
            ->add('text', null, array(
                "label" => "contact.text",
            ))
            ->add('submit', "submit", array(
                "label" => "contact.submit",
                "attr" => array("class" => "btn-brand")
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hg\AppBundle\Entity\Contact',
            'validation_groups' => "contact_logged",
            "attr" => array(
                "id" => "form-contact"
            )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_contact';
    }
}
