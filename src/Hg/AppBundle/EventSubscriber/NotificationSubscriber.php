<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:18
 */

namespace Hg\AppBundle\EventSubscriber;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Notification;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Event\ContactEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\LoanFileEvent;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\HgEvents;
use Hg\Services\Planb\EmailHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NotificationSubscriber implements EventSubscriberInterface {

    private $em;
    private $emailHandler;

    public function __construct(EntityManager $em, EmailHandler $emailHandler) {
        $this->em = $em;
        $this->emailHandler = $emailHandler;
    }

    public static function getSubscribedEvents()
    {
        return array(
            HgEvents::AUCTION_DRAFT_REQUESTED => array('onAuctionDraftRequested', 0),
            HgEvents::AUCTION_AUCTION_REQUESTED => array('onAuctionAuctionRequested', 0),
            HgEvents::FILE_PENDING => array('onFilePending', 0),
            HgEvents::AUCTION_AUCTION_LAUNCH => array('onAuctionLaunch', 0),
            HgEvents::AUCTION_CANCELED => array('onAuctionCanceled', 0),
            HgEvents::AUCTION_OVERDUE => array('onAuctionOverdue', 0),
            HgEvents::AUCTION_ACCEPTED => array('onAuctionAccepted', 0),
            HgEvents::TRANSACTION_ADD => array('onTransactionAdd', 0),
            HgEvents::TRANSACTION_REPAYMENT_ADMIN_NOTIFY => array('onRepaymentAdminNotify', 0),
            HgEvents::CONTACT => array('onContact', 0),
        );
    }

    public function onAuctionDraftRequested(LoanEvent $event)
    {
        $notification = $this->getNotificationObject(Notification::TYPE_AUCTION_DRAFT_REQUESTED);
        $notification->setLoan($event->getLoan());
        $this->em->flush();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->sendToAdmin("auction_draft_request", $array);
    }

    public function onAuctionAuctionRequested(LoanEvent $event){
        $notification = $this->getNotificationObject(Notification::TYPE_AUCTION_AUCTION_REQUESTED);
        $notification->setLoan($event->getLoan());
        $this->em->flush();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->sendToAdmin("auction_auction_requested", $array);
    }

    public function onFilePending($event){

        if($event instanceof LoanFileEvent){
            $notification = $this->getNotificationObject(Notification::TYPE_FILE);
            $notification->setLoan($event->getLoan());
            $notification->setLoanFile($event->getLoanFile());
            $array = array(
                "user" => $event->getLoan()->getUser(),
                "loan" => $event->getLoan(),
                "file" => $event->getLoanFile(),
            );
            $this->sendToAdmin("file_pending_loan", $array);
            $this->em->flush();
        }
        if($event instanceof ProfileFileEvent){
            $notification = $this->getNotificationObject(Notification::TYPE_FILE);
            $notification->setUser($event->getProfile()->getUser());
            $notification->setProfileFile($event->getProfileFile());
            $array = array(
                "user" => $event->getProfile()->getUser(),
                "file" => $event->getProfileFile(),
            );
            $this->sendToAdmin("file_pending_profile", $array);
            $this->em->flush();
        }

    }

    public function onAuctionLaunch(LoanEvent $event){
        $notification = $this->getNotificationObject(Notification::TYPE_AUCTION_LAUNCH);
        $notification->setLoan($event->getLoan());
        $this->em->flush();

        $array = array(
            "loan" => $event->getLoan(),
        );
        $this->sendToAdmin("auction_launch", $array);
    }

    public function onAuctionAccepted(LoanEvent $event){
        $notification = $this->getNotificationObject(Notification::TYPE_AUCTION_ACCEPTED);
        $notification->setLoan($event->getLoan());
        $this->em->flush();

        $array = array(
            "loan" => $event->getLoan(),
        );
        $this->sendToAdmin("auction_accepted", $array);
    }

    public function onAuctionCanceled(LoanEvent $event){
        $notification = $this->getNotificationObject(Notification::TYPE_AUCTION_CANCELED);
        $notification->setLoan($event->getLoan());
        $this->em->flush();

        $array = array(
            "loan" => $event->getLoan(),
        );
        $this->sendToAdmin("auction_canceled", $array);
    }

    public function onAuctionOverdue(LoanEvent $event){
        $notification = $this->getNotificationObject(Notification::TYPE_AUCTION_OVERDUE);
        $notification->setLoan($event->getLoan());
        $this->em->flush();

        $array = array(
            "loan" => $event->getLoan(),
        );
        $this->sendToAdmin("auction_overdue", $array);
    }


    public function onTransactionAdd(TransactionEvent $event){
        $transaction = $event->getTransaction();
        $notification = $this->getNotificationObject(Notification::TYPE_TRANSACTION);
        $notification->setTransaction($transaction);
        $this->em->flush();

        $array = array(
            "transaction" => $transaction,
        );

        switch ($transaction->getType()) {
            case $transaction::TYPE_DEPOSIT:
                $this->sendToAdmin("transaction_deposit_add", $array);
                break;
            case $transaction::TYPE_WITHDRAW:
                $this->sendToAdmin("transaction_withdraw_add", $array);
                break;
        }
    }

    public function onRepaymentAdminNotify(TransactionEvent $event){
        $transaction = $event->getTransaction();
        $notification = $this->getNotificationObject(Notification::TYPE_TRANSACTION);
        $notification->setTransaction($transaction);
        $this->em->flush();

        $array = array(
            "transaction" => $transaction,
        );

        $this->sendToAdmin("transaction_repayment_notify", $array);
    }

    public function onContact(ContactEvent $event){
        $notification = $this->getNotificationObject(Notification::TYPE_CONTACT);
        $notification->setContact($event->getContact());
        $this->em->flush();
    }



    protected  function getNotificationObject($type){
        $notification = new Notification($type);
        $this->em->persist($notification);
        return $notification;
    }

    protected function sendToAdmin($template, $array){
        $this->emailHandler->send("admin_" . $template, null, $array);
    }
} 