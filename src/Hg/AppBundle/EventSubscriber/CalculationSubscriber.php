<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:18
 */

namespace Hg\AppBundle\EventSubscriber;

use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\HgEvents;
use Hg\Services\Planb\Calculations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CalculationSubscriber implements EventSubscriberInterface {

    private $calculations;

    public function __construct(Calculations $calculations) {
        $this->calculations = $calculations;
    }

    public static function getSubscribedEvents()
    {
        return array(
            HgEvents::AUCTION_INVESTMENT_ADD => array('onInvestmentAdd', 0),
            HgEvents::AUCTION_INVESTMENT_DELETE => array('onInvestmentDelete', 0),
            HgEvents::AUCTION_INVESTMENT_CANCEL => array('onInvestmentCancel', 0),
        );
    }

    public function onInvestmentAdd(InvestmentEvent $event)
    {
        $this->calculations->recalculateLoan($event->getInvestment()->getLoan());
    }

    public function onInvestmentDelete(InvestmentEvent $event)
    {
        $this->calculations->recalculateLoan($event->getInvestment()->getLoan());
    }

    public function onInvestmentCancel(InvestmentEvent $event)
    {
        $this->calculations->recalculateLoan($event->getInvestment()->getLoan());
    }

} 