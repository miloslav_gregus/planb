<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:18
 */

namespace Hg\AppBundle\EventSubscriber;

use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Notification;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\HgEvents;
use Hg\Services\Planb\Credit;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreditSubscriber implements EventSubscriberInterface
{

    private $credit;

    public function __construct(Credit $credit)
    {
        $this->credit = $credit;
    }

    public static function getSubscribedEvents()
    {
        return array(
            HgEvents::AUCTION_INVESTMENT_ADD => array('onInvestmentAdd', -255),
            HgEvents::AUCTION_INVESTMENT_DELETE => array('onInvestmentDelete', -255),
            HgEvents::AUCTION_INVESTMENT_CANCEL => array('onInvestmentCancel', -255),
            HgEvents::TRANSACTION_ADD => array('onTransactionAdd', -255),
            HgEvents::TRANSACTION_ACCEPTED => array('onTransactionAccepted', -255),
            HgEvents::TRANSACTION_REJECTED => array('onTransactionRejected', -255),
            HgEvents::AUCTION_LOAN => array('onAuctionLoan', 0),
        );
    }

    public function onInvestmentAdd(InvestmentEvent $event)
    {
        $investment = $event->getInvestment();
        $this->credit->addAuctionsUser($investment->getAmount(), $investment->getUser());
    }

    public function onInvestmentDelete(InvestmentEvent $event)
    {
        $investment = $event->getInvestment();
        $this->credit->rejectAuctionsUser($investment->getAmount(), $investment->getUser());

    }

    public function onInvestmentCancel(InvestmentEvent $event)
    {
        $investment = $event->getInvestment();
        $this->credit->rejectAuctionsUser($investment->getAmount(), $investment->getUser());
    }


    public function onTransactionAdd(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();

        switch ($transaction->getType()) {
            case $transaction::TYPE_WITHDRAW:
                $this->credit->addWithdrawUser($transaction->getAmount(), $transaction->getFromUser());
                break;
        }
    }

    public function onTransactionAccepted(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();

        switch ($transaction->getType()) {
            case $transaction::TYPE_DEPOSIT:
                $this->credit->acceptFreeUser($transaction->getAmount(), $transaction->getFromUser());
                break;
            case $transaction::TYPE_WITHDRAW:
                $this->credit->acceptWithdrawUser($transaction->getAmount(), $transaction->getFromUser());
                break;
            case $transaction::TYPE_INTEREST:
                $this->credit->addInterestUser($transaction->getAmount(), $transaction->getToUser());
                break;
            case $transaction::TYPE_PRINCIPAL:
                $this->credit->addPrincipalUser($transaction->getAmount(), $transaction->getToUser());
                break;
        }
    }

    public function onTransactionRejected(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();

        switch ($transaction->getType()) {
            case $transaction::TYPE_WITHDRAW:
                $this->credit->rejectWithdrawUser($transaction->getAmount(), $transaction->getFromUser());
                break;
        }

    }

    public function onAuctionLoan(LoanEvent $event)
    {
        $loan = $event->getLoan();

        /** @var Investment $investment */
        foreach ($loan->getInvestments() AS $investment) {
            switch ($investment->getStatus()) {
                case $investment::STATUS_PAID:
                    $this->credit->addLoanUser($investment->getAmount(), $investment->getUser());
                    break;
            }
        }
    }
} 