<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:18
 */

namespace Hg\AppBundle\EventSubscriber;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\HgEvents;
use Hg\Services\Planb\Calculations;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ManipulationSubscriber implements EventSubscriberInterface {

    private $dispatcher;
    private $em;
    private $calculations;

    public function __construct(EventDispatcher $dispatcher, EntityManager $em, Calculations $calculations) {
        $this->dispatcher = $dispatcher;
        $this->em = $em;
        $this->calculations = $calculations;
    }

    public static function getSubscribedEvents()
    {
        return array(
            HgEvents::AUCTION_CANCELED => array('onAuctionCanceled', 255),
            HgEvents::AUCTION_OVERDUE => array('onAuctionOverdue', 255),
        );
    }

    public function onAuctionOverdue(LoanEvent $event)
    {
        $loan = $event->getLoan();

        foreach ($loan->getInvestments() AS $investment){
            if($investment->isStatus(array(Investment::STATUS_LIMBO, Investment::STATUS_INSERTED))){
                $investment->setStatus(Investment::STATUS_CANCELED);

                $this->dispatcher->dispatch(HgEvents::AUCTION_INVESTMENT_CANCEL, new InvestmentEvent($investment));
            }
        }

        $loan->setStatus($loan::STATUS_OVERDUE);

        /**
         * odstranenie pripadnych niekdajsich transakcii
         */
        $transactions = $this->em->getRepository("AppBundle:Transaction")->findByLoan($loan);
        foreach ($transactions AS $transaction) {
            $this->em->remove($transaction);
        }

        $this->calculations->recalculateLoan($loan, array(Investment::STATUS_CANCELED));
        $loan->setAuctionEndAt(new \DateTime());
        $loan->getUser()->addAuctionsActive(-1)->addAuctionsInactive(1);

        $this->flush();
    }

    public function onAuctionCanceled(LoanEvent $event)
    {
        $loan = $event->getLoan();

        foreach ($loan->getInvestments() AS $investment){
            if($investment->isStatus(array(Investment::STATUS_LIMBO, Investment::STATUS_INSERTED, Investment::STATUS_PREPARED))){
                $investment->setStatus(Investment::STATUS_CANCELED);

                $this->dispatcher->dispatch(HgEvents::AUCTION_INVESTMENT_CANCEL, new InvestmentEvent($investment));
            }
        }

        /**
         * odstranenie pripadnych niekdajsich transakcii
         */
        $transactions = $this->em->getRepository("AppBundle:Transaction")->findByLoan($loan);
        foreach ($transactions AS $transaction) {
            $this->em->remove($transaction);
        }

        $loan->setStatus($loan::STATUS_CANCELED);
        $this->calculations->recalculateLoan($loan, array(Investment::STATUS_CANCELED));
        $loan->setAuctionEndAt(new \DateTime());
        $loan->getUser()->addAuctionsActive(-1)->addAuctionsInactive(1);

        $this->flush();
    }

    public function flush(){
        $this->em->flush();
    }

} 