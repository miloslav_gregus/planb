<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:18
 */

namespace Hg\AppBundle\EventSubscriber;

use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\ProfileFile;
use Hg\AppBundle\Event\AnswerEvent;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\LoanFileEvent;
use Hg\AppBundle\Event\ProfileFileEvent;
use Hg\AppBundle\Event\QuestionEvent;
use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\Event\UserEvent;
use Hg\AppBundle\HgEvents;
use Hg\Services\Planb\EmailHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailSubscriber implements EventSubscriberInterface {

    private $emailHandler;

    public function __construct(EmailHandler $emailHandler) {
        $this->emailHandler = $emailHandler;
    }

    public static function getSubscribedEvents()
    {
        return array(
            HgEvents::AUCTION_ANSWER_ADD => array('onAnswerAdd', 0),
            HgEvents::AUCTION_QUESTION_ADD => array('onQuestionAdd', 0),
            HgEvents::USER_REGISTRATION => array('onUserRegistration', 0),
            HgEvents::USER_PASSWORD_RESET => array('onUserPasswordReset', 0),
            HgEvents::AUCTION_DRAFT_REQUESTED => array('onAuctionDraftRequested', 0),
            HgEvents::AUCTION_DRAFT_APPROVED => array('onAuctionDraftApproved', 0),
            HgEvents::AUCTION_DRAFT_CANCELED => array('onAuctionDraftCanceled', 0),
            HgEvents::AUCTION_AUCTION_REQUESTED => array('onAuctionAuctionRequested', 0),
            HgEvents::AUCTION_AUCTION_SCORING => array('onAuctionAuctionScoring', 0),
            HgEvents::AUCTION_AUCTION_APPROVED => array('onAuctionAuctionApproved', 0),
            HgEvents::AUCTION_AUCTION_CANCELED => array('onAuctionAuctionCanceled', 0),
            HgEvents::AUCTION_ACCEPTED => array('onAuctionAccepted', 0),
            HgEvents::AUCTION_CANCELED => array('onAuctionCanceled', 0),
            HgEvents::AUCTION_OVERDUE => array('onAuctionOverdue', 0),
            HgEvents::AUCTION_PAYABLE => array('onAuctionPayable', 0),
            HgEvents::AUCTION_LOAN => array('onAuctionLoan', 0),
            HgEvents::FILE_APPROVE => array('onFileApprove', 0),
            HgEvents::FILE_REJECT => array('onFileReject', 0),
            HgEvents::FILE_DELIVERED => array('onFileDelivered', 0),
            HgEvents::FILE_MISSING => array('onFileMissing', 0),
            HgEvents::GRID_CARD_GENERATED => array('onGridCardGenerated', 0),
            HgEvents::AUCTION_AUCTION_LAUNCH => array('onAuctionLaunch', 0),
            HgEvents::AUCTION_INVESTMENT_ADD => array('onInvestmentAdd', 0),
            HgEvents::AUCTION_INVESTMENT_CANCEL => array('onInvestmentCancel', 0),
            HgEvents::AUCTION_INVESTMENT_DELETE => array('onInvestmentDelete', 0),
            HgEvents::AUCTION_INVESTMENT_PREPARED => array('onInvestmentPrepared', 0),
            HgEvents::INVESTOR_APPROVED => array('onInvestorApproved', 0),
            HgEvents::TRANSACTION_ADD => array('onTransactionAdd', 0),
            HgEvents::TRANSACTION_ACCEPTED => array('onTransactionAccepted', 0),
            HgEvents::TRANSACTION_REJECTED => array('onTransactionRejected', 0),
            HgEvents::TRANSACTION_REPAYMENT_PENDING => array('onTransactionRepaymentPending', 0),
            HgEvents::TRANSACTION_REPAYMENT_OVERDUE => array('onTransactionRepaymentOverdue', 0),
            HgEvents::INVESTOR_REGISTRATION => array('onInvestorRegistration', 0),
        );
    }

    public function onAnswerAdd(AnswerEvent $event)
    {
        $array = array(
            "comment" => $event->getComment()
        );
        $this->emailHandler->send("answer_add", $event->getComment()->getLoan()->getUser(), $array);
    }

    public function onQuestionAdd(QuestionEvent $event)
    {
        $array = array(
            "comment" => $event->getComment()
        );
        $this->emailHandler->send("question_add", $event->getComment()->getLoan()->getUser(), $array);
    }

    public function onUserRegistration(UserEvent $event)
    {
        $user = $event->getUser();
        $array = array(
            "user" => $user
        );

        if($user->isType($user::TYPE_CUSTOMER)){
            $this->emailHandler->send("registration_customer", $user, $array);
        }
        if($user->isType($user::TYPE_INVESTOR)){
            $this->emailHandler->send("registration_investor", $user, $array);
        }
    }
    public function onUserPasswordReset(UserEvent $event)
    {
        $user = $event->getUser();
        $array = array(
            "user" => $user
        );
        $this->emailHandler->send("password_reset", $user, $array);
    }



    public function onAuctionDraftRequested(LoanEvent $event)
    {
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan(),
            "files" => $event->getFiles()
        );
        $this->emailHandler->send("auction_draft_request",$user, $array);
    }


    public function onAuctionDraftApproved(LoanEvent $event)
    {
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_draft_approved",$user, $array);
    }

    public function onAuctionDraftCanceled(LoanEvent $event)
    {
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_draft_canceled",$user, $array);
    }


    public function onAuctionAuctionRequested(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_auction_requested",$user, $array);
    }

    public function onAuctionAuctionScoring(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan(),
            "files" => $event->getFiles()
        );
        $this->emailHandler->send("auction_auction_scoring",$user, $array);
    }

    public function onAuctionAuctionApproved(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_auction_approved",$user, $array);
    }

    public function onAuctionAuctionCanceled(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_auction_canceled",$user, $array);
    }

    public function onAuctionPayable(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_payable",$user, $array);
    }

    public function onAuctionLoan(LoanEvent $event){
        $user = $event->getLoan()->getUser();
        $loan = $event->getLoan();

        $array = array(
            "loan" => $loan
        );
        $this->emailHandler->send("auction_loan",$user, $array);

        foreach($loan->getInvestments() AS $investment){
            if($investment->isStatus($investment::STATUS_PAID)){
                $user = $investment->getUser();
                $array = array(
                    "investment" => $investment
                );
                $this->emailHandler->send("investment_loan",$user, $array);
            }
        }
    }


    public function onAuctionAccepted(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan(),
            "files" => $event->getFiles()
        );
        $this->emailHandler->send("auction_accepted",$user, $array);
    }


    public function onAuctionCanceled(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_canceled",$user, $array);
    }

    public function onAuctionOverdue(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan()
        );
        $this->emailHandler->send("auction_overdue",$user, $array);
    }

    public function onFileApprove($event){

        if($event instanceof ProfileFileEvent){
            $user = $event->getProfile()->getUser();
            $array = array(
                "file" => $event->getProfileFile(),
                "user" => $user,
            );

        }
        if($event instanceof LoanFileEvent){
            $user = $event->getLoan()->getUser();
            $array = array(
                "file" => $event->getLoanFile(),
                "user" => $user,
            );
        }

        $this->emailHandler->send("file_approve",$user, $array);
    }

    public function onFileReject($event){

        if($event instanceof ProfileFileEvent){
            $user = $event->getProfile()->getUser();
            $array = array(
                "file" => $event->getProfileFile(),
                "user" => $user,
            );

        }
        if($event instanceof LoanFileEvent){
            $user = $event->getLoan()->getUser();
            $array = array(
                "file" => $event->getLoanFile(),
                "user" => $user,
            );
        }

        $this->emailHandler->send("file_reject",$user, $array);
    }

    public function onFileDelivered($event){

        if($event instanceof ProfileFileEvent){
            $user = $event->getProfile()->getUser();
            $array = array(
                "file" => $event->getProfileFile(),
                "user" => $user,
            );

        }
        if($event instanceof LoanFileEvent){
            $user = $event->getLoan()->getUser();
            $array = array(
                "file" => $event->getLoanFile(),
                "user" => $user,
            );
        }

        $this->emailHandler->send("file_delivered",$user, $array);
    }

    public function onFileMissing($event){

        if($event instanceof ProfileFileEvent){
            $user = $event->getProfile()->getUser();
            $array = array(
                "file" => $event->getProfileFile(),
                "user" => $user,
            );

        }
        if($event instanceof LoanFileEvent){
            $user = $event->getLoan()->getUser();
            $array = array(
                "file" => $event->getLoanFile(),
                "user" => $user,
            );
        }

        $this->emailHandler->send("file_missing",$user, $array);
    }


    public function onInvestorApproved(UserEvent $event){
        $user = $event->getUser();

        $array = array(
            "user" => $user,
        );
        $this->emailHandler->send("investor_approved",$user, $array);
    }

    public function onAuctionLaunch(LoanEvent $event){
        $user = $event->getLoan()->getUser();

        $array = array(
            "loan" => $event->getLoan(),
        );
        $this->emailHandler->send("auction_launch",$user, $array);
    }


    public function onInvestmentAdd(InvestmentEvent $event)
    {
        $user = $event->getInvestment()->getLoan()->getUser();

        $array = array(
            "investment" => $event->getInvestment(),
        );
        $this->emailHandler->send("investment_add",$user, $array);

    }

    public function onInvestmentCancel(InvestmentEvent $event)
    {
        $user = $event->getInvestment()->getUser();

        $array = array(
            "investment" => $event->getInvestment(),
        );
        $this->emailHandler->send("investment_cancel",$user, $array);

    }

    public function onInvestmentDelete(InvestmentEvent $event)
    {
        $user = $event->getInvestment()->getUser();

        $array = array(
            "investment" => $event->getInvestment(),
        );
        $this->emailHandler->send("investment_delete",$user, $array);

    }

    public function onInvestmentPrepared(InvestmentEvent $event)
    {
        $user = $event->getInvestment()->getUser();

        $array = array(
            "investment" => $event->getInvestment(),
        );
        $this->emailHandler->send("investment_prepared",$user, $array);

    }

    public function onInvestorRegistration(UserEvent $event)
    {
        $user = $event->getUser();

        $array = array(
            "user" => $event->getUser(),
            "files" => $event->getFiles(),
        );
        $this->emailHandler->send("investor_registration",$user, $array);

    }

    public function onGridCardGenerated(UserEvent $event)
    {
        $user = $event->getUser();

        $array = array(
            "user" => $event->getUser(),
            "files" => $event->getFiles(),
        );
        $this->emailHandler->send("grid_card",$user, $array);
//        list($file) = $event->getFiles();
//        $file->removeUpload();
    }


    public function onTransactionAdd(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();
        $user = $transaction->getFromUser();

        $array = array(
            "transaction" => $transaction,
        );

        switch ($transaction->getType()) {
            case $transaction::TYPE_DEPOSIT:
                $this->emailHandler->send("transaction_deposit_add",$user, $array);
                break;
            case $transaction::TYPE_WITHDRAW:
                $this->emailHandler->send("transaction_withdraw_add",$user, $array);
                break;
        }

    }

    public function onTransactionAccepted(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();
        $fromUser = $transaction->getFromUser();
        $toUser = $transaction->getToUser();
        $array = array(
            "transaction" => $transaction,
        );

        switch ($transaction->getType()) {
            case $transaction::TYPE_DEPOSIT:
                $this->emailHandler->send("transaction_deposit_accepted",$fromUser, $array);
                break;
            case $transaction::TYPE_WITHDRAW:
                $this->emailHandler->send("transaction_withdraw_accepted",$fromUser, $array);
                break;
            case $transaction::TYPE_REPAYMENT:
                $this->emailHandler->send("transaction_repayment_accepted",$fromUser, $array);
                break;
            case $transaction::TYPE_PRINCIPAL:
                $this->emailHandler->send("transaction_principal_accepted",$toUser, $array);
                break;
            case $transaction::TYPE_INTEREST:
                $this->emailHandler->send("transaction_interest_accepted",$toUser, $array);
                break;
        }

    }

    public function onTransactionRejected(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();
        $user = $transaction->getFromUser();
        $array = array(
            "transaction" => $transaction,
        );

        switch ($transaction->getType()) {
            case $transaction::TYPE_DEPOSIT:
                $this->emailHandler->send("transaction_deposit_rejected",$user, $array);
                break;
            case $transaction::TYPE_WITHDRAW:
                $this->emailHandler->send("transaction_withdraw_rejected",$user, $array);
                break;
        }

    }

    public function onTransactionRepaymentPending(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();
        $user = $transaction->getFromUser();
        $array = array(
            "transaction" => $transaction,
        );

        switch ($transaction->getType()) {
            case $transaction::TYPE_REPAYMENT:
                $this->emailHandler->send("transaction_repayment_pending",$user, $array);
                break;
        }
    }

    public function onTransactionRepaymentOverdue(TransactionEvent $event)
    {
        $transaction = $event->getTransaction();
        $user = $transaction->getFromUser();
        $array = array(
            "transaction" => $transaction,
        );

        switch ($transaction->getType()) {
            case $transaction::TYPE_REPAYMENT:
                $this->emailHandler->send("transaction_repayment_overdue",$user, $array);
                break;
        }
    }


} 