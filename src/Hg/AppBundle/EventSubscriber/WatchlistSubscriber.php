<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:18
 */

namespace Hg\AppBundle\EventSubscriber;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Watchlist;
use Hg\AppBundle\Event\AnswerEvent;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\QuestionEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class WatchlistSubscriber implements EventSubscriberInterface {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            HgEvents::AUCTION_ANSWER_ADD => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_QUESTION_ADD => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_ACCEPTED => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_CANCELED => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_OVERDUE => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_INVESTMENT_ADD => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_INVESTMENT_CANCEL => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_INVESTMENT_DELETE => array('onUpdatedAdd', 0),
            HgEvents::AUCTION_INVESTMENT_PREPARED => array('onUpdatedAdd', 0),
        );
    }

    public function onUpdatedAdd($event)
    {
        $loan = null;
        $user = null;

        if($event instanceof LoanEvent){
            $loan = $event->getLoan();
        }
        if($event instanceof AnswerEvent || $event instanceof QuestionEvent){
            $loan = $event->getComment()->getLoan();
            $user = $event->getComment()->getUser();
        }
        if($event instanceof InvestmentEvent){
            $loan = $event->getInvestment()->getLoan();
            $user = $event->getInvestment()->getUser();
        }

        if(!$loan) return;


        $watchlists = $this->em->getRepository("AppBundle:Watchlist")->findBy(array(
            "loan" => $loan,
            "status" => Watchlist::STATUS_ACTIVE
        ));


        /** @var Watchlist $watchlist */
        foreach($watchlists AS $watchlist){
            if(!$watchlist->isUpdated()){
                $watchlist->setUpdated(true);
                $watchlist->getUser()->addWatchlistCount(1);
            }
        }
        $this->em->flush();


    }
} 