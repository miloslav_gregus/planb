<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;


use Hg\AppBundle\Entity\Comment;
use Symfony\Component\EventDispatcher\Event;

class QuestionEvent extends Event{

    private $comment;

    public function __construct(Comment $comment) {
        $this->comment = $comment;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param Comment $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


} 