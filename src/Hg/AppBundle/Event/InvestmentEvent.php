<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Investment;
use Symfony\Component\EventDispatcher\Event;

class InvestmentEvent extends Event {

    private $investment;

    public function __construct(Investment $investment) {
        $this->investment = $investment;
    }

    /**
     * @return \Hg\AppBundle\Entity\Investment
     */
    public function getInvestment()
    {
        return $this->investment;
    }

    /**
     * @param \Hg\AppBundle\Entity\Investment $investment
     */
    public function setInvestment($investment)
    {
        $this->investment = $investment;
    }


}