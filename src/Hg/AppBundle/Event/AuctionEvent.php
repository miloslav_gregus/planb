<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Loan;
use Symfony\Component\EventDispatcher\Event;

class AuctionEvent extends Event {

    private $loan;

    public function __construct(Loan $loan) {
        $this->loan = $loan;
    }

    /**
     * @return \Hg\AppBundle\Entity\Loan
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * @param \Hg\AppBundle\Entity\Loan $loan
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    }

} 