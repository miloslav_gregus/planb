<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\LoanFile;
use Symfony\Component\EventDispatcher\Event;

class LoanFileEvent extends Event {

    private $loan;
    private $loanFile;

    public function __construct(Loan $loan, LoanFile $loanFile) {
        $this->loan = $loan;
        $this->loanFile = $loanFile;
    }

    /**
     * @return \Hg\AppBundle\Entity\LoanFile
     */
    public function getLoanFile()
    {
        return $this->loanFile;
    }

    /**
     * @param \Hg\AppBundle\Entity\LoanFile $loanFile
     */
    public function setLoanFile($loanFile)
    {
        $this->loanFile = $loanFile;
    }

    /**
     * @return \Hg\AppBundle\Entity\Loan
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * @param \Hg\AppBundle\Entity\Loan $loan
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    }


} 