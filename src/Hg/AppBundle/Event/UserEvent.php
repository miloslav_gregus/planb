<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Comment;
use Hg\AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserEvent extends Event {

    private $user;
    private $files;

    public function __construct(User $user, $files = null) {
        $this->user = $user;
        $this->files = $files;
    }

    /**
     * @return \Hg\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \Hg\AppBundle\Entity\User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return null
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param null $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }


} 