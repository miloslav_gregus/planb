<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Loan;
use Symfony\Component\EventDispatcher\Event;

class LoanEvent extends Event {

    private $loan;
    private $files;

    public function __construct(Loan $loan, $files = null) {
        $this->loan = $loan;
        $this->files = $files;
    }

    /**
     * @return \Hg\AppBundle\Entity\Loan
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * @param \Hg\AppBundle\Entity\Loan $loan
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }


} 