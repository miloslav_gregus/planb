<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Contact;
use Symfony\Component\EventDispatcher\Event;

class ContactEvent extends Event {

    private $contact;

    public function __construct(Contact $contact) {
        $this->contact = $contact;
    }

    /**
     * @return \Hg\AppBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param \Hg\AppBundle\Entity\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }


} 