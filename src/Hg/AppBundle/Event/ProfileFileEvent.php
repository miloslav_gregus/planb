<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\Profile;
use Hg\AppBundle\Entity\ProfileFile;
use Symfony\Component\EventDispatcher\Event;

class ProfileFileEvent extends Event {

    private $profile;
    private $profileFile;

    public function __construct(Profile $profile, ProfileFile $profileFile) {
        $this->profile = $profile;
        $this->profileFile = $profileFile;
    }

    /**
     * @return mixed
     */
    public function getProfileFile()
    {
        return $this->profileFile;
    }

    /**
     * @param mixed $profileFile
     */
    public function setProfileFile($profileFile)
    {
        $this->profileFile = $profileFile;
    }

    /**
     * @return \Hg\AppBundle\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param \Hg\AppBundle\Entity\Profile $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }


}