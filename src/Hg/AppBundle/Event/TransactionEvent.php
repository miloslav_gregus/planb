<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:19
 */

namespace Hg\AppBundle\Event;

use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class TransactionEvent extends Event {

    private $transaction;

    public function __construct(Transaction $transaction) {
        $this->transaction = $transaction;
    }

    /**
     * @return \Hg\AppBundle\Entity\Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param \Hg\AppBundle\Entity\Transaction $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }


}