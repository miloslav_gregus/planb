<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lifebuoy
 *
 * @ORM\Table(
 *  name="pb_lifebuoys",
 *  indexes={
 *     @ORM\Index(name="status_idx", columns={"created"})
 *  }
 * )
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\LifebuoyRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Lifebuoy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", scale=2)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    private $user;

    public function __construct(){

    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Lifebuoy
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return double
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Lifebuoy
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @return Lifebuoy
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}