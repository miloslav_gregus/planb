<?php

namespace Hg\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Hg\AppBundle\Validator\Constraints as HgAssert;

/**
 * Loan
 *
 * @ORM\Table(
 *      name="pb_profiles"
 * )
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\ProfileRepository")
 */
class Profile
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    private $title;
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     *
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $birthSurname;

    /**
     * @var integer
     *
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $gender;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_loan_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $familyStatus;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_loan_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $education;

    /**
     * @var integer
     *
     * @ORM\Column(type="boolean")
     */
    private $student;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @HgAssert\PersonalId(
     *      groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     *
     * @ORM\Column(type="string", length=13)
     */
    private $personalId;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @ORM\Column(type="string", length=30)
     */
    private $citizenship;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @Assert\Regex(
     *     pattern="/[a-zA-Z]{2}\d{6}/",
     *     message="Nonvalid ID Card number",
     *     groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @HgAssert\IdCard(
     *      groups={"flow_hg_profile_registration_step1", "flow_hg_loan_validate_step2"}
     * )
     * @ORM\Column(type="string", length=8)
     */
    private $idCard;


    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2", "flow_hg_loan_validate_step3"}
     * )
     *
     * @ORM\Column(type="string", length=100)
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2", "flow_hg_loan_validate_step3"}
     * )
     *
     * @ORM\Column(type="string", length=15)
     */
    private $addressNumber;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2", "flow_hg_loan_validate_step3"}
     * )
     * @ORM\Column(type="string", length=40)
     */
    private $addressCity;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2", "flow_hg_loan_validate_step3"}
     * )
     * @ORM\Column(type="string", length=10)
     */
    private $addressZip;

    /**
     * @var integer
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_validate_step3", "flow_hg_loan_validate_step3"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $addressAccomodationType;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $addressContactNotSame;
    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2", "flow_hg_loan_validate_step3"}
     * )
     * @ORM\Column(type="string", length=2)
     */
    private $addressCountry;


    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_has_country", "flow_hg_loan_validate_step3_has_country"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $addressRegion;


    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_extra", "flow_hg_loan_validate_step3_extra"}
     * )
     * @ORM\Column(type="string", length=100)
     */
    private $addressContactStreet;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_extra", "flow_hg_loan_validate_step3_extra"}
     * )
     * @ORM\Column(type="string", length=15)
     */
    private $addressContactNumber;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_extra", "flow_hg_loan_validate_step3_extra"}
     * )
     * @ORM\Column(type="string", length=40)
     */
    private $addressContactCity;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_extra", "flow_hg_loan_validate_step3_extra"}
     * )
     * @ORM\Column(type="string", length=10)
     */
    private $addressContactZip;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_extra", "flow_hg_loan_validate_step3_extra"}
     * )
     * @ORM\Column(type="string", length=2)
     */
    private $addressContactCountry;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2_has_contact_country", "flow_hg_loan_validate_step3_has_contact_country"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $addressContactRegion;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30)
     */
    private $addressTel;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      groups={"flow_hg_profile_registration_step2", "flow_hg_loan_validate_step3"}
     * )
     * @ORM\Column( type="string", length=30)
     */
    private $addressMobil;

    /**
     * @var string
     * @Assert\Range(
     *      min = 0,
     *      groups={"flow_hg_auction_validate_step1", "flow_hg_profile_registration_step2"}
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bankPrefix;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_auction_validate_step1", "flow_hg_profile_registration_step3"}
     * )
     * @Assert\Range(
     *      min = 0,
     *      groups={"flow_hg_auction_validate_step1", "flow_hg_profile_registration_step3"}
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bankNumber;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_auction_validate_step1", "flow_hg_profile_registration_step3"}
     * )
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $bankCode;


    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"flow_hg_auction_validate_step1"}
     * )
     * @ORM\Column(type="smallint", length=255)
     */
    private $otherIdType;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"flow_hg_auction_validate_step1"}
     * )
     * @ORM\Column(type="string", length=32)
     */
    private $otherIdNumber;

    /**
     * @ORM\OneToMany(targetEntity="ProfileFile", mappedBy="profile", cascade={"persist"}, orphanRemoval=true)
     */
    private $files;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="profile", cascade={"persist"})
     */
    private $user;


    public function __construct()
    {
        $this->title = "";
        $this->citizenship = "";
        $this->name = "";
        $this->surname = "";
        $this->birthSurname = "";
        $this->gender = 0;
        $this->familyStatus = 0;
        $this->personalId = "";
        $this->idCard = "";
        $this->education = 0;
        $this->student = false;
        $this->addressStreet = "";
        $this->addressNumber = "";
        $this->addressCity = "";
        $this->addressCountry = "SK";
        $this->addressZip = "";
        $this->addressAccomodationType = "";
        $this->addressContactNotSame = false;
        $this->addressRegion = "";
        $this->addressContactStreet = "";
        $this->addressContactNumber = "";
        $this->addressContactCity = "";
        $this->addressContactZip = "";
        $this->addressContactCountry = "SK";
        $this->addressContactRegion = "";
        $this->addressTel = "";
        $this->addressMobil = "";
        $this->otherIdType = "";
        $this->otherIdNumber = "";

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Profile
     */
    public function setTitle($title)
    {
        $this->title = $title ? $title : "";

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Profile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFullName()
    {
        return ($this->title ? $this->title . " " : "") . $this->name . " " . $this->surname;
    }


    /**
     * Set surname
     *
     * @param string $surname
     * @return Profile
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set birthSurname
     *
     * @param string $birthSurname
     * @return Profile
     */
    public function setBirthSurname($birthSurname)
    {
        $this->birthSurname = $birthSurname ? $birthSurname : "";

        return $this;
    }

    /**
     * Get birthSurname
     *
     * @return string
     */
    public function getBirthSurname()
    {
        return $this->birthSurname;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return Profile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set familyStatus
     *
     * @param integer $familyStatus
     * @return Profile
     */
    public function setFamilyStatus($familyStatus)
    {
        $this->familyStatus = $familyStatus;

        return $this;
    }

    /**
     * Get familyStatus
     *
     * @return integer
     */
    public function getFamilyStatus()
    {
        return $this->familyStatus;
    }

    /**
     * Set education
     *
     * @param integer $education
     * @return Profile
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return integer
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set student
     *
     * @param boolean $student
     * @return Profile
     */
    public function setStudent($student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return boolean
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return Profile
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set personalId
     *
     * @param string $personalId
     * @return Profile
     */
    public function setPersonalId($personalId)
    {
        $this->personalId = $personalId;

        return $this;
    }

    /**
     * Get personalId
     *
     * @return string
     */
    public function getPersonalId()
    {
        return $this->personalId;
    }

    /**
     * Set citizenship
     *
     * @param string $citizenship
     * @return Profile
     */
    public function setCitizenship($citizenship)
    {
        $this->citizenship = $citizenship;

        return $this;
    }

    /**
     * Get citizenship
     *
     * @return string
     */
    public function getCitizenship()
    {
        return $this->citizenship;
    }

    /**
     * Set idCard
     *
     * @param string $idCard
     * @return Profile
     */
    public function setIdCard($idCard)
    {
        $this->idCard = $idCard;

        return $this;
    }

    /**
     * Get idCard
     *
     * @return string
     */
    public function getIdCard()
    {
        return $this->idCard;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     * @return Profile
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;

        return $this;
    }

    /**
     * Get addressStreet
     *
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * Set addressNumber
     *
     * @param string $addressNumber
     * @return Profile
     */
    public function setAddressNumber($addressNumber)
    {
        $this->addressNumber = $addressNumber;

        return $this;
    }

    /**
     * Get addressNumber
     *
     * @return string
     */
    public function getAddressNumber()
    {
        return $this->addressNumber;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return Profile
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressZip
     *
     * @param string $addressZip
     * @return Profile
     */
    public function setAddressZip($addressZip)
    {
        $this->addressZip = $addressZip;

        return $this;
    }

    /**
     * Get addressZip
     *
     * @return string
     */
    public function getAddressZip()
    {
        return $this->addressZip;
    }

    /**
     * Set addressAccomodationType
     *
     * @param integer $addressAccomodationType
     * @return Profile
     */
    public function setAddressAccomodationType($addressAccomodationType)
    {
        $this->addressAccomodationType = $addressAccomodationType;

        return $this;
    }

    /**
     * Get addressAccomodationType
     *
     * @return integer
     */
    public function getAddressAccomodationType()
    {
        return $this->addressAccomodationType;
    }

    /**
     * Set addressContactNotSame
     *
     * @param boolean $addressContactNotSame
     * @return Profile
     */
    public function setAddressContactNotSame($addressContactNotSame)
    {
        $this->addressContactNotSame = $addressContactNotSame;

        return $this;
    }

    /**
     * Get addressContactNotSame
     *
     * @return boolean
     */
    public function getAddressContactNotSame()
    {
        return $this->addressContactNotSame;
    }

    /**
     * Set addressCountry
     *
     * @param string $addressCountry
     * @return Profile
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;

        return $this;
    }

    /**
     * Get addressCountry
     *
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * Set addressRegion
     *
     * @param integer $addressRegion
     * @return Profile
     */
    public function setAddressRegion($addressRegion)
    {
        $this->addressRegion = $addressRegion;

        return $this;
    }

    /**
     * Get addressRegion
     *
     * @return integer
     */
    public function getAddressRegion()
    {
        return $this->addressRegion;
    }

    /**
     * Set addressContactStreet
     *
     * @param string $addressContactStreet
     * @return Profile
     */
    public function setAddressContactStreet($addressContactStreet)
    {
        $this->addressContactStreet = $addressContactStreet;

        return $this;
    }

    /**
     * Get addressContactStreet
     *
     * @return string
     */
    public function getAddressContactStreet()
    {
        return $this->addressContactStreet;
    }

    /**
     * Set addressContactNumber
     *
     * @param string $addressContactNumber
     * @return Profile
     */
    public function setAddressContactNumber($addressContactNumber)
    {
        $this->addressContactNumber = $addressContactNumber;

        return $this;
    }

    /**
     * Get addressContactNumber
     *
     * @return string
     */
    public function getAddressContactNumber()
    {
        return $this->addressContactNumber;
    }

    /**
     * Set addressContactCity
     *
     * @param string $addressContactCity
     * @return Profile
     */
    public function setAddressContactCity($addressContactCity)
    {
        $this->addressContactCity = $addressContactCity;

        return $this;
    }

    /**
     * Get addressContactCity
     *
     * @return string
     */
    public function getAddressContactCity()
    {
        return $this->addressContactCity;
    }

    /**
     * Set addressContactZip
     *
     * @param string $addressContactZip
     * @return Profile
     */
    public function setAddressContactZip($addressContactZip)
    {
        $this->addressContactZip = $addressContactZip;

        return $this;
    }

    /**
     * Get addressContactZip
     *
     * @return string
     */
    public function getAddressContactZip()
    {
        return $this->addressContactZip;
    }

    /**
     * Set addressContactCountry
     *
     * @param string $addressContactCountry
     * @return Profile
     */
    public function setAddressContactCountry($addressContactCountry)
    {
        $this->addressContactCountry = $addressContactCountry;

        return $this;
    }

    /**
     * Get addressContactCountry
     *
     * @return string
     */
    public function getAddressContactCountry()
    {
        return $this->addressContactCountry;
    }

    /**
     * Set addressContactRegion
     *
     * @param integer $addressContactRegion
     * @return Profile
     */
    public function setAddressContactRegion($addressContactRegion)
    {
        $this->addressContactRegion = $addressContactRegion;

        return $this;
    }

    /**
     * Get addressContactRegion
     *
     * @return integer
     */
    public function getAddressContactRegion()
    {
        return $this->addressContactRegion;
    }

    /**
     * Set addressTel
     *
     * @param string $addressTel
     * @return Profile
     */
    public function setAddressTel($addressTel)
    {
        $this->addressTel = $addressTel;

        return $this;
    }

    /**
     * Get addressTel
     *
     * @return string
     */
    public function getAddressTel()
    {
        return $this->addressTel;
    }

    /**
     * Set addressMobil
     *
     * @param string $addressMobil
     * @return Profile
     */
    public function setAddressMobil($addressMobil)
    {
        $this->addressMobil = $addressMobil;

        return $this;
    }

    /**
     * Get addressMobil
     *
     * @return string
     */
    public function getAddressMobil()
    {
        return $this->addressMobil;
    }

    /**
     * Set bankPrefix
     *
     * @param integer $bankPrefix
     * @return Profile
     */
    public function setBankPrefix($bankPrefix)
    {
        $this->bankPrefix = $bankPrefix;

        return $this;
    }

    /**
     * Get bankPrefix
     *
     * @return integer
     */
    public function getBankPrefix()
    {
        return $this->bankPrefix;
    }

    /**
     * Set bankNumber
     *
     * @param integer $bankNumber
     * @return Profile
     */
    public function setBankNumber($bankNumber)
    {
        $this->bankNumber = $bankNumber;

        return $this;
    }

    /**
     * Get bankNumber
     *
     * @return integer
     */
    public function getBankNumber()
    {
        return $this->bankNumber;
    }

    /**
     * Get formatted bank number
     *
     * @return integer
     */
    public function getBankNumberFormatted()
    {
        return ($this->getBankPrefix() ? $this->getBankPrefix() . "-" : $this->getBankPrefix()) . $this->getBankNumber() . "/" . $this->getBankCode();
    }

    /**
     * Set bankCode
     *
     * @param integer $bankCode
     * @return Profile
     */
    public function setBankCode($bankCode)
    {
        $this->bankCode = $bankCode;

        return $this;
    }

    /**
     * Get bankCode
     *
     * @return integer
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * Add files
     *
     * @param \Hg\AppBundle\Entity\ProfileFile $files
     * @return Profile
     */
    public function addFile(\Hg\AppBundle\Entity\ProfileFile $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Hg\AppBundle\Entity\ProfileFile $files
     */
    public function removeFile(\Hg\AppBundle\Entity\ProfileFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set user
     *
     * @param \Hg\AppBundle\Entity\User $user
     * @return Profile
     */
    public function setUser(\Hg\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Hg\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set otherIdType
     *
     * @param integer $otherIdType
     * @return Profile
     */
    public function setOtherIdType($otherIdType)
    {
        $this->otherIdType = $otherIdType;

        return $this;
    }

    /**
     * Get otherIdType
     *
     * @return integer
     */
    public function getOtherIdType()
    {
        return $this->otherIdType;
    }

    /**
     * Set otherIdNumber
     *
     * @param string $otherIdNumber
     * @return Profile
     */
    public function setOtherIdNumber($otherIdNumber)
    {
        $this->otherIdNumber = $otherIdNumber;

        return $this;
    }

    /**
     * Get otherIdNumber
     *
     * @return string
     */
    public function getOtherIdNumber()
    {
        return $this->otherIdNumber;
    }
}