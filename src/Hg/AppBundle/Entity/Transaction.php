<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(
name="pb_transactions",
 *   indexes={
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="type_idx", columns={"type"}),
 *         @ORM\Index(name="variable_number_idx", columns={"variable_number"}),
 *         @ORM\Index(name="from_type_idx", columns={"from_type"}),
 *         @ORM\Index(name="to_type_number_idx", columns={"to_type"}),
 *         @ORM\Index(name="due_date_idx", columns={"due_date"})
 *   }))
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\TransactionRepository")
 */
class Transaction
{
    const STATUS_FUTURE = "future";
    const STATUS_PENDING = "pending";
    const STATUS_CANCELED = "canceled";
    const STATUS_SUCCESS = "success";
    const STATUS_OVERDUE = "overdue";

    const TYPE_DEPOSIT = "deposit";
    const TYPE_WITHDRAW = "withdraw";
    const TYPE_INVESTMENT = "investment";
    const TYPE_LOAN = "loan";
    const TYPE_REPAYMENT = "repayment";
    const TYPE_PRINCIPAL = "principal";
    const TYPE_INTEREST = "interest";
    const TYPE_FEE = "fee";
    const TYPE_FEE_LIFEBUOY = "fee_lifebuoy";

    const ACCOUNT_TYPE_PLANB = "planb";
    const ACCOUNT_TYPE_INVESTOR_REAL = "investor_real";
    const ACCOUNT_TYPE_INVESTOR_VIRTUAL = "investor_virtual";
    const ACCOUNT_TYPE_CUSTOMER_REAL = "customer_real";
    const ACCOUNT_TYPE_LOAN_VIRTUAL = "loan_virtual";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Loan")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    private $loan;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $fromUser;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $toUser;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fromType;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $toType;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $specificNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $variableNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct($type)
    {
        $this->repayment = 0;
        $this->type = $type;
        $this->status = self::STATUS_FUTURE;
        $this->specificNumber = "";
        $this->dueDate = new \DateTime();


        if($type == self::TYPE_REPAYMENT){
            $this->fromType = self::ACCOUNT_TYPE_CUSTOMER_REAL;
            $this->toType = self::ACCOUNT_TYPE_LOAN_VIRTUAL;
        }

        if($type == self::TYPE_INTEREST){
            $this->fromType = self::ACCOUNT_TYPE_LOAN_VIRTUAL;
            $this->toType = self::ACCOUNT_TYPE_INVESTOR_VIRTUAL;
        }

        if($type == self::TYPE_PRINCIPAL){
            $this->fromType = self::ACCOUNT_TYPE_LOAN_VIRTUAL;
            $this->toType = self::ACCOUNT_TYPE_INVESTOR_VIRTUAL;
        }

        if($type == self::TYPE_INVESTMENT){
            $this->fromType = self::ACCOUNT_TYPE_INVESTOR_VIRTUAL;
            $this->toType = self::ACCOUNT_TYPE_LOAN_VIRTUAL;
        }

        if($type == self::TYPE_LOAN){
            $this->fromType = self::ACCOUNT_TYPE_LOAN_VIRTUAL;
            $this->toType = self::ACCOUNT_TYPE_CUSTOMER_REAL;
        }
        if($type == self::TYPE_FEE){
            $this->fromType = self::ACCOUNT_TYPE_CUSTOMER_REAL;
            $this->toType = self::ACCOUNT_TYPE_PLANB;
        }
        if($type == self::TYPE_FEE_LIFEBUOY){
            $this->fromType = self::ACCOUNT_TYPE_CUSTOMER_REAL;
            $this->toType = self::ACCOUNT_TYPE_PLANB;
        }
        if($type == self::TYPE_DEPOSIT){
            $this->fromType = self::ACCOUNT_TYPE_INVESTOR_REAL;
            $this->toType = self::ACCOUNT_TYPE_INVESTOR_VIRTUAL;
        }
        if($type == self::TYPE_WITHDRAW){
            $this->fromType = self::ACCOUNT_TYPE_INVESTOR_VIRTUAL;
            $this->toType = self::ACCOUNT_TYPE_INVESTOR_REAL;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getFormattedId()
    {

        if ($this->isType(self::TYPE_FEE)) {
            $id = $this->getId() + 5000000000;
        } elseif($this->isType(self::TYPE_WITHDRAW)){
            $id = $this->getId() + 6000000000;
        } else {
            $id = $this->getId() + 4000000000;
        }

        return $id;
    }

    /**
     * Set fromUser
     *
     * @param User $fromUser
     * @return Transaction
     */
    public function setFromUser($fromUser)
    {
        $this->fromUser = $fromUser;

        return $this;
    }

    /**
     * Get fromUser
     *
     * @return User
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     * Set toUser
     *
     * @param User $toUser
     * @return Transaction
     */
    public function setToUser($toUser)
    {
        $this->toUser = $toUser;

        return $this;
    }

    /**
     * Get toUser
     *
     * @return User
     */
    public function getToUser()
    {
        return $this->toUser;
    }

    /**
     * Set fromType
     *
     * @param integer $fromType
     * @return Transaction
     */
    public function setFromType($fromType = null)
    {
        $this->fromType = $fromType;

        return $this;
    }

    /**
     * Get fromType
     *
     * @return integer
     */
    public function getFromType()
    {
        return $this->fromType;
    }
    /**
     * Get fromType
     *
     * @return integer
     */
    public function getFromTypeFormatted()
    {
        return "transactions.account_type." . $this->fromType;
    }

    /**
     * Set toType
     *
     * @param integer $toType
     * @return Transaction
     */
    public function setToType($toType = null)
    {
        $this->toType = $toType;

        return $this;
    }

    /**
     * Get toType
     *
     * @return integer
     */
    public function getToType()
    {
        return $this->toType;
    }

    /**
     * Get toType
     *
     * @return string
     */
    public function getToTypeFormatted()
    {
        return "transactions.account_type." . $this->toType;
    }

    /**
     * Set variableNumber
     *
     * @param string $variableNumber
     * @return Transaction
     */
    public function setVariableNumber($variableNumber = null)
    {
         $this->variableNumber = $variableNumber;

        return $this;
    }

    /**
     * Get variableNumber
     *
     * @return string
     */
    public function getVariableNumber()
    {
        return $this->variableNumber;
    }

    /**
     * Set repaiment
     *
     * @param integer $repaiment
     * @return Transaction
     */
    public function setRepaiment($repaiment)
    {
        $this->repaiment = $repaiment;

        return $this;
    }

    /**
     * Get repaiment
     *
     * @return integer
     */
    public function getRepaiment()
    {
        return $this->repaiment;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Transaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getTypeFormatted()
    {
        return "transactions.type." . $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Transaction
     */
    public function setStatus($status)
    {
        if(!$this->isStatus(array(
            self::STATUS_CANCELED,
            self::STATUS_SUCCESS
        ))){
            $this->status = $status;
        }

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Get status
     *
     * @return string
     */
    public function getStatusFormatted()
    {
        return "transactions.status." . $this->status;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * @return Transaction
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Transaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get repaiment
     *
     * @return string
     */
    public function getDescription()
    {
        return "transactions.description." . $this->getType();
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist()
     */
    public function preUpdate()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set loan
     *
     * @param \Hg\AppBundle\Entity\Loan $loan
     * @return Transaction
     */
    public function setLoan(\Hg\AppBundle\Entity\Loan $loan = null)
    {
        $this->loan = $loan;

        return $this;
    }

    /**
     * Get loan
     *
     * @return \Hg\AppBundle\Entity\Loan
     */
    public function getLoan()
    {
        return $this->loan;
    }


    /**
     * Get whatever is correct type
     *
     * @param string|array $type
     * @return boolean
     */
    public function isType($type)
    {
        if (is_array($type)) {
            return in_array($this->getType(), $type);
        }

        return $this->getType() == $type;
    }

    /**
     * Get whatever is correct status
     *
     * @param string|array $status
     * @return boolean
     */
    public function isStatus($status)
    {
        if (is_array($status)) {
            return in_array($this->getStatus(), $status);
        }

        return $this->getStatus() == $status;
    }



    /**
     * Set specificNumber
     *
     * @param string $specificNumber
     * @return Transaction
     */
    public function setSpecificNumber($specificNumber)
    {
        $this->specificNumber = $specificNumber;
    
        return $this;
    }

    /**
     * Get specificNumber
     *
     * @return string 
     */
    public function getSpecificNumber()
    {
        return $this->specificNumber;
    }
}