<?php

namespace Hg\AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="pb_users",
 *      indexes={
 * @ORM\Index(name="created_at_idx", columns={"created_at"}),
 * @ORM\Index(name="type_idx", columns={"type"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{

    const TYPE_PLANB = "planb";
    const TYPE_CUSTOMER = "customer";
    const TYPE_INVESTOR = "investor";

    // shared
    const STATUS_PENDING = "pending";
    const STATUS_DEFAULT = "default";

    // investor
    const STATUS_DOCUMENTS_WAITING = "documents-waiting";

    // customer
    const STATUS_DRAFT_REQUESTED = "draft-requested";
    const STATUS_DRAFT_APPROVED = "draft-approved";
    const STATUS_DRAFT_CANCELED = "draft-canceled";
    const STATUS_AUCTION_DRAFT = "auction-draft";
    const STATUS_AUCTION_REQUESTED = "auction-requested";
    const STATUS_AUCTION_CANCELED = "auction-canceled";
    const STATUS_AUCTION_APPROVED = "auction-approved";
    const STATUS_AUCTION_SCORING = "auction-scoring";
    const STATUS_AUCTION = "auction";
    const STATUS_A_ACCEPTED = "auction-accepted";
    const STATUS_A_CANCELED = "auction-canceled";
    const STATUS_AUCTION_PAYABLE = "auction-payable";




    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string*
     *
     * @Assert\Length(
     *     min=3,
     *     max=18,
     *     minMessage="fos_user.username.min_chars",
     *     maxMessage="fos_user.username.max_chars",
     *     groups={"Registration"}
     * )
     * @Assert\Regex(
     *     pattern     = "/[^a-zA-Z_0-9]/",
     *     message = "fos_user.username.regex",
     *     match = false,
     *     groups={"Registration"}
     * )
     */
    protected $username;


    /**
     * @ORM\OneToOne(targetEntity="Profile", inversedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    protected $profile;

    /**
     * @var decimal $creditAll
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $creditAll;
    /**
     * @var decimal $creditFree
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $creditFree;
    /**
     * @var decimal $creditAuctions
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $creditAuctions;
    /**
     * @var decimal $creditLoans
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $creditLoans;
    /**
     * @var decimal $creditWithdraw
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $creditWithdraw;

    /**
     * @var integer $auctionsPending
     *
     * @ORM\Column(type="smallint")
     */
    protected $auctionsPending;

    /**
     * @var integer $auctionsActive
     *
     * @ORM\Column(type="smallint")
     */
    protected $auctionsActive;

    /**
     * @var integer $auctionsInactive
     *
     * @ORM\Column(type="smallint")
     */
    protected $auctionsInactive;

    /**
     * @var integer $loansActive
     *
     * @ORM\Column(type="smallint")
     */
    protected $loansActive;

    /**
     * @var integer $loansInactive
     *
     * @ORM\Column(type="smallint")
     */
    protected $loansInactive;

    /**
     * @var integer $watchlist
     *
     * @ORM\Column(type="smallint")
     */
    protected $watchlistCount;

    /**
     * @var boolean $notificationsWatchlist
     *
     * @ORM\Column(type="boolean")
     */
    protected $notificationsWatchlist;

    /**
     * @var boolean $notificationsWatchdog
     *
     * @ORM\Column(type="boolean")
     */
    protected $notificationsWatchdog;
    /**
     * @var $validationHash
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $validationHash;

    /**
     * @var $type
     *
     * @ORM\Column(type="string", length=20)
     */
    protected $type;

    /**
     * @var $status
     *
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @var $doneAlready
     *
     * @ORM\Column(type="boolean")
     */
    protected $doneAlready;

    /**
     * @var \Datetime $updatedAt
     *
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;
    /**
     * @var \Datetime $createdAt
     *
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;


    public function __construct()
    {
        parent::__construct();
        $this->creditAll = 0;
        $this->creditFree = 0;
        $this->creditAuctions = 0;
        $this->creditLoans = 0;
        $this->auctionsPending = 0;
        $this->auctionsActive = 0;
        $this->auctionsInactive = 0;
        $this->loansActive = 0;
        $this->loansInactive = 0;
        $this->creditWithdraw = 0;
        $this->doneAlready = 0;
        $this->watchlistCount = 0;
        $this->notificationsWatchdog = true;
        $this->notificationsWatchlist = true;
        $this->status = self::STATUS_PENDING;
        $this->createdAt = new \DateTime();
    }


    public function getFormattedId(){
        $id = $this->getId();

        if($this->isCustomer()){
            $id = $id + 1000000000;
        }

        if($this->isInvestor()){
            $id = $id + 2000000000;
        }

        return $id;
    }

    public function isInvestor() {
        return $this->isType($this::TYPE_INVESTOR);
    }

    public function isCustomer() {
        return $this->isType($this::TYPE_CUSTOMER);
    }

    public function getRoles()
    {
        $roles = $this->roles;

        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }

        return array_unique($roles);
    }






    /**
     * Set updatedAt
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt = null)
    {
        if(!$updatedAt) $updatedAt =  new \DateTime("now");
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get whatever is correct type
     *
     * @param string|array $type
     * @return boolean
     */
    public function isType($type)
    {
        if (is_array($type)) {
            return in_array($this->getType(), $type);
        }

        return $this->getType() == $type;
    }

    /**
     * Set credit
     *
     * @param integer $credit
     * @return User
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    
        return $this;
    }

    /**
     * Get credit
     *
     * @return integer 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set profile
     *
     * @param \Hg\AppBundle\Entity\Profile $profile
     * @return User
     */
    public function setProfile(\Hg\AppBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return \Hg\AppBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set creditAll
     *
     * @param integer $creditAll
     * @return User
     */
    public function setCreditAll($creditAll)
    {
        $this->creditAll = $creditAll;
    
        return $this;
    }

    /**
     * Set creditAll
     *
     * @param integer $creditAll
     * @return User
     */
    public function addCreditAll($creditAll)
    {
        $this->creditAll += $creditAll;

        return $this;
    }

    /**
     * Get creditAll
     *
     * @return integer 
     */
    public function getCreditAll()
    {
        return $this->creditAll;
    }

    /**
     * Set creditAuctions
     *
     * @param integer $creditAuctions
     * @return User
     */
    public function setCreditAuctions($creditAuctions)
    {
        $this->creditAuctions = $creditAuctions;
    
        return $this;
    }

    /**
     * Set creditAuctions
     *
     * @param integer $creditAuctions
     * @return User
     */
    public function addCreditAuctions($creditAuctions)
    {
        $this->creditAuctions += $creditAuctions;

        return $this;
    }

    /**
     * Get creditAuctions
     *
     * @return integer 
     */
    public function getCreditAuctions()
    {
        return $this->creditAuctions;
    }

    /**
     * Set creditLoans
     *
     * @param integer $creditLoans
     * @return User
     */
    public function setCreditLoans($creditLoans)
    {
        $this->creditLoans = $creditLoans;
    
        return $this;
    }

    /**
     * Set creditLoans
     *
     * @param integer $creditLoans
     * @return User
     */
    public function addCreditLoans($creditLoans)
    {
        $this->creditLoans += $creditLoans;

        return $this;
    }

    /**
     * Get creditLoans
     *
     * @return integer 
     */
    public function getCreditLoans()
    {
        return $this->creditLoans;
    }

    /**
     * Set creditWithdraw
     *
     * @param integer $creditWithdraw
     * @return User
     */
    public function setCreditWithdraw($creditWithdraw)
    {
        $this->creditWithdraw = $creditWithdraw;
    
        return $this;
    }

    /**
     * Set creditWithdraw
     *
     * @param integer $creditWithdraw
     * @return User
     */
    public function addCreditWithdraw($creditWithdraw)
    {
        $this->creditWithdraw += $creditWithdraw;

        return $this;
    }

    /**
     * Get creditWithdraw
     *
     * @return integer 
     */
    public function getCreditWithdraw()
    {
        return $this->creditWithdraw;
    }

    /**
     * Set creditFree
     *
     * @param integer $creditFree
     * @return User
     */
    public function setCreditFree($creditFree)
    {
        $this->creditFree = $creditFree;
    
        return $this;
    }

    /**
     * Set creditFree
     *
     * @param integer $creditFree
     * @return User
     */
    public function addCreditFree($creditFree)
    {
        $this->creditFree += $creditFree;

        return $this;
    }

    /**
     * Get creditFree
     *
     * @return integer 
     */
    public function getCreditFree()
    {
        return $this->creditFree;
    }


    /**
     * Set validationHash
     *
     * @param \DateTime $validationHash
     * @return User
     */
    public function setValidationHash($validationHash)
    {
        $this->validationHash = $validationHash;
    
        return $this;
    }

    /**
     * Get validationHash
     *
     * @return \DateTime 
     */
    public function getValidationHash()
    {
        return $this->validationHash;
    }

    /**
     * Get validationHash
     *
     * @return bool
     */
    public function hasValidationCode()
    {
        return $this->validationHash !== null;
    }


    public function getValidateHashByGrid($grid){
        $string = $grid . $this->getValidationHash()->format("U");

        return strtoupper(substr(base64_encode(md5($string)), 0,6));
    }

    public function checkValidateHash($value, $grid){
        return strtoupper($value) == $this->getValidateHashByGrid($grid);
    }



    /**
     * Set status
     *
     * @param string $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get whatever is correct type
     *
     * @param string|array $status
     * @return boolean
     */
    public function isStatus($status)
    {
        if (is_array($status)) {
            return in_array($this->getStatus(), $status);
        }

        return $this->getStatus() == $status;
    }

    /**
     * Set doneAlready
     *
     * @param boolean $doneAlready
     * @return User
     */
    public function setDoneAlready($doneAlready)
    {
        $this->doneAlready = $doneAlready;
    
        return $this;
    }

    /**
     * Get doneAlready
     *
     * @return boolean 
     */
    public function getDoneAlready()
    {
        return $this->doneAlready;
    }

    /**
     * Set auctionsPending
     *
     * @param integer $auctionsActive
     * @return User
     */
    public function setAuctionsPending($auctionsActive)
    {
        $this->auctionsPending = $auctionsActive;
    
        return $this;
    }

    /**
     * Add auctionsPending
     *
     * @param integer $add
     * @return User
     */
    public function addAuctionsPending($add)
    {
        $this->auctionsPending += $add;
        return $this;
    }

    /**
     * Get auctionsPending
     *
     * @return integer 
     */
    public function getAuctionsPending()
    {
        return $this->auctionsPending;
    }

    
    /**
     * Set auctionsActive
     *
     * @param integer $auctionsActive
     * @return User
     */
    public function setAuctionsActive($auctionsActive)
    {
        $this->auctionsActive = $auctionsActive;

        return $this;
    }

    /**
     * Add auctionsActive
     *
     * @param integer $add
     * @return User
     */
    public function addAuctionsActive($add)
    {
        $this->auctionsActive += $add;
        return $this;
    }

    /**
     * Get auctionsActive
     *
     * @return integer
     */
    public function getAuctionsActive()
    {
        return $this->auctionsActive;
    }

    /**
     * Set auctionsInactive
     *
     * @param integer $auctionsInactive
     * @return User
     */
    public function setAuctionsInactive($auctionsInactive)
    {
        $this->auctionsInactive = $auctionsInactive;
    
        return $this;
    }

    /**
     * Add auctionsInactive
     *
     * @param integer $add
     * @return User
     */
    public function addAuctionsInactive($add)
    {
        $this->auctionsInactive += $add;
        return $this;
    }

    /**
     * Get auctionsInactive
     *
     * @return integer 
     */
    public function getAuctionsInactive()
    {
        return $this->auctionsInactive;
    }

    /**
     * Set loansActive
     *
     * @param integer $loansActive
     * @return User
     */
    public function setLoansActive($loansActive)
    {
        $this->loansActive = $loansActive;
        return $this;
    }

    /**
     * Add loansActive
     *
     * @param integer $add
     * @return User
     */
    public function addLoansActive($add)
    {
        $this->loansActive += $add;
        return $this;
    }

    /**
     * Get loansActive
     *
     * @return integer 
     */
    public function getLoansActive()
    {
        return $this->loansActive;
    }

    /**
     * Set loansInactive
     *
     * @param integer $loansInactive
     * @return User
     */
    public function setLoansInactive($loansInactive)
    {
        $this->loansInactive = $loansInactive;
    
        return $this;
    }

    /**
     * Add loansInactive
     *
     * @param integer $add
     * @return User
     */
    public function addLoansInactive($add)
    {
        $this->loansInactive += $add;
        return $this;
    }

    /**
     * Get loansInactive
     *
     * @return integer 
     */
    public function getLoansInactive()
    {
        return $this->loansInactive;
    }

    /**
     * Set watchlistCount
     *
     * @param integer $watchlistCount
     * @return User
     */
    public function setWatchlistCount($watchlistCount)
    {
        $this->watchlistCount = $watchlistCount;
    
        return $this;
    }

    /**
     * Get watchlistCount
     *
     * @return integer 
     */
    public function getWatchlistCount()
    {
        return $this->watchlistCount;
    }

    /**
     * Add watchlistCount
     *
     * @param integer $add
     * @return User
     */
    public function addWatchlistCount($add)
    {
        $this->watchlistCount += $add;
        //var_dump($add);exit;
        return $this;
    }

    /**
     * Set notificationsWatchlist
     *
     * @param boolean $notificationsWatchlist
     * @return User
     */
    public function setNotificationsWatchlist($notificationsWatchlist)
    {
        $this->notificationsWatchlist = $notificationsWatchlist;
    
        return $this;
    }

    /**
     * Get notificationsWatchlist
     *
     * @return boolean 
     */
    public function getNotificationsWatchlist()
    {
        return $this->notificationsWatchlist;
    }

    /**
     * Set notificationsWatchdog
     *
     * @param boolean $notificationsWatchdog
     * @return User
     */
    public function setNotificationsWatchdog($notificationsWatchdog)
    {
        $this->notificationsWatchdog = $notificationsWatchdog;
    
        return $this;
    }

    /**
     * Get notificationsWatchdog
     *
     * @return boolean 
     */
    public function getNotificationsWatchdog()
    {
        return $this->notificationsWatchdog;
    }
}