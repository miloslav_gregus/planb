<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 *
 * @ORM\Table(
 *      name="pb_contact"
 * )
 * @ORM\Entity
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={ "contact_logged", "contact_visitor"})
     * @Assert\Length(
     *     min=3,
     *     groups={ "contact_logged", "contact_visitor"}
     * )
     * @ORM\Column(type="string", length=80)
     */
    private $subject;

    /**
     * @var string
     * @Assert\NotBlank(groups={ "contact_logged", "contact_visitor"})
     * @Assert\Length(
     *     min=10,
     *     groups={ "contact_logged", "contact_visitor"}
     * )
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={ "contact_visitor"})
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={ "contact_visitor"})
     * @ORM\Column(type="string")
     */
    private $username;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    protected $user;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Contact
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Contact
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Contact
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set user
     *
     * @param \Hg\AppBundle\Entity\User $user
     * @return Contact
     */
    public function setUser(\Hg\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Hg\AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}