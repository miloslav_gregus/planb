<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Investment
 *
 * @ORM\Table(
    name="pb_investments",
 *  indexes={
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"})
 *      })
 * )
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\InvestmentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Investment
{
    const STATUS_INSERTED = "inserted";
    const STATUS_CANCELED = "canceled";
    const STATUS_DELETED = "deleted";
    const STATUS_LIMBO = "limbo";
    const STATUS_PREPARED = "prepared";
    const STATUS_PAID = "paid";
    const STATUS_REPAID = "repaid";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT")
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="Loan")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT")
     */
    private $loan;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $amount;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $interest;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct(){
        $this->status = self::STATUS_INSERTED;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return Investment
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set loan
     *
     * @param integer $loan
     * @return Investment
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    
        return $this;
    }

    /**
     * Get loan
     *
     * @return Loan
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Investment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Investment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Investment
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Investment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set interest
     *
     * @param string $interest
     * @return Investment
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;
    
        return $this;
    }

    /**
     * Get interest
     *
     * @return decimal
     */
    public function getInterest()
    {
        return $this->interest;
    }


    /**
     * Get whatever is correct status
     *
     * @param string|array $type
     * @return boolean
     */
    public function isStatus($type)
    {
        if (is_array($type)) {
            return in_array($this->getStatus(), $type);
        }

        return $this->getStatus() == $type;
    }
}