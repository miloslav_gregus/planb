<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(
    name="pb_notifications",
 *  indexes={
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="type_idx", columns={"type"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *      })
 * )
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\NotificationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Notification
{
    const TYPE_AUCTION_DRAFT_REQUESTED = "auction-draft-requested";
    const TYPE_AUCTION_AUCTION_REQUESTED = "auction-auction-requested";
    const TYPE_AUCTION_AUCTION_APPROVE = "auction-auction-approved";

    const TYPE_AUCTION_LAUNCH = "auction-launch";
    const TYPE_AUCTION_ACCEPTED = "auction-accepted";
    const TYPE_AUCTION_CANCELED = "auction-canceled";
    const TYPE_AUCTION_OVERDUE = "auction-overdue";
    const TYPE_AUCTION_PAYABLE = "auction-payable";

    const TYPE_FILE = "file-pending";
    const TYPE_TRANSACTION = "transaction";
    const TYPE_CONTACT = "contact";

    const STATUS_PENDING = "pending";
    const STATUS_DONE = "done";


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Loan")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $loan;

    /**
     * @ORM\ManyToOne(targetEntity="ProfileFile")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $profileFile;

    /**
     * @ORM\ManyToOne(targetEntity="LoanFile")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $loanFile;

    /**
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $contact;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Transaction")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $transaction;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct($type = null){
        $this->status = self::STATUS_PENDING;
        $this->type = $type;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist()
     */
    public function preUpdate()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Notification
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Notification
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set loan
     *
     * @param \Hg\AppBundle\Entity\Loan $loan
     * @return Notification
     */
    public function setLoan(\Hg\AppBundle\Entity\Loan $loan = null)
    {
        $this->loan = $loan;
    
        return $this;
    }

    /**
     * Get loan
     *
     * @return \Hg\AppBundle\Entity\Loan 
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set profileFile
     *
     * @param \Hg\AppBundle\Entity\ProfileFile $profileFile
     * @return Notification
     */
    public function setProfileFile(\Hg\AppBundle\Entity\ProfileFile $profileFile = null)
    {
        $this->profileFile = $profileFile;
    
        return $this;
    }

    /**
     * Get profileFile
     *
     * @return \Hg\AppBundle\Entity\ProfileFile 
     */
    public function getProfileFile()
    {
        return $this->profileFile;
    }

    /**
     * Set loanFile
     *
     * @param \Hg\AppBundle\Entity\LoanFile $loanFile
     * @return Notification
     */
    public function setLoanFile(\Hg\AppBundle\Entity\LoanFile $loanFile = null)
    {
        $this->loanFile = $loanFile;
    
        return $this;
    }

    /**
     * Get loanFile
     *
     * @return \Hg\AppBundle\Entity\LoanFile 
     */
    public function getLoanFile()
    {
        return $this->loanFile;
    }

    /**
     * Set contact
     *
     * @param \Hg\AppBundle\Entity\Contact $contact
     * @return Notification
     */
    public function setContact(\Hg\AppBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \Hg\AppBundle\Entity\Contact 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set user
     *
     * @param \Hg\AppBundle\Entity\User $user
     * @return Notification
     */
    public function setUser(\Hg\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Hg\AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set transaction
     *
     * @param \Hg\AppBundle\Entity\Transaction $transaction
     * @return Notification
     */
    public function setTransaction(\Hg\AppBundle\Entity\Transaction $transaction = null)
    {
        $this->transaction = $transaction;
    
        return $this;
    }

    /**
     * Get transaction
     *
     * @return \Hg\AppBundle\Entity\Transaction 
     */
    public function getTransaction()
    {
        return $this->transaction;
    }


    /**
     * Get whatever is correct type
     *
     * @param string|array $type
     * @return boolean
     */
    public function isStatus($type)
    {
        if (is_array($type)) {
            return in_array($this->getStatus(), $type);
        }

        return $this->getStatus() == $type;
    }
}