<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Watchdog
 *
 * @ORM\Table(
    name="pb_watchdog",
 *   indexes={
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="auction_rating_idx", columns={"auction_rating"}),
 *         @ORM\Index(name="loan_maturity_idx", columns={"loan_maturity"}),
 *         @ORM\Index(name="amount_min_idx", columns={"amount_min"}),
 *         @ORM\Index(name="amount_max_idx", columns={"amount_max"}),
 *         @ORM\Index(name="days_end_min_idx", columns={"days_end_min"}),
 *         @ORM\Index(name="days_end_min_idx", columns={"days_end_max"}),
 *         @ORM\Index(name="interest_average_min_idx", columns={"interest_average_min"}),
 *         @ORM\Index(name="interest_average_max_idx", columns={"interest_average_max"}),
 *         @ORM\Index(name="collected_percentage_min_idx", columns={"collected_percentage_min"}),
 *         @ORM\Index(name="collected_percentage_max_idx", columns={"collected_percentage_max"}),
 *         @ORM\Index(name="updated_at_idx", columns={"updated_at"})
 *    })
 * )
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\WatchdogRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Watchdog
{
    const STATUS_ACTIVE = "active";
    const STATUS_DELETED = "deleted";
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $auctionRating;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $loanMaturity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $amountMin;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $amountMax;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $daysEndMin;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $daysEndMax;

    /**
     * @var integer
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $interestAverageMin;

    /**
     * @var integer
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $interestAverageMax;

    /**
     * @var integer
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $collectedPercentageMin;

    /**
     * @var integer
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $collectedPercentageMax;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    public function __construct(){
        $this->status = self::STATUS_ACTIVE;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Watchdog
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Watchdog
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set auctionRating
     *
     * @param string $auctionRating
     * @return Watchdog
     */
    public function setAuctionRating($auctionRating)
    {
        $auctionRating = is_array($auctionRating) && count($auctionRating) ? "," . implode(",", $auctionRating).","  : ",1,2,3,4,5,";

        $this->auctionRating = $auctionRating;
    
        return $this;
    }

    /**
     * Get auctionRating
     *
     * @return string 
     */
    public function getAuctionRating()
    {
        $ratings = explode(",", $this->auctionRating);
        array_shift($ratings);
        array_pop($ratings);
        return $ratings;
    }

    /**
     * Set loanMaturity
     *
     * @param string $loanMaturity
     * @return Watchdog
     */
    public function setLoanMaturity($loanMaturity)
    {
        $loanMaturity = is_array($loanMaturity) && count($loanMaturity) ? "," . implode(",", $loanMaturity).","  : ",12,24,36,48,60,";

        $this->loanMaturity = $loanMaturity;
    
        return $this;
    }

    /**
     * Get loanMaturity
     *
     * @return string 
     */
    public function getLoanMaturity()
    {
        $maturity = explode(",", $this->loanMaturity);
        array_shift($maturity);
        array_pop($maturity);
        return $maturity;
    }

    /**
     * Set amountMin
     *
     * @param integer $amountMin
     * @return Watchdog
     */
    public function setAmountMin($amountMin)
    {
        $this->amountMin = $amountMin;
    
        return $this;
    }

    /**
     * Get amountMin
     *
     * @return integer 
     */
    public function getAmountMin()
    {
        return $this->amountMin;
    }

    /**
     * Set amountMax
     *
     * @param integer $amountMax
     * @return Watchdog
     */
    public function setAmountMax($amountMax)
    {
        $this->amountMax = $amountMax;
    
        return $this;
    }

    /**
     * Get amountMax
     *
     * @return integer 
     */
    public function getAmountMax()
    {
        return $this->amountMax;
    }

    /**
     * Set daysEndMin
     *
     * @param integer $daysEndMin
     * @return Watchdog
     */
    public function setDaysEndMin($daysEndMin)
    {
        $this->daysEndMin = $daysEndMin;
    
        return $this;
    }

    /**
     * Get daysEndMin
     *
     * @return integer 
     */
    public function getDaysEndMin()
    {
        return $this->daysEndMin;
    }

    /**
     * Set daysEndMax
     *
     * @param integer $daysEndMax
     * @return Watchdog
     */
    public function setDaysEndMax($daysEndMax)
    {
        $this->daysEndMax = $daysEndMax;
    
        return $this;
    }

    /**
     * Get daysEndMax
     *
     * @return integer 
     */
    public function getDaysEndMax()
    {
        return $this->daysEndMax;
    }

    /**
     * Set interestAverageMin
     *
     * @param integer $interestAverageMin
     * @return Watchdog
     */
    public function setInterestAverageMin($interestAverageMin)
    {
        $this->interestAverageMin = $interestAverageMin;
    
        return $this;
    }

    /**
     * Get interestAverageMin
     *
     * @return integer 
     */
    public function getInterestAverageMin()
    {
        return $this->interestAverageMin;
    }

    /**
     * Set interestAverageMax
     *
     * @param integer $interestAverageMax
     * @return Watchdog
     */
    public function setInterestAverageMax($interestAverageMax)
    {
        $this->interestAverageMax = $interestAverageMax;
    
        return $this;
    }

    /**
     * Get interestAverageMax
     *
     * @return integer 
     */
    public function getInterestAverageMax()
    {
        return $this->interestAverageMax;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Watchdog
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Watchdog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Hg\AppBundle\Entity\User $user
     * @return Watchdog
     */
    public function setUser(\Hg\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Hg\AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set collectedPercentageMin
     *
     * @param integer $collectedPercentageMin
     * @return Watchdog
     */
    public function setCollectedPercentageMin($collectedPercentageMin)
    {
        $this->collectedPercentageMin = $collectedPercentageMin;
    
        return $this;
    }

    /**
     * Get collectedPercentageMin
     *
     * @return integer 
     */
    public function getCollectedPercentageMin()
    {
        return $this->collectedPercentageMin;
    }

    /**
     * Set collectedPercentageMax
     *
     * @param integer $collectedPercentageMax
     * @return Watchdog
     */
    public function setCollectedPercentageMax($collectedPercentageMax)
    {
        $this->collectedPercentageMax = $collectedPercentageMax;
    
        return $this;
    }

    /**
     * Get collectedPercentageMax
     *
     * @return integer 
     */
    public function getCollectedPercentageMax()
    {
        return $this->collectedPercentageMax;
    }
}