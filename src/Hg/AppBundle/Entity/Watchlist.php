<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Watchlist
 *
 * @ORM\Table(
    name="pb_watchlist",
 *  indexes={
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="updated_idx", columns={"updated"}),
 *         @ORM\Index(name="updated_at_idx", columns={"updated_at"})
 *    })
 * )
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\WatchlistRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Watchlist
{
    const STATUS_ACTIVE = "active";
    const STATUS_DELETED = "deleted";
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Loan")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $loan;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    private $user;

    /**
     * @var boolean
     *
     * @ORM\Column( type="boolean")
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;



    public function __construct(){
        $this->status = self::STATUS_ACTIVE;
        $this->updated = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set loan
     *
     * @param string $loan
     * @return Watchlist
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    
        return $this;
    }

    /**
     * Get loan
     *
     * @return string 
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return Watchlist
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Watchlist
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Watchlist
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updated
     *
     * @param boolean $updated
     * @return Watchlist
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return boolean 
     */
    public function isUpdated()
    {
        return $this->updated;
    }
    /**
     * Get updated
     *
     * @return boolean
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Watchlist
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}