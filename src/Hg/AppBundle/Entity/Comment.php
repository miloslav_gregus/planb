<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(
    name="pb_comments",
    indexes={
 *    @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *    @ORM\Index(name="status_idx", columns={"status"})
 *  })
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\CommentRepository")
 */
class Comment
{

    const STATUS_ACTIVE = "active";
    const STATUS_DELETED = "deleted";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="string")
     */
    private $status;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="Loan")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $loan;

    /**
     * @var string
     * @Assert\NotBlank(groups={"Default"})
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\OneToOne(targetEntity="Comment", inversedBy="parent")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $child;

    /**
     * @ORM\OneToOne(targetEntity="Comment", mappedBy="children")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;



    public function __construct(){
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Comment
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Hg\AppBundle\Entity\User $user
     * @return Comment
     */
    public function setUser(\Hg\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Hg\AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set loan
     *
     * @param \Hg\AppBundle\Entity\Loan $loan
     * @return Comment
     */
    public function setLoan(\Hg\AppBundle\Entity\Loan $loan = null)
    {
        $this->loan = $loan;
    
        return $this;
    }

    /**
     * Get loan
     *
     * @return \Hg\AppBundle\Entity\Loan 
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set child
     *
     * @param \Hg\AppBundle\Entity\Comment $child
     * @return Comment
     */
    public function setChild(\Hg\AppBundle\Entity\Comment $child = null)
    {
        $this->child = $child;
    
        return $this;
    }

    /**
     * Get child
     *
     * @return \Hg\AppBundle\Entity\Comment 
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Comment
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

   

    /**
     * Set parent
     *
     * @param \Hg\AppBundle\Entity\Comment $parent
     * @return Comment
     */
    public function setParent(\Hg\AppBundle\Entity\Comment $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Hg\AppBundle\Entity\Comment 
     */
    public function getParent()
    {
        return $this->parent;
    }
}