<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LoanObligation
 *
 * @ORM\Table(
 *    name="pb_loan_obligations"
 * )
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class LoanObligation
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Loan", inversedBy="additionalObligations")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $loan;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="integer")
     */
    private $monthly;

    /**
     * @var \DateTime
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="date")
     */
    private $start;
    /**
     * @var \DateTime
     * @Assert\NotBlank(
     *      groups={"Default", "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="date")
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return LoanObligation
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set monthly
     *
     * @param integer $monthly
     * @return LoanObligation
     */
    public function setMonthly($monthly)
    {
        $this->monthly = $monthly;
    
        return $this;
    }

    /**
     * Get monthly
     *
     * @return integer 
     */
    public function getMonthly()
    {
        return $this->monthly;
    }

    /**
     * Set maturity
     *
     * @param \DateTime $maturity
     * @return LoanObligation
     */
    public function setMaturity($maturity)
    {
        $this->maturity = $maturity;
    
        return $this;
    }

    /**
     * Get maturity
     *
     * @return \DateTime 
     */
    public function getMaturity()
    {
        return $this->maturity;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return LoanObligation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set loan
     *
     * @param integer $loan
     * @return LoanObligation
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    
        return $this;
    }

    /**
     * Get loan
     *
     * @return integer 
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LoanObligation
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return LoanObligation
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return LoanObligation
     */
    public function setStart($start)
    {
        $this->start = $start;
    
        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return LoanObligation
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }
}