<?php

namespace Hg\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Loan
 *
 * @ORM\Table(
 *      name="pb_loans",
 *      indexes={
 *         @ORM\Index(name="status_idx", columns={"status"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *         @ORM\Index(name="loan_maturity_idx", columns={"loan_maturity"}),
 *         @ORM\Index(name="auction_approved_amount_idx", columns={"auction_approved_amount"}),
 *         @ORM\Index(name="auction_rating_idx", columns={"auction_rating"}),
 *         @ORM\Index(name="auction_start_at_idx", columns={"auction_start_at"}),
 *         @ORM\Index(name="auction_end_at_idx", columns={"auction_end_at"}),
 *         @ORM\Index(name="auction_interest_average_idx", columns={"auction_interest_average"}),
 *         @ORM\Index(name="auction_collected_percentage_idx", columns={"auction_collected_percentage"})
 *      })
 * )
 * @ORM\Entity(repositoryClass="Hg\AppBundle\Entity\Repository\LoanRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Loan
{
    const STATUS_DRAFT = "draft";
    const STATUS_DRAFT_REQUESTED = "draft-requested";
    const STATUS_DRAFT_APPROVED = "draft-approved";
    const STATUS_DRAFT_CANCELED = "draft-canceled";
    const STATUS_AUCTION_DRAFT = "auction-draft";
    const STATUS_AUCTION_REQUESTED = "auction-requested";
    const STATUS_AUCTION_SCORING = "auction-scoring";
    const STATUS_AUCTION_CANCELED = "auction-canceled";
    const STATUS_AUCTION_APPROVED = "auction-approved";
    const STATUS_AUCTION = "auction";
    const STATUS_CANCELED = "canceled";
    const STATUS_OVERDUE = "overdue";
    const STATUS_ACCEPTED = "accepted";
    const STATUS_REAUCTION = "reauction";
    const STATUS_PAYABLE = "payable";
    const STATUS_LOAN = "loan";
    const STATUS_REPAID = "repaid";


    const RATING_AA = 1;
    const RATING_A = 2;
    const RATING_B = 3;
    const RATING_C = 4;
    const RATING_D = 5;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Profile")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    protected $profile;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"auctionTitle"}, updatable=false, separator="-", unique=true)
     *
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var integer
     * @Assert\NotBlank(groups={ "form_auction_approve"})
     * @ORM\Column(type="smallint")
     */
    private $loanMaturity;

    /**
     * @var integer
     * @Assert\NotBlank(groups={ "form_auction_approve"})
     * @ORM\Column(type="smallint")
     */
    private $loanPurpose;

    /**
     * @var date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $loanStartAt;

    /**
     * @var date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $loanEndAt;

    /**
     * @var integer
     * @Assert\NotBlank(groups={ "form_auction_approve"})
     * @ORM\Column(type="integer", length=2)
     */
    private $auctionRating;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(groups={ "flow_hg_loan_validate_step1", "form_auction_approve"})
     * @Assert\Length(
     *     min=5, groups={ "flow_hg_loan_validate_step1", "form_auction_approve"},
     *     max=50, groups={ "flow_hg_loan_validate_step1", "form_auction_approve"}
     * )
     */
    private $auctionTitle;

    /**
     * @var integer
     *
     * @Assert\NotBlank(groups={ "flow_hg_loan_validate_step1"})
     * @Assert\Range(
     *     min=300, groups={ "flow_hg_loan_validate_step1"},
     *     max=10000, groups={ "flow_hg_loan_validate_step1"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $auctionRequestedAmount;

    /**
     * @var integer
     *
     * @Assert\NotBlank(groups={"form_auction_approve"})
     * @Assert\Range(
     *     min=300, groups={"form_auction_approve"},
     *     max=10000, groups={"form_auction_approve"}
     * )
     *
     * @ORM\Column(type="smallint")
     */
    private $auctionApprovedAmount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $auctionStartAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $auctionEndAt;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $auctionInterestRecommemded;
    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $auctionInterestAverage;
    /**
     * @var integer
     *
     * @ORM\Column(type="smallint" )
     */
    private $auctionCollectedAmount;
    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=4)
     */
    private $auctionCollectedPercentage;
    /**
     * @var double
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $feeAmount;
    /**
     * @var double
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $feeLifebuoy;
    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4"}
     * )
     * @ORM\Column(type="integer", length=255)
     */
    private $incomeType;

    /**
     * @var integer
     *
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub1", "flow_hg_loan_validate_step4_sub2", "flow_hg_loan_validate_step4_sub3"}
     * )
     * @ORM\Column(type="integer", length=255)
     */
    private $incomeSector;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub1", "flow_hg_loan_validate_step4_sub2", "flow_hg_loan_validate_step4_sub3", "flow_hg_loan_validate_step4_sub4", "flow_hg_loan_validate_step4_sub5", "flow_hg_loan_validate_step4_sub7"}
     * )
     * @ORM\Column(type="integer", length=255)
     */
    private $incomeEmploymentType;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub1", "flow_hg_loan_validate_step4_sub2"}
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $incomeEmployerName;
    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub1", "flow_hg_loan_validate_step4_sub2"}
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $incomeEmploymentPosition;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub1", "flow_hg_loan_validate_step4_sub3", "flow_hg_loan_validate_step4_sub5", "flow_hg_loan_validate_step4_sub7"}
     * )
     * @ORM\Column(type="date", length=255, nullable=true)
     */
    private $incomeStartAt;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub2"}
     * )
     * @ORM\Column(type="date", length=255, nullable=true)
     */
    private $incomeEndAt;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_loan_validate_step4_sub8", "flow_hg_loan_validate_step4_sub3_others", "flow_hg_loan_validate_step4_sub4_others"}
     * )
     * @ORM\Column(type="string", length=100)
     */
    private $incomeOther;

    /**
     * @var integer
     *
     * @ORM\Column(type="text")
     */
    private $additionalObligationInfo;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @Assert\Range(
     *      min = 0,
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="smallint", length=255)
     */
    private $additionalIncomes;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @Assert\Range(
     *      min = 0,
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $additionalIncomesOthers;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @Assert\Range(
     *      min = 0,
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $additionalIncomesPartner;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @Assert\Range(
     *      min = 0,
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $additionalOutcomesHousehold;

    /**
     * @var integer
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @Assert\Range(
     *      min = 0,
     *      groups={ "flow_hg_auction_validate_step2"}
     * )
     * @ORM\Column(type="smallint")
     */
    private $additionalKids;

    /**
     * @var integer
     *
     * @ORM\Column(type="text")
     */
    private $additionalOutcomesInfo;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step3"}
     * )
     * @ORM\Column(type="text")
     */
    private $additionalWhyBorrowInfo;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={ "flow_hg_auction_validate_step3"}
     * )
     * @ORM\Column(type="text")
     */
    private $additionalWhySecureInfo;

    /**
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $repaymentDay;

    /**
     * @ORM\OneToMany(targetEntity="LoanObligation", mappedBy="loan", cascade={"persist"}, orphanRemoval=true)
     */
    private $additionalObligations;
    /**
     * @ORM\OneToMany(targetEntity="LoanFile", mappedBy="loan", cascade={"persist"}, orphanRemoval=true)
     */
    private $additionalFiles;

    /**
     * @ORM\OneToMany(targetEntity="Investment", mappedBy="loan", orphanRemoval=true)
     */
    private $investments;



    public function __construct(){
        $this->status = self::STATUS_DRAFT;
        $this->auctionRating = 0;
        $this->auctionRequestedAmount = 10000;
        $this->auctionApprovedAmount = 0;
        $this->auctionInterestRecommemded = 0;
        $this->auctionInterestAverage = 0;
        $this->auctionCollectedAmount = 0;
        $this->auctionCollectedPercentage = 0;
        $this->incomeType = 0;
        $this->incomeSector = 0;
        $this->incomeEmploymentType = 0;
        $this->incomeEmployerName = "";
        $this->incomeEmploymentPosition = "";
        $this->incomeOther = "";
        $this->additionalObligationInfo = "";
        $this->additionalIncomes = 0;
        $this->additionalIncomesOthers = 0;
        $this->additionalIncomesPartner = 0;
        $this->additionalOutcomesHousehold = 0;
        $this->additionalKids = 0;
        $this->additionalOutcomesInfo = "";
        $this->additionalWhyBorrowInfo = "";
        $this->additionalWhySecureInfo = "";
        $this->additionalObligations = new ArrayCollection();
        $this->additionalFiles = new ArrayCollection();
        $this->repaymentDay = 20;
        $this->feeAmount = 0;
        $this->feeLifebuoy = 0;
    }


    public function getFormattedId(){
        $id = $this->getId() + 3000000000;
        return $id;
    }


    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get whatever is correct type
     *
     * @param string|array $type
     * @return boolean
     */
    public function isStatus($type)
    {
        if (is_array($type)) {
            return in_array($this->getStatus(), $type);
        }

        return $this->getStatus() == $type;
    }





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Loan
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Loan
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Loan
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Loan
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set loanMaturity
     *
     * @param integer $loanMaturity
     * @return Loan
     */
    public function setLoanMaturity($loanMaturity)
    {
        $this->loanMaturity = $loanMaturity;
    
        return $this;
    }

    /**
     * Get loanMaturity
     *
     * @return integer 
     */
    public function getLoanMaturity()
    {
        return $this->loanMaturity;
    }

    /**
     * Set loanPurpose
     *
     * @param integer $loanPurpose
     * @return Loan
     */
    public function setLoanPurpose($loanPurpose)
    {
        $this->loanPurpose = $loanPurpose;
    
        return $this;
    }

    /**
     * Get loanPurpose
     *
     * @return integer 
     */
    public function getLoanPurpose()
    {
        return $this->loanPurpose;
    }

    /**
     * Set loanStartAt
     *
     * @param \DateTime $loanStartAt
     * @return Loan
     */
    public function setLoanStartAt($loanStartAt)
    {
        $this->loanStartAt = $loanStartAt;
    
        return $this;
    }

    /**
     * Get loanStartAt
     *
     * @return \DateTime 
     */
    public function getLoanStartAt()
    {
        return $this->loanStartAt;
    }

    /**
     * Set loanEndAt
     *
     * @param \DateTime $loanEndAt
     * @return Loan
     */
    public function setLoanEndAt($loanEndAt)
    {
        $this->loanEndAt = $loanEndAt;
    
        return $this;
    }

    /**
     * Get loanEndAt
     *
     * @return \DateTime 
     */
    public function getLoanEndAt()
    {
        return $this->loanEndAt;
    }

    /**
     * Set auctionRating
     *
     * @param integer $auctionRating
     * @return Loan
     */
    public function setAuctionRating($auctionRating)
    {
        $this->auctionRating = $auctionRating;
    
        return $this;
    }

    /**
     * Get auctionRating
     *
     * @return integer 
     */
    public function getAuctionRating()
    {
        return $this->auctionRating;
    }

    /**
     * Set auctionTitle
     *
     * @param string $auctionTitle
     * @return Loan
     */
    public function setAuctionTitle($auctionTitle)
    {
        $this->auctionTitle = $auctionTitle;
    
        return $this;
    }

    /**
     * Get auctionTitle
     *
     * @return string 
     */
    public function getAuctionTitle()
    {
        return $this->auctionTitle;
    }

    /**
     * Set auctionRequestedAmount
     *
     * @param integer $auctionRequestedAmount
     * @return Loan
     */
    public function setAuctionRequestedAmount($auctionRequestedAmount)
    {
        $this->auctionRequestedAmount = $auctionRequestedAmount;
    
        return $this;
    }

    /**
     * Get auctionRequestedAmount
     *
     * @return integer 
     */
    public function getAuctionRequestedAmount()
    {
        return $this->auctionRequestedAmount;
    }

    /**
     * Set auctionApprovedAmount
     *
     * @param integer $auctionApprovedAmount
     * @return Loan
     */
    public function setAuctionApprovedAmount($auctionApprovedAmount)
    {
        $this->auctionApprovedAmount = $auctionApprovedAmount;
    
        return $this;
    }

    /**
     * Get auctionApprovedAmount
     *
     * @return integer 
     */
    public function getAuctionApprovedAmount()
    {
        return $this->auctionApprovedAmount;
    }

    /**
     * Set auctionStartAt
     *
     * @param \DateTime $auctionStartAt
     * @return Loan
     */
    public function setAuctionStartAt($auctionStartAt)
    {
        $this->auctionStartAt = $auctionStartAt;
    
        return $this;
    }

    /**
     * Get auctionStartAt
     *
     * @return \DateTime 
     */
    public function getAuctionStartAt()
    {
        return $this->auctionStartAt;
    }

    /**
     * Set auctionEndAt
     *
     * @param \DateTime $auctionEndAt
     * @return Loan
     */
    public function setAuctionEndAt($auctionEndAt)
    {
        $this->auctionEndAt = $auctionEndAt;
    
        return $this;
    }

    /**
     * Get auctionEndAt
     *
     * @return \DateTime 
     */
    public function getAuctionEndAt()
    {
        return $this->auctionEndAt;
    }

    /**
     * Set auctionInterestRecommemded
     *
     * @param string $auctionInterestRecommemded
     * @return Loan
     */
    public function setAuctionInterestRecommemded($auctionInterestRecommemded)
    {
        $this->auctionInterestRecommemded = $auctionInterestRecommemded;
    
        return $this;
    }

    /**
     * Get auctionInterestRecommemded
     *
     * @return string 
     */
    public function getAuctionInterestRecommemded($full = false)
    {
        return $this->auctionInterestRecommemded * ($full ? 100 : 1);
    }

    /**
     * Set auctionInterestAverage
     *
     * @param string $auctionInterestAverage
     * @return Loan
     */
    public function setAuctionInterestAverage($auctionInterestAverage)
    {
        $this->auctionInterestAverage = $auctionInterestAverage;
    
        return $this;
    }

    /**
     * Get auctionInterestAverage
     *
     * @return string 
     */
    public function getAuctionInterestAverage()
    {
        return $this->auctionInterestAverage;
    }

    /**
     * Set auctionCollectedAmount
     *
     * @param integer $auctionCollectedAmount
     * @return Loan
     */
    public function setAuctionCollectedAmount($auctionCollectedAmount)
    {
        $this->auctionCollectedAmount = $auctionCollectedAmount;
    
        return $this;
    }

    /**
     * Get auctionCollectedAmount
     *
     * @return integer 
     */
    public function getAuctionCollectedAmount()
    {
        return $this->auctionCollectedAmount;
    }

    /**
     * Set auctionCollectedPercentage
     *
     * @param string $auctionCollectedPercentage
     * @return Loan
     */
    public function setAuctionCollectedPercentage($auctionCollectedPercentage)
    {
        $this->auctionCollectedPercentage = $auctionCollectedPercentage;
    
        return $this;
    }

    /**
     * Get auctionCollectedPercentage
     *
     * @return string 
     */
    public function getAuctionCollectedPercentage()
    {
        return $this->auctionCollectedPercentage;
    }

    /**
     * Set incomeType
     *
     * @param integer $incomeType
     * @return Loan
     */
    public function setIncomeType($incomeType)
    {
        $this->incomeType = $incomeType;
    
        return $this;
    }

    /**
     * Get incomeType
     *
     * @return integer 
     */
    public function getIncomeType()
    {
        return $this->incomeType;
    }

    /**
     * Set incomeSector
     *
     * @param integer $incomeSector
     * @return Loan
     */
    public function setIncomeSector($incomeSector)
    {
        $this->incomeSector = $incomeSector;
    
        return $this;
    }

    /**
     * Get incomeSector
     *
     * @return integer 
     */
    public function getIncomeSector()
    {
        return $this->incomeSector;
    }

    /**
     * Set incomeEmploymentType
     *
     * @param integer $incomeEmploymentType
     * @return Loan
     */
    public function setIncomeEmploymentType($incomeEmploymentType)
    {
        $this->incomeEmploymentType = $incomeEmploymentType;
    
        return $this;
    }

    /**
     * Get incomeEmploymentType
     *
     * @return integer 
     */
    public function getIncomeEmploymentType()
    {
        return $this->incomeEmploymentType;
    }

    /**
     * Set incomeEmployerName
     *
     * @param string $incomeEmployerName
     * @return Loan
     */
    public function setIncomeEmployerName($incomeEmployerName)
    {
        $this->incomeEmployerName = $incomeEmployerName;
    
        return $this;
    }

    /**
     * Get incomeEmployerName
     *
     * @return string 
     */
    public function getIncomeEmployerName()
    {
        return $this->incomeEmployerName;
    }

    /**
     * Set incomeEmploymentPosition
     *
     * @param string $incomeEmploymentPosition
     * @return Loan
     */
    public function setIncomeEmploymentPosition($incomeEmploymentPosition)
    {
        $this->incomeEmploymentPosition = $incomeEmploymentPosition;
    
        return $this;
    }

    /**
     * Get incomeEmploymentPosition
     *
     * @return string 
     */
    public function getIncomeEmploymentPosition()
    {
        return $this->incomeEmploymentPosition;
    }

    /**
     * Set incomeStartAt
     *
     * @param \DateTime $incomeStartAt
     * @return Loan
     */
    public function setIncomeStartAt($incomeStartAt)
    {
        $this->incomeStartAt = $incomeStartAt;
    
        return $this;
    }

    /**
     * Get incomeStartAt
     *
     * @return \DateTime 
     */
    public function getIncomeStartAt()
    {
        return $this->incomeStartAt;
    }

    /**
     * Set incomeEndAt
     *
     * @param \DateTime $incomeEndAt
     * @return Loan
     */
    public function setIncomeEndAt($incomeEndAt)
    {
        $this->incomeEndAt = $incomeEndAt;
    
        return $this;
    }

    /**
     * Get incomeEndAt
     *
     * @return \DateTime 
     */
    public function getIncomeEndAt()
    {
        return $this->incomeEndAt;
    }

    /**
     * Set incomeOther
     *
     * @param string $incomeOther
     * @return Loan
     */
    public function setIncomeOther($incomeOther)
    {
        $this->incomeOther = $incomeOther;
    
        return $this;
    }

    /**
     * Get incomeOther
     *
     * @return string 
     */
    public function getIncomeOther()
    {
        return $this->incomeOther;
    }

    /**
     * Set additionalObligationInfo
     *
     * @param string $additionalObligationInfo
     * @return Loan
     */
    public function setAdditionalObligationInfo($additionalObligationInfo)
    {
        $this->additionalObligationInfo = $additionalObligationInfo;
    
        return $this;
    }

    /**
     * Get additionalObligationInfo
     *
     * @return string 
     */
    public function getAdditionalObligationInfo()
    {
        return $this->additionalObligationInfo;
    }

    /**
     * Set additionalIncomes
     *
     * @param integer $additionalIncomes
     * @return Loan
     */
    public function setAdditionalIncomes($additionalIncomes)
    {
        $this->additionalIncomes = $additionalIncomes;
    
        return $this;
    }

    /**
     * Get additionalIncomes
     *
     * @return integer 
     */
    public function getAdditionalIncomes()
    {
        return $this->additionalIncomes;
    }

    /**
     * Set additionalIncomesOthers
     *
     * @param integer $additionalIncomesOthers
     * @return Loan
     */
    public function setAdditionalIncomesOthers($additionalIncomesOthers)
    {
        $this->additionalIncomesOthers = $additionalIncomesOthers;
    
        return $this;
    }

    /**
     * Get additionalIncomesOthers
     *
     * @return integer 
     */
    public function getAdditionalIncomesOthers()
    {
        return $this->additionalIncomesOthers;
    }

    /**
     * Set additionalIncomesPartner
     *
     * @param integer $additionalIncomesPartner
     * @return Loan
     */
    public function setAdditionalIncomesPartner($additionalIncomesPartner)
    {
        $this->additionalIncomesPartner = $additionalIncomesPartner;
    
        return $this;
    }

    /**
     * Get additionalIncomesPartner
     *
     * @return integer 
     */
    public function getAdditionalIncomesPartner()
    {
        return $this->additionalIncomesPartner;
    }

    /**
     * Set additionalOutcomesHousehold
     *
     * @param integer $additionalOutcomesHousehold
     * @return Loan
     */
    public function setAdditionalOutcomesHousehold($additionalOutcomesHousehold)
    {
        $this->additionalOutcomesHousehold = $additionalOutcomesHousehold;
    
        return $this;
    }

    /**
     * Get additionalOutcomesHousehold
     *
     * @return integer 
     */
    public function getAdditionalOutcomesHousehold()
    {
        return $this->additionalOutcomesHousehold;
    }

    /**
     * Set additionalKids
     *
     * @param integer $additionalKids
     * @return Loan
     */
    public function setAdditionalKids($additionalKids)
    {
        $this->additionalKids = $additionalKids;
    
        return $this;
    }

    /**
     * Get additionalKids
     *
     * @return integer 
     */
    public function getAdditionalKids()
    {
        return $this->additionalKids;
    }

    /**
     * Set additionalOutcomesInfo
     *
     * @param string $additionalOutcomesInfo
     * @return Loan
     */
    public function setAdditionalOutcomesInfo($additionalOutcomesInfo)
    {
        $this->additionalOutcomesInfo = $additionalOutcomesInfo;
    
        return $this;
    }

    /**
     * Get additionalOutcomesInfo
     *
     * @return string 
     */
    public function getAdditionalOutcomesInfo()
    {
        return $this->additionalOutcomesInfo;
    }

    /**
     * Set additionalWhyBorrowInfo
     *
     * @param string $additionalWhyBorrowInfo
     * @return Loan
     */
    public function setAdditionalWhyBorrowInfo($additionalWhyBorrowInfo)
    {
        $this->additionalWhyBorrowInfo = $additionalWhyBorrowInfo;
    
        return $this;
    }

    /**
     * Get additionalWhyBorrowInfo
     *
     * @return string 
     */
    public function getAdditionalWhyBorrowInfo()
    {
        return $this->additionalWhyBorrowInfo;
    }

    /**
     * Set additionalWhySecureInfo
     *
     * @param string $additionalWhySecureInfo
     * @return Loan
     */
    public function setAdditionalWhySecureInfo($additionalWhySecureInfo)
    {
        $this->additionalWhySecureInfo = $additionalWhySecureInfo;
    
        return $this;
    }

    /**
     * Get additionalWhySecureInfo
     *
     * @return string 
     */
    public function getAdditionalWhySecureInfo()
    {
        return $this->additionalWhySecureInfo;
    }

    /**
     * Set user
     *
     * @param \Hg\AppBundle\Entity\User $user
     * @return Loan
     */
    public function setUser(\Hg\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Hg\AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set profile
     *
     * @param \Hg\AppBundle\Entity\Profile $profile
     * @return Loan
     */
    public function setProfile(\Hg\AppBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return \Hg\AppBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Add additionalObligations
     *
     * @param \Hg\AppBundle\Entity\LoanObligation $additionalObligations
     * @return Loan
     */
    public function addAdditionalObligation(\Hg\AppBundle\Entity\LoanObligation $additionalObligations)
    {
        $additionalObligations->setLoan($this);
        $this->additionalObligations[] = $additionalObligations;
    
        return $this;
    }

    /**
     * Remove additionalObligations
     *
     * @param \Hg\AppBundle\Entity\LoanObligation $additionalObligations
     */
    public function removeAdditionalObligation(\Hg\AppBundle\Entity\LoanObligation $additionalObligations)
    {
        $this->additionalObligations->removeElement($additionalObligations);
    }

    /**
     * Get additionalObligations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdditionalObligations()
    {
        return $this->additionalObligations;
    }

    /**
     * Add additionalFiles
     *
     * @param \Hg\AppBundle\Entity\LoanFile $additionalFiles
     * @return Loan
     */
    public function addAdditionalFile(\Hg\AppBundle\Entity\LoanFile $additionalFiles)
    {
        $additionalFiles->setLoan($this);
        $this->additionalFiles[] = $additionalFiles;
    
        return $this;
    }

    /**
     * Remove additionalFiles
     *
     * @param \Hg\AppBundle\Entity\LoanFile $additionalFiles
     */
    public function removeAdditionalFile(\Hg\AppBundle\Entity\LoanFile $additionalFiles)
    {
        $this->additionalFiles->removeElement($additionalFiles);
    }

    /**
     * Get additionalFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdditionalFiles()
    {
        return $this->additionalFiles;
    }

    /**
     * Add investments
     *
     * @param \Hg\AppBundle\Entity\Investment $investments
     * @return Loan
     */
    public function addInvestment(\Hg\AppBundle\Entity\Investment $investments)
    {
        $this->investments[] = $investments;
    
        return $this;
    }

    /**
     * Remove investments
     *
     * @param \Hg\AppBundle\Entity\Investment $investments
     */
    public function removeInvestment(\Hg\AppBundle\Entity\Investment $investments)
    {
        $this->investments->removeElement($investments);
    }

    /**
     * Get investments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvestments()
    {
        return $this->investments;
    }

    /**
     * Get investments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function isActive()
    {
        return $this->isStatus(self::STATUS_AUCTION);
    }

    /**
     * Get investments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActiveText()
    {
        return $this->isActive() ? "active" : "inactive";
    }


    public function __toString() {
        return $this->getAuctionTitle();
    }



    /**
     * Set repaymentDay
     *
     * @param integer $repaymentDay
     * @return Loan
     */
    public function setRepaymentDay($repaymentDay)
    {
        $this->repaymentDay = $repaymentDay;
    
        return $this;
    }

    /**
     * Get repaymentDay
     *
     * @return integer 
     */
    public function getRepaymentDay()
    {
        return $this->repaymentDay;
    }

    /**
     * Set feeAmount
     *
     * @param string $feeAmount
     * @return Loan
     */
    public function setFeeAmount($feeAmount)
    {
        $this->feeAmount = $feeAmount;
    
        return $this;
    }

    /**
     * Get feeAmount
     *
     * @return string 
     */
    public function getFeeAmount()
    {
        return $this->feeAmount;
    }

    /**
     * Set feeLifebuoy
     *
     * @param string $feeLifebuoy
     * @return Loan
     */
    public function setFeeLifebuoy($feeLifebuoy)
    {
        $this->feeLifebuoy = $feeLifebuoy;

        return $this;
    }

    /**
     * Get feeLifebuoy
     *
     * @return string
     */
    public function getFeeLifebuoy()
    {
        return $this->feeLifebuoy;
    }

    /**
     * Get feeAmount
     *
     * @return string
     */
    public function getCollectedAmountWithoutFee()
    {
        return $this->auctionCollectedAmount - $this->feeAmount - $this->feeLifebuoy;
    }
}