<?php

namespace Hg\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Hg\AppBundle\Entity\Notification;

/**
 * NotificationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NotificationRepository extends EntityRepository
{
    public function countsByStatus($status = array(Notification::STATUS_PENDING))
    {
        $query = $this->createQueryBuilder("n")
            ->select("COUNT(n.id)");

        if (count($status)) {
            $query->andWhere('n.status IN(:status)')
                ->setParameter('status', $status);
        };

        return $query->getQuery()->getSingleScalarResult();
    }
}
