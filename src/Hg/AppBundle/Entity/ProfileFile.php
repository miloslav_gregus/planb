<?php

namespace Hg\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Profile Files
 *
 * @ORM\Table(
 *    name="pb_profile_files",
 *    indexes={
 *       @ORM\Index(name="type_idx", columns={"type"})
 *    }
 * )
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 *
 * @Assert\Callback(
 *    methods={"isFileAlreadyUploaded"},
 *    groups={"flow_hg_auction_validate_step3"}
 * )
 */
class ProfileFile
{
    const STATUS_PENDING = "pending";
    const STATUS_SUCCESS = "success";
    const STATUS_DELETED = "deleted";
    const STATUS_MISSING = "missing";
    const STATUS_DELIVERED = "delivered";


    const TYPE_DATA_PROCESSING = "data_processing";
    const TYPE_FRAMEWORK_CONTRACT = "framework_contract";
    const TYPE_TERMS_CONDITIONS = "terms_conditions";
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $status;
    /**
     * @var string
     * @ORM\Column(type="array", nullable=true)
     */
    private $pages;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $mandatory;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Profile", inversedBy="files")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $profile;

    /**
     * @var string
     * @Assert\NotBlank(
     *      groups={"hg_form_upload_profile_files"}
     * )
     * @ORM\Column(type="string")
     */
    private $type;
    /**
     * @Assert\NotBlank(
     *      groups={"hg_form_upload_profile_files"}
     * )
     * @Assert\File(
     *      maxSize="60M",
     *      groups={"hg_form_upload_profile_files"}
     * )
     */
    private $file;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $filename;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=8)
     */
    private $ext;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    
    public function __construct()
    {
        $this->status = self::STATUS_PENDING;
        $this->filename = "";
        $this->ext = "";
        $this->mandatory = true;
    }
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->filename)) {
            // store the old name to delete after the update
            $this->temp = $this->getPath();
            $this->filename = "";
            $this->ext = "";
        } else {
            $this->filename = 'initial';
            $this->ext = "";
        }
    }


    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getPath()
    {
        return $this->filename . ($this->ext ? "." . $this->ext : "");
    }

    public function getAbsolutePath()
    {
        return null === $this->getPath()
            ? null
            : $this->getUploadRootDir() . '/' . $this->getPath();
    }

    public function getWebPath()
    {
        return null === $this->getPath()
            ? null
            : $this->getUploadDir() . '/' . $this->getPath();
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../data/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        $username = $this->getProfile()->getUser()->getUsername();
        return 'profiles/' . $username;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $originalName = $this->getFile()->getClientOriginalName();
            $originalName = pathinfo($originalName, PATHINFO_FILENAME);
            $this->filename = Urlizer::urlize($originalName . "-" . uniqid());

            $this->ext = (string)$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->getAbsolutePath());

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            @unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            @unlink($file);
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    public function isFileAlreadyUploaded(ExecutionContextInterface $context)
    {

        if ($this->file === null && !$this->filename) {
            $context->addViolationAt('file', 'File wasnt uploaded yet', array(), null);
        }

    }

    public function getNiceFilename()
    {

        $niceFilename = substr($this->filename, 0, strpos($this->filename, strrchr($this->filename, "-")));

        return $niceFilename . ($this->ext ? "." . $this->ext : "");
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return ProfileFile
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set ext
     *
     * @param string $ext
     * @return ProfileFile
     */
    public function setExt($ext)
    {
        $this->ext = $ext;

        return $this;
    }

    /**
     * Get ext
     *
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ProfileFile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set profile
     *
     * @param \Hg\AppBundle\Entity\Profile $profile
     * @return ProfileFile
     */
    public function setProfile(\Hg\AppBundle\Entity\Profile $profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \Hg\AppBundle\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProfileFile
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set pages
     *
     * @param array $pages
     * @return ProfileFile
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
    
        return $this;
    }

    /**
     * Get pages
     *
     * @return array 
     */
    public function getPages()
    {
        return $this->pages;
    }



    public function setFileContent($content, $name = null){
        $name = $name ? $name : uniqid();

        $path = sys_get_temp_dir() . "/" . uniqid();
        file_put_contents($path, $content);

        $uploadedFile = new UploadedFile($path, $name, null, null, null, true);

        $this->setFile($uploadedFile);

        return $this;
    }

    public function getFileContent(){
        return file_get_contents($this->getAbsolutePath());
    }

    /**
     * Set mandatory
     *
     * @param boolean $mandatory
     * @return ProfileFile
     */
    public function setMandatory($mandatory)
    {
        $this->mandatory = $mandatory;
    
        return $this;
    }

    /**
     * Get mandatory
     *
     * @return boolean 
     */
    public function getMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ProfileFile
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Get title for transalation
     *
     * @return string
     */
    public function getTitle()
    {
        return "file_type." . $this->type;
    }

    /**
     * Get tooltip
     *
     * @return string
     */
    public function getTooltip()
    {
        if(0){
            return "file_type." . $this->type . "_tooltip";
        }

        return null;
    }
}