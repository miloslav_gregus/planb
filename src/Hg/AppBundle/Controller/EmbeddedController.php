<?php

namespace Hg\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class EmbeddedController extends Controller
{
    /**
     * @Template()
     */
    public function latestAuctionsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loans = $em->getRepository("AppBundle:Loan")->findLastest(3);

        return array("loans" => $loans);
    }

    /**
     * @Template()
     */
    public function latestAuctionsSidebarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loans = $em->getRepository("AppBundle:Loan")->findLastest(3);

        return array("loans" => $loans);
    }


}
