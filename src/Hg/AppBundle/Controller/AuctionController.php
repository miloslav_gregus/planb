<?php

namespace Hg\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Comment;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Repository\InvestmentRepository;
use Hg\AppBundle\Entity\Repository\LoanRepository;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Entity\Watchdog;
use Hg\AppBundle\Entity\Watchlist;
use Hg\AppBundle\Event\AnswerEvent;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\Event\LoanEvent;
use Hg\AppBundle\Event\QuestionEvent;
use Hg\AppBundle\HgEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Hg\AppBundle\Entity\Loan;
use Symfony\Component\Validator\Constraints\DateTime;


class AuctionController extends Controller
{
    /**
     * @Route("/aukcie/{slug}", name="hg_app_auction_detail")
     * @Template()
     */
    public function detailAction(Request $request, $slug)
    {

        $em = $this->getDoctrine()->getManager();

        /** @var Loan $loan */
        $loan = $em->getRepository('AppBundle:Loan')->findOneBy(array(
            'slug' => $slug
        ));

        if (!$loan) {
            throw new NotFoundHttpException;
        }

        if($loan->isStatus($loan::STATUS_AUCTION)){
            $seconds = $this->getDiffSeconds($loan->getAuctionEndAt());
            if($seconds < 0){
                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_OVERDUE, new LoanEvent($loan));
            }
        }

        if ($request->isXmlHttpRequest()) {

            if ($request->getMethod() == 'GET') {

                if ($request->query->has("answer")) {
                    return $this->forward('AppBundle:Auction:comments', array(
                            "request" => $request,
                            "loanId" => $loan->getId())
                    );
                } elseif ($request->query->has("question")) {
                    return $this->forward('AppBundle:Auction:comments', array(
                            "request" => $request,
                            "loanId" => $loan->getId())
                    );
                } else {
                    return $this->forward('AppBundle:Auction:investments', array(
                            "request" => $request,
                            "loanId" => $loan->getId())
                    );
                }
            }

            if ($request->getMethod() == 'POST') {

                if ($request->request->get("form_question_add") ||
                    $request->request->get("form_answer_add")
                ) {
                    return $this->forward('AppBundle:Auction:comments', array(
                            "request" => $request,
                            "loanId" => $loan->getId())
                    );
                } else {
                    return $this->forward('AppBundle:Auction:investmentAdd', array(
                            "request" => $request,
                            "loanId" => $loan->getId())
                    );
                }
            }
        }



        if (!$loan->isStatus(array(
            Loan::STATUS_AUCTION,
            Loan::STATUS_ACCEPTED,
            Loan::STATUS_LOAN,
            Loan::STATUS_PAYABLE,
            Loan::STATUS_REPAID,
            Loan::STATUS_OVERDUE,
            Loan::STATUS_CANCELED,
        ))
        ) {
            throw new NotFoundHttpException('Auction not found');
        }

        $user = $this->getUser();
        if($user && $user->isType(User::TYPE_INVESTOR)){
            /** @var Watchlist $watchlist */
            $watchlist = $em->getRepository('AppBundle:Watchlist')->findOneBy(array(
                "user" => $user,
                "loan" => $loan,
                "status" => Watchlist::STATUS_ACTIVE
            ));
            if($watchlist && $watchlist->isUpdated()){
                $watchlist->setUpdated(false);
                $watchlist->getUser()->addWatchlistCount(-1);
                $em->flush();
            }
        }

        return array("loan" => $loan);
    }

    /**
     * @Route("/aukcie", name="hg_app_auction_index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $filter = $request->query->get('filter');

        if ($request->isXmlHttpRequest()) {
            return $this->forward('AppBundle:Auction:list', array("request" => $request));
        }

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $watchdogs = null;
        if($user && $user->isType(User::TYPE_INVESTOR)){
            $watchdogs = $em->getRepository('AppBundle:Watchdog')->findBy(array(
                "user" => $user,
                "status" => Watchdog::STATUS_ACTIVE,
            ));
        }

        return array(
            "filter" => $filter,
            "watchdogs" => $watchdogs
        );
    }

    /**
     * @Template()
     */
    public function listAction(Request $request)
    {
        $filter = $request->query->get('filter');
        $filter['status'] = isset($filter['status']) ? $filter['status'] : "active";

        $paginator = $this->get('knp_paginator');

        $em = $this->getDoctrine()->getManager();

        /** @var LoanRepository $loanRepository */
        $loanRepository = $em->getRepository('AppBundle:Loan');
        $loansQuery = $loanRepository->findAuctions($filter);

        $paged = intval($request->query->get('paged', 1));
        $loans = $paginator->paginate(
            $loansQuery,
            $paged /* page number */,
            10,
            array('distinct' => false)
        );


        foreach($loans->getItems() AS $loan){
            if($loan->isStatus($loan::STATUS_AUCTION)){
                $seconds = $this->getDiffSeconds($loan->getAuctionEndAt());
                if($seconds < 0){
                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_OVERDUE, new LoanEvent($loan));
                }
            }
        }

        $user = $this->getUser();
        $watchlists = $watchdogs = null;
        if($user && $user->isType(User::TYPE_INVESTOR)){
            $watchlists = $em->getRepository('AppBundle:Watchlist')->findByLoansUser($loans->getItems(), $user);
            $watchdogs = $em->getRepository('AppBundle:Watchdog')->findByLoansUser($loans->getItems(), $user);
        }

        return array(
            "loans" => $loans,
            "filter" => $filter,
            "watchlists" => $watchlists,
            "watchdogs" => $watchdogs,
        );
    }


    /**
     * @Template()
     */
    public function investmentsAction(Request $request, $loanId)
    {
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');

        /** @var InvestmentRepository $investmentsRepository */
        $investmentsRepository = $em->getRepository('AppBundle:Investment');
        $loan =  $em->getRepository('AppBundle:Loan')->find($loanId);
        $loansQuery = $investmentsRepository->findInvestmentsByLoanIdQuery($loanId);

        $paged = intval($request->query->get('paged', 1));
        $investments = $paginator->paginate(
            $loansQuery,
            $paged /* page number */,
            10,
            array('distinct' => false)
        );


        return array(
            "investments" => $investments,
            "loan" => $loan
        );
    }


    /**
     * @Template()
     */
    public function investmentAddAction(Request $request, $loanId)
    {

        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var Loan $loan */
        $loan = $em->getRepository('AppBundle:Loan')->find($loanId);
        if (!$loan && $loan->isStatus($loan::STATUS_AUCTION)) {
            throw new NotFoundHttpException;
        }

        if (!$user) {
            return;
        }

        /** @var InvestmentRepository $investmentsRepository */
        $investmentsRepository = $em->getRepository('AppBundle:Investment');
        $investment = $investmentsRepository->findInvestmentsByLoanIdUserId($loanId, $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $return = array(
            "investment" => $investment,
            "personalIdSame" => false,
        );

        if($loan->getProfile()->getPersonalId() == $user->getProfile()->getPersonalId()){
            $return['personalIdSame'] = true;
        }


        if (!$investment) {
            $investment = new Investment();
            $investment->setUser($user);
            $investment->setLoan($loan);
            $form = $this->createForm("form_investment_add", $investment, array(
                "action" => $request->getRequestUri()
            ));

            if ($request->isMethod('POST') && !$return['personalIdSame']) {
                $form->submit($request);

                if ($form->isValid() && $investment->getLoan()->isStatus(Loan::STATUS_AUCTION)) {

                    $em->persist($investment);
                    $em->flush();

                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_INVESTMENT_ADD, new InvestmentEvent($investment));

                    $return["investment"] = $investment;
                }
            }

            $return["form"] = $form->createView();
        }

        $return["active"] = $loan->isStatus(Loan::STATUS_AUCTION) ? "active" : "inactive";

        return $return;
    }

    /**
     * @Template()
     */
    public function commentsAction(Request $request, $loanId)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository("AppBundle:Comment")->findCommentsByLoanIdQuery($loanId)->getQuery()->getResult();

        /** @var User $user */
        $user = $this->getUser();

        $loan = $em->getRepository("AppBundle:Loan")->findOneById($loanId);

        $answerId = $request->query->get("answer");
        $return = array(
            "comments" => $comments,
            "questionForm" => null,
            "answerForm" => null,
            "answerId" => $answerId,
            "loan" => $loan,
            "isAjax" => $request->isXmlHttpRequest()
        );

        if ($user) {

            if ($user->isType($user::TYPE_INVESTOR)) {
                $comment = new Comment;
                $questionForm = $this->createForm("form_question_add", $comment, array(
                    "action" => $request->getRequestUri()
                ));

                $questionForm->handleRequest($request);
                if ($questionForm->isValid()) {
                    $comment->setLoan($loan);
                    $comment->setUser($user);
                    $em->persist($comment);
                    $em->flush();

                    $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_QUESTION_ADD, new QuestionEvent($comment));
                    $return['comment'] = $comment;
                } else {
                    $return['questionForm'] = $questionForm->createView();
                    $return['withWrapper'] = $request->query->has("question");
                }


            }
            if ($user->isType($user::TYPE_CUSTOMER)) {

                if ($loan && $answerId) {
                    if ($loan->getUser() == $user) {
                        $comment = new Comment;
                        $answerForm = $this->createForm("form_answer_add", $comment, array(
                            "action" => $request->getRequestUri()
                        ));

                        $answerForm->handleRequest($request);
                        if ($answerForm->isValid()) {

                            $question = $em->getRepository("AppBundle:Comment")->findOneBy(array(
                                "id" => $answerId,
                                "loan" => $loanId,
                            ));


                            if ($question && !$question->getChild()) {
                                $comment->setUser($user);
                                $comment->setLoan($loan);
                                $comment->setParent($question);
                                $question->setChild($comment);
                                $em->persist($comment);
                                $em->flush();

                                $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_ANSWER_ADD, new AnswerEvent($comment));
                            }

                            $return['comment'] = $comment;
                        } else {
                            $return['answerForm'] = $answerForm->createView();
                        }

                    }
                }
            }
        }

        return $return;
    }


    function getDiffSeconds($date){
        $dateSeconds = $date->getTimestamp();
        $now = new \DateTime();
        $nowSeconds = $now->getTimestamp();

        return $dateSeconds - $nowSeconds;
    }
}
