<?php

namespace Hg\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Form\Form;

/**
 *
 */
class PageController extends Controller
{

    /**
     * @Route(
     *      "/info/{slug}.{_format}",
     *      name="hg_app_page",
     *      requirements={"_format"="html"},
     *      defaults={"_format"="html"}
     * )
     * @Template
     */
    public function indexAction(Request $request, $slug)
    {
        $em = $this->getDoctrine();


        $page = $em->getRepository('AppBundle:Page')->findOneBy(array(
            'active' => 1,
            'slug' => $slug
        ));

        if(!$page){
            throw new NotFoundHttpException;
        }

        $menuInvestment = $em->getRepository('AppBundle:Page')->findBy(array(
            'active' => true,
            'inMenu' => 1
        ), array("ordered" => "ASC"));

        $menuBorrow = $em->getRepository('AppBundle:Page')->findBy(array(
            'active' => true,
            'inMenu' => 2
        ), array("ordered" => "ASC"));

        $menuHowItWorks = $em->getRepository('AppBundle:Page')->findBy(array(
            'active' => true,
            'inMenu' => 3
        ), array("ordered" => "ASC"));


        return array(
            'page' => $page,
            'menuInvestment' => $menuInvestment,
            'menuBorrow' => $menuBorrow,
            'menuHowItWorks' => $menuHowItWorks
        );
    }

}
