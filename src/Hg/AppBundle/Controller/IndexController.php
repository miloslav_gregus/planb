<?php

namespace Hg\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

class IndexController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $lifebuoy = $em->getRepository("AppBundle:Lifebuoy")->findLast();
        return array("lifebuoy" => $lifebuoy);
    }

    /**
     * @Route("/test")
     * @Template()
     */
    public function testAction()
    {
//        $em = $this->getDoctrine()->getManager();
//        $user = $em->getRepository("AppBundle:User")->find(16);
//
//
//        $this->get("event_dispatcher")->dispatch(HgEvents::USER_REGISTRATION, new UserEvent($user));

//        echo "fsd";
//        $this->getDDA();
//        echo "2";

//        $command = $this->get('MyCommandService');
//
//        $input = new ArgvInput(array('arg1'=> 'value'));
//        $output = new ConsoleOutput();
//
//        $command->run($input, $output);


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:User")->findOneByUsername("Ranmacar");

        $documentsGenerator = $this->get("hg.services.planb.documents_generator");
        $file = $documentsGenerator->generate("framework-contract-demo", array(
            "user" => $user
        ));

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="ramcova-zmluva-529b8876c19ce.pdf"');

        echo $file;
        exit;

        return array();
    }

    /**
     * @Route("/recalculate")
     * @Template()
     */
    public function recalculateAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository("AppBundle:User")->findAll();

        $credit = $this->get("hg.services.planb.credit");

        foreach($users AS $user) {
            $credit->recalculateUser($user);
        }

        echo "ok";
        exit;

    }
}
