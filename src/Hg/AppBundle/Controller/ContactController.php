<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hg\AppBundle\Controller;

use Hg\AppBundle\Entity\Contact;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Event\ContactEvent;
use Hg\AppBundle\Form\Contact\ContactType;
use Hg\AppBundle\HgEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Controller managing the user profile
 *
 */
class ContactController extends Controller
{

    /**
     * @Route("/kontakt", name="hg_app_contact")
     * @Template
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $contact = new Contact();
        $form = $this->createForm("form_contact", $contact, array("validation_groups" => $user ? "contact_logged" : "contact_visitor"));


        if ($request->isMethod('POST')) {
            $form->submit($request);

            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                if($user){
                    $contact->setEmail($user->getEmail());
                    $contact->setUsername($user->getUsername());
                    $contact->setUser($user);
                }
                $em->persist($contact);
                $em->flush();

                $planBemail = $this->container->getParameter("meta.email");


                $subject = $this->get("translator")->trans("contact.subject_text", array(
                    "%id%" => $contact->getId(),
                    "%subject%" => $contact->getSubject()
                ));
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($planBemail)
                    ->setReplyTo($contact->getEmail())
                    ->setTo($planBemail);

                $text = "";
                if($user){
                    $text .= "Používateľ: {$contact->getUsername()} (ID: {$user->getFormattedId()})" . \PHP_EOL;
                }else{
                    $text .= "Meno: {$contact->getUsername()}" . \PHP_EOL;
                }
                $text .= "Email: {$contact->getEmail()}" . \PHP_EOL . \PHP_EOL;
                $text .= $contact->getText();

                $message->setBody($text, 'text/plain');
                $this->get("mailer")->send($message);

                $this->get("event_dispatcher")->dispatch(HgEvents::CONTACT, new ContactEvent($contact));

                $request->getSession()->getFlashBag()->add('success', 'contact.successfully_send');

                return $this->redirect($this->generateUrl("hg_app_contact"));
            }
        }

        return array(
            'form' => $form->createView(),
            "sidebarMenuPoint" => 303,
        );
    }


}
