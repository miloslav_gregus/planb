<?php

namespace Hg\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Investment;
use Hg\AppBundle\Entity\Loan;
use Hg\AppBundle\Entity\Transaction;
use Hg\AppBundle\Entity\User;
use Hg\AppBundle\Entity\Watchdog;
use Hg\AppBundle\Entity\Watchlist;
use Hg\AppBundle\Event\InvestmentEvent;
use Hg\AppBundle\HgEvents;
use Hg\Services\Planb\FinancialFunctions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/ajax", options={"expose"="true"})
 *
 */
class AjaxController extends Controller
{

    /**
     * @Route("/investment-delete/{investmentId}.{_format}", name="hg_app_ajax_investment_delete", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function deleteInvestmentAction(Request $request, $investmentId)
    {

        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        $em = $this->getDoctrine()->getManager();

        /** @var Investment $investment */
        $investment = $em->getRepository('AppBundle:Investment')->findOneBy(array(
            "id" => $investmentId,
            "status" => array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO)
        ));

        if ($investment && $investment->getLoan()->getUser() == $this->getUser()) {
            $investment->setStatus($investment::STATUS_DELETED);
            $em->flush();

            $this->get("event_dispatcher")->dispatch(HgEvents::AUCTION_INVESTMENT_DELETE, new InvestmentEvent($investment));

            $twig = $this->get("twig");
            $template = $twig->loadTemplate("AppBundle:Auction:table_detail.html.twig");
            $data["template"] = $template->render(array(
                "loan" => $investment->getLoan(),
            ));

            $data["status"] = 1;
        }
        return array(
            'data' => $data,
        );
    }


    /**
     * @Route("/investment-limbo/{investmentId}.{_format}", name="hg_app_ajax_investment_limbo", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function limboInvestmentAction(Request $request, $investmentId)
    {

        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        $em = $this->getDoctrine()->getManager();

        /** @var Investment $investment */
        $investment = $em->getRepository('AppBundle:Investment')->findOneBy(array(
            "id" => $investmentId,
            "status" => array(Investment::STATUS_INSERTED, Investment::STATUS_LIMBO, Investment::STATUS_PREPARED)
        ));

        if ($investment && $investment->getLoan()->getUser() == $this->getUser()) {

            if ($investment->isStatus(array($investment::STATUS_INSERTED, $investment::STATUS_PREPARED))) {
                $investment->setStatus($investment::STATUS_LIMBO);
            } else {
                $investment->setStatus($investment::STATUS_INSERTED);
            }
            $em->flush();

            $calculations = $this->get("hg.services.planb.calculations")->recalculateSummary($investment->getLoan());

            $twig = $this->get("twig");
            $template = $twig->loadTemplate("UserBundle:includes:summary-stop.html.twig");
            $data["template"] = $template->render(array(
                "investment" => $investment,
                "calculations" => $calculations,
                "loan" => $investment->getLoan(),
                "active" => "active",
                "user" => $this->getUser()
            ));


            $template = $twig->loadTemplate("UserBundle:includes:investments-summary-tfoot.html.twig");
            $data['tfoot'] = $template->render(array(
                "calculations" => $calculations,
            ));

            $data["calculations"] = $calculations;
            $data["status"] = 1;
        }
        return array(
            'data' => $data,
        );
    }


    /**
     * @Route("/repayment-calendar/{loanId}", name="hg_app_ajax_repayment_calendar")
     * @Method({"POST"})
     * @Template
     */
    public function repaymentCalendarAction(Request $request, $loanId)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException;
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Loan $loan */
        $loan = $em->getRepository('AppBundle:Loan')->find($loanId);

        if (!$loan || $loan->getUser() != $this->getUser()) {
            throw new NotFoundHttpException;
        }

        $loanStartAt = new \DateTime($request->request->get('form_auction_stop[loanStartAt]', date("Y-m-d", strtotime("+7 days")), true));
        $repaymentDay = $request->request->get('form_auction_stop[repaymentDay]', 20, true);

        $calculation = $this->get("hg.services.planb.calculations");
        $loan->setLoanStartAt($loanStartAt);
        $loan->setRepaymentDay($repaymentDay);
        return $calculation->recalculateLoanTransactionsSummary($loan);
    }

    /**
     * @Route("/watchlist-add/{loanId}.{_format}", name="hg_app_ajax_watchlist", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function watchlistAddAction(Request $request, $loanId)
    {
        if (!$request->isXmlHttpRequest() || !($user = $this->getUser())) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $watchlist = $em->getRepository('AppBundle:Watchlist')->findOneBy(array(
            "loan" => $loanId,
            "user" => $user
        ));


        if (!$watchlist) {
            $loan = $em->getRepository("AppBundle:Loan")->find($loanId);

            if (!$loan) {
                throw new NotFoundHttpException;
            }

            $watchlist = new Watchlist();
            $em->persist($watchlist);
            $watchlist->setUser($user)
                ->setLoan($loan);
        }else{
            if($watchlist->getStatus() == $watchlist::STATUS_ACTIVE){
                $watchlist->setStatus($watchlist::STATUS_DELETED);
                if($watchlist->isUpdated()){
                    $watchlist->setUpdated(false);
                    $watchlist->getUser()->addWatchlistCount(-1);
                }
            }else{
                $watchlist->setStatus($watchlist::STATUS_ACTIVE);
            }
        }
        $em->flush();
        $data["status"] = 1;

        return array(
            'data' => $data,
        );
    }

    /**
     * @Route("/watchdog-remove/{watchdogId}.{_format}", name="hg_app_ajax_watchdog_remove", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function watchdogRemoveAction(Request $request, $watchdogId)
    {
        if (!$request->isXmlHttpRequest() || !($user = $this->getUser())) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $watchdog = $em->getRepository('AppBundle:Watchdog')->findOneBy(array(
            "id" => $watchdogId,
            "user" => $this->getUser()
        ));


        if ($watchdog) {
            $watchdog->setStatus(Watchdog::STATUS_DELETED);
            $em->flush();
            $data["status"] = 1;
        }

        return array(
            'data' => $data,
        );
    }

    /**
     * @Route("/watchdog-filter/{watchdogId}.{_format}", name="hg_app_ajax_watchdog_filter", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function watchdogFilterAction(Request $request, $watchdogId)
    {
        if (!$request->isXmlHttpRequest() || !($user = $this->getUser())) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Watchdog $watchdog */
        $watchdog = $em->getRepository('AppBundle:Watchdog')->findOneBy(array(
            "id" => $watchdogId,
            "user" => $this->getUser()
        ));

        if ($watchdog) {
            $data['filter'] = array(
              "rating" => $watchdog->getAuctionRating(),
              "maturity" => $watchdog->getLoanMaturity(),
              "amountMin" => $watchdog->getAmountMin(),
              "amountMax" => $watchdog->getAmountMax(),
              "daysEndMin" => $watchdog->getDaysEndMin(),
              "daysEndMax" => $watchdog->getDaysEndMax(),
              "interestAverageMin" => $watchdog->getInterestAverageMin() * 100,
              "interestAverageMax" => $watchdog->getInterestAverageMax() * 100,
              "collectedPercentageMin" => $watchdog->getCollectedPercentageMin() * 100,
              "collectedPercentageMax" => $watchdog->getCollectedPercentageMax() * 100,
            );
            $data["status"] = 1;
        }

        return array(
            'data' => $data,
        );
    }

    /**
     * @Route("/notifications-watchlist.{_format}", name="hg_app_ajax_notifications_watchlist", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function notificationsWatchlistAction(Request $request)
    {
        if (!$request->isXmlHttpRequest() || !($user = $this->getUser())) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user->setNotificationsWatchlist(!$user->getNotificationsWatchlist());
        $em->flush();

        $data["status"] = 1;

        return array(
            'data' => $data,
        );
    }

    /**
     * @Route("/notifications-watchdog.{_format}", name="hg_app_ajax_notifications_watchdog", requirements={"_format"="json"})
     * @Method({"POST"})
     * @Template
     */
    public function notificationsWatchdogAction(Request $request)
    {
        if (!$request->isXmlHttpRequest() || !($user = $this->getUser())) {
            throw new NotFoundHttpException;
        }

        $data = array("status" => 0);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user->setNotificationsWatchdog(!$user->getNotificationsWatchdog());
        $em->flush();

        $data["status"] = 1;

        return array(
            'data' => $data,
        );
    }

}
