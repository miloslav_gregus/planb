<?php

namespace Hg\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\LoanFile;
use Hg\AppBundle\Entity\ProfileFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UploadController extends Controller
{
    /**
     * @Route("/uploads/loans/{id}", name="hg_app_uploads_loan")
     */
    public function loansAction($id)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var LoanFile $loanFile */
        $loanFile = $em->getRepository("AppBundle:LoanFile")->find($id);
        if (!$loanFile) throw new NotFoundHttpException;

        $user = $this->getUser();


        if ($loanFile->getLoan()->getUser() != $user && $user->getId() != 1)
            throw new AccessDeniedException('This user does not have access to this section.');


        $response = new Response($loanFile->getFileContent());

        $file = new File($loanFile->getAbsolutePath());
        $mime = $file->getMimeType();

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', $mime);
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $loanFile->getNiceFilename());
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        return $response;
    }

    /**
     * @Route("/uploads/profiles/{id}", name="hg_app_uploads_profile")
     */
    public function profileAction($id)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var ProfileFile $profileFile */
        $profileFile = $em->getRepository("AppBundle:ProfileFile")->find($id);
        if (!$profileFile) throw new NotFoundHttpException;

        $user = $this->getUser();


        if ($profileFile->getProfile()->getUser() != $user && $user->getId() != 1)
            throw new AccessDeniedException('This user does not have access to this section.');


        $response = new Response($profileFile->getFileContent());

        $file = new File($profileFile->getAbsolutePath());
        $mime = $file->getMimeType();

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', $mime);
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $profileFile->getNiceFilename());
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        return $response;
    }


}
