<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 9.11.2013
 * Time: 0:09
 */

namespace Hg\AppBundle;


class HgEvents {
    const USER_REGISTRATION = 'hg.user.registration';
    const USER_PASSWORD_RESET = 'hg.user.password';
    const AUCTION_ANSWER_ADD = 'hg.auction.answer_add';
    const AUCTION_QUESTION_ADD = 'hg.auction.question_add';
    const AUCTION_INVESTMENT_ADD = 'hg.auction.investment_add';
    const AUCTION_INVESTMENT_DELETE = 'hg.auction.investment_delete';
    const AUCTION_INVESTMENT_CANCEL = 'hg.auction.investment_cancel';
    const AUCTION_INVESTMENT_PREPARED = 'hg.auction.investment_prepared';

    const TRANSACTION_ADD = 'hg.transaction.deposit_add';
    const TRANSACTION_ACCEPTED = 'hg.transaction.accepted';
    const TRANSACTION_REJECTED = 'hg.transaction.rejected';
    const TRANSACTION_REPAYMENT_PENDING = 'hg.transaction.repayment.pending';
    const TRANSACTION_REPAYMENT_ADMIN_NOTIFY = 'hg.transaction.repayment.admin_notify';
    const TRANSACTION_REPAYMENT_OVERDUE = 'hg.transaction.repayment.overdue';
    const TRANSACTION_REPAYMENT_SUCCESS = 'hg.transaction.repayment.success';

    const AUCTION_DRAFT_REQUESTED = 'hg.auction.draft_requested';
    const AUCTION_DRAFT_APPROVED = 'hg.auction.draft_approved';
    const AUCTION_DRAFT_CANCELED = 'hg.auction.draft_canceled';


    const AUCTION_AUCTION_REQUESTED = 'hg.auction.auction_requested';
    const AUCTION_AUCTION_SCORING = 'hg.auction.auction_scoring';
    const AUCTION_AUCTION_CANCELED = 'hg.auction.auction_canceled';
    const AUCTION_AUCTION_APPROVED = 'hg.auction.auction_approved';
    const AUCTION_AUCTION_LAUNCH = 'hg.auction.auction_launch';


    const AUCTION_ACCEPTED = 'hg.auction.accepted';
    const AUCTION_CANCELED = 'hg.auction.canceled';
    const AUCTION_OVERDUE = 'hg.auction.overdue';
    const AUCTION_PAYABLE = 'hg.auction.payable';
    const AUCTION_LOAN = 'hg.auction.loan';



    const FILE_MISSING = 'hg.customer.file.missing';
    const FILE_PENDING = 'hg.customer.file.pending';
    const FILE_APPROVE = 'hg.customer.file.approve';
    const FILE_REJECT = 'hg.customer.file.reject';
    const FILE_DELIVERED = 'hg.customer.file.delivered';

    const INVESTOR_APPROVED = 'hg.investor.approved';
    const INVESTOR_REGISTRATION = 'hg.investor.registration';

    const CONTACT = 'hg.contact';

    const GRID_CARD_GENERATED = 'hg.grid_card';

}