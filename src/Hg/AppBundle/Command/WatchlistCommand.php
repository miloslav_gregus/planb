<?php

namespace Hg\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Watchlist;
use Hg\Services\Planb\EmailHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;

class WatchlistCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('notifications:watchlist')
            ->setDescription('Email notifications for watchlist');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $text = "Watchlist notifies successfully";

        $container = $this->getContainer();


        $container->enterScope('request');
        $container->set('request', new Request(), 'request');


        /** @var EntityManager $em */
        $em = $container->get("doctrine")->getManager();

        /** @var EmailHandler $emailHandler */
        $emailHandler = $container->get("hg.services.planb.email_handler");

        $watchlists = $em->getRepository("AppBundle:Watchlist")->findBy(array(
            "updated" => true,
            "status" => Watchlist::STATUS_ACTIVE
        ));

        $users = array();

        foreach ($watchlists AS $watchlist) {
            $user = $watchlist->getUser();

            if (!$user->getNotificationsWatchlist()) continue;

            if (!isset($users[$user->getId()])) $users[$user->getId()] = array();
            $users[$user->getId()][] = $watchlist;
        }


        foreach ($users AS $watchlists) {
            $user = $watchlists[0]->getUser();
            $loans = array();
            foreach($watchlists AS $watchlist){
                $loans[] = $watchlist->getLoan();
            }

            $data = array(
                "loans" => $loans,
                "user" => $user,
            );


            $emailHandler->send("notification_watchlist", $user, $data);
        }


        $transport = $this->getContainer()->get('mailer')->getTransport();
        $spool = $transport->getSpool();
        if (!$spool instanceof \Swift_MemorySpool) {
            return;
        }
        $spool->flushQueue($this->getContainer()->get('swiftmailer.transport.real'));
        $output->writeln($text);
    }
}