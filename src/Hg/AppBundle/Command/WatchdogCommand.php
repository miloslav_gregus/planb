<?php

namespace Hg\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Watchlist;
use Hg\Services\Planb\EmailHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;

class WatchdogCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('notifications:watchdog')
            ->setDescription('Email notifications for watchlist');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $text = "Watchdog notifies successfully";

        $container = $this->getContainer();


        $container->enterScope('request');
        $container->set('request', new Request(), 'request');


        /** @var EntityManager $em */
        $em = $container->get("doctrine")->getManager();

        /** @var EmailHandler $emailHandler */
        $emailHandler = $container->get("hg.services.planb.email_handler");

        $loans = $em->getRepository("AppBundle:Loan")->findYesterday();

        $users = array();

        $watchdogs = $em->getRepository("AppBundle:Watchdog")->findByLoansUser($loans, null, false);

        foreach($watchdogs AS $watchdog){
            if(!count($watchdog)) continue;

            $user = $watchdog[0]->getUser();
            if(isset($users[$user->getId()])) continue;

            $w = $em->getRepository("AppBundle:Watchdog")->findByLoansUser($loans, $user, false);
            $loanIds = array_keys($w);

            $users[$user->getId()] = $loansFiltered = $em->getRepository("AppBundle:Loan")->findBy(array("id" => $loanIds));

            $data = array(
                "loans" => $loansFiltered,
                "user" => $user,
            );

            $emailHandler->send("notification_watchdog", $user, $data);
        }

        $transport = $this->getContainer()->get('mailer')->getTransport();
        $spool = $transport->getSpool();
        if (!$spool instanceof \Swift_MemorySpool) {
            return;
        }
        $spool->flushQueue($this->getContainer()->get('swiftmailer.transport.real'));
        $output->writeln($text);
    }
}