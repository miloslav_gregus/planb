<?php

namespace Hg\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\Transaction;
use Hg\Services\Planb\EmailHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Hg\AppBundle\Event\TransactionEvent;
use Hg\AppBundle\HgEvents;

class RepaymentsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('notifications:repayments')
            ->setDescription('Repayment recalculations and notifications');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $text = "Repayment transactions recalculated successfully";

        $container = $this->getContainer();

        $container->enterScope('request');
        $container->set('request', new Request(), 'request');

        /** @var EntityManager $em */
        $em = $container->get("doctrine")->getManager();

        /** @var EmailHandler $emailHandler */
        $emailHandler = $container->get("hg.services.planb.email_handler");


        // update users repayments from future to pending
        $transactions = $em->getRepository("AppBundle:Transaction")->findRepayments(null, new \Datetime("+7 days"), Transaction::STATUS_FUTURE);
        foreach($transactions AS $transaction){
            $transaction->setStatus(Transaction::STATUS_PENDING);
            $em->flush();
            $this->getContainer()->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_REPAYMENT_PENDING, new TransactionEvent($transaction));
        }

        // notification for admin in day of repaymen
        $transactions = $em->getRepository("AppBundle:Transaction")->findRepayments(new \Datetime(), new \Datetime(), array(
            Transaction::STATUS_FUTURE,
            Transaction::STATUS_PENDING
        ));
        foreach($transactions AS $transaction){
            $this->getContainer()->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_REPAYMENT_ADMIN_NOTIFY, new TransactionEvent($transaction));
        }


        // notification for admin in day of repaymen
        $transactions = $em->getRepository("AppBundle:Transaction")->findRepayments(null, new \Datetime("-1 day"), array(
            Transaction::STATUS_FUTURE,
            Transaction::STATUS_PENDING
        ));
        foreach($transactions AS $transaction){
            $transaction->setStatus(Transaction::STATUS_OVERDUE);
            $dependentTransactions = $em->getRepository("AppBundle:Transaction")
                ->findDependentRepayments($transaction->getVariableNumber(), $transaction->getSpecificNumber());
            foreach($dependentTransactions AS $dependentTransaction){
                $dependentTransaction->setStatus(Transaction::STATUS_OVERDUE);
            }
            $em->flush();

            $this->getContainer()->get("event_dispatcher")->dispatch(HgEvents::TRANSACTION_REPAYMENT_OVERDUE, new TransactionEvent($transaction));
        }



        $transport = $this->getContainer()->get('mailer')->getTransport();
        $spool = $transport->getSpool();
        if (!$spool instanceof \Swift_MemorySpool) {
            return;
        }
        $spool->flushQueue($this->getContainer()->get('swiftmailer.transport.real'));
        $output->writeln($text);
    }
}