jQuery(function ($) {
    $('.loan-calculator').each(function () {
        var $wrapper = $(this)

        function recalculate() {
            try {
                var amount = $wrapper.find('.loan-height .slider').slider("value")
                var months = $wrapper.find('.loan-length .slider').slider("value")
                var interest = $wrapper.find('.loan-interest .slider').slider("value")
            } catch (err) {
                return
            }


            var monthlyInterest = calculations.monthlyInterest(interest)
            var monthlyRepayment = calculations.pmt(monthlyInterest, months, amount)

            $wrapper.find(".circle .value").text(round2dec(monthlyRepayment))
        }

        $wrapper.find('.loan-height .slider').each(function () {
            var $slider = $(this)
            var value = $slider.data('value')
            var min = $slider.data('min')
            var max = $slider.data('max')

            var func = function () {
                var $this = $(this)
                setTimeout(function () {
                    var value = $this.slider('value')
                    $slider.closest('.loan-height').find('.value').text(value)
                    recalculate()
                }, 100)
            }

            $slider.slider({
                range: "min",
                min: min,
                max: max,
                value: value,
                step: 100,
                animate: true,
                slide: func,
                create: func,
                stop: func
            })
        })
        $wrapper.find('.loan-length .slider').each(function () {
            var $slider = $(this)
            var value = $slider.data('value')
            var min = $slider.data('min')
            var max = $slider.data('max')

            var func = function (e, ui) {
                var $this = $(this)
                setTimeout(function () {
                    var value = $this.slider('value')
                    $slider.closest('.loan-length').find('.value').text(value)
                    recalculate()
                }, 100)
            }

            $slider.slider({
                range: "min",
                min: min,
                max: max,
                value: value,
                animate: true,
                step: 12,
                slide: func,
                create: func,
                stop: func
            })
        })
        $wrapper.find('.loan-interest .slider').each(function () {
            var $slider = $(this)
            var value = $slider.data('value')
            var min = $slider.data('min')
            var max = $slider.data('max')

            var func = function (e, ui) {
                var $this = $(this)
                setTimeout(function () {
                    var value = $this.slider('value')
                    $slider.closest('.loan-interest').find('.value').text(value)
                    recalculate()
                }, 100)
            }

            $slider.slider({
                range: "min",
                min: min,
                max: max,
                value: value,
                animate: true,
                slide: func,
                create: func,
                stop: func
            })
        })
    })

    $('.investment-calculator').each(function () {
        var $wrapper = $(this)

        function recalculate() {
            try {
                var amount = $wrapper.find('.loan-height .slider').slider("value")
                var months = $wrapper.find('.loan-length .slider').slider("value")
                var interest = $wrapper.find('.loan-interest .slider').slider("value")
            } catch (err) {
                return
            }

            var monthlyInterest = calculations.monthlyInterest(interest)
            var monthlyRepayment = calculations.pmt(monthlyInterest, months, amount)
            var repayment = monthlyRepayment * months;
            var profit = repayment - amount;

            $wrapper.find(".circle .value").text(round2dec(profit))
        }

        $wrapper.find('.loan-height .slider').each(function () {
            var $slider = $(this)
            var value = $slider.data('value')
            var min = $slider.data('min')
            var max = $slider.data('max')

            var func = function () {
                var $this = $(this)
                setTimeout(function () {
                    var value = $this.slider('value')
                    $slider.closest('.loan-height').find('.value').text(value)
                    recalculate()
                }, 100)
            }

            $slider.slider({
                range: "min",
                min: min,
                max: max,
                value: value,
                step: 25,
                animate: true,
                slide: func,
                create: func,
                stop: func
            })
        })
        $wrapper.find('.loan-length .slider').each(function () {
            var $slider = $(this)
            var value = $slider.data('value')
            var min = $slider.data('min')
            var max = $slider.data('max')

            var func = function (e, ui) {
                var $this = $(this)
                setTimeout(function () {
                    var value = $this.slider('value')
                    $slider.closest('.loan-length').find('.value').text(value)
                    recalculate()
                }, 100)
            }

            $slider.slider({
                range: "min",
                min: min,
                max: max,
                value: value,
                step: 12,
                animate: true,
                slide: func,
                create: func,
                stop: func
            })
        })
        $wrapper.find('.loan-interest .slider').each(function () {
            var $slider = $(this)
            var value = $slider.data('value')
            var min = $slider.data('min')
            var max = $slider.data('max')

            var func = function (e, ui) {
                var $this = $(this)
                var $this = $(this)
                setTimeout(function () {
                    var value = $this.slider('value')
                    $slider.closest('.loan-interest').find('.value').text(value)
                    recalculate()
                }, 100)
            }

            $slider.slider({
                range: "min",
                min: min,
                max: max,
                value: value,
                animate: true,
                slide: func,
                create: func,
                stop: func
            })
        })
    })

})

function round2dec(num) {
    num = Math.round(num * 100) / 100
    num = parseFloat(num).toFixed(2);
    return num.replace(".", ",")
}


var calculations = {
    "pmt": function PMT(i, n, p) {
        return Math.abs(i * p * Math.pow((1 + i), n) / (1 - Math.pow((1 + i), n)));
    },
    "monthlyInterest": function monthlyInterest(interest) {
        interest = interest / 100;
        interest = interest < 0.0000001 ? 0.0000001 : interest
        return (Math.pow(1 + interest, 1 / 12) - 1)
    }
}




