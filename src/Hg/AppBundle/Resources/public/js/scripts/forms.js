jQuery(function ($) {

    $("select:not(.standard)").select2()

    $('select').bind('change', function () {
        $(this).trigger('blur');
    });


    if($("#form-loan-validate").size()){
        var $loanMaturity = $("#hg_loan_validate_step1_loanMaturity")
        var loanMaturityValues = $loanMaturity.html()

        var amountChange = function(){
            $loanMaturity.html(loanMaturityValues);

            setTimeout(function(){
               var val = parseInt($amount.val())

               if(val <= 3000) {
                   $loanMaturity.find("option[value=48],option[value=60]").remove()
               }
               $loanMaturity.trigger("change")
            }, 20)
        }

        var $amount = $("#hg_loan_validate_step1_auctionRequestedAmount")
        $amount.spinner({
            step: 100,
            min: $amount.data("min"),
            max: $amount.data("max"),
            spin: amountChange
        }).change(amountChange)




        var $addressIsSame = $("#hg_loan_validate_step3_profile_addressContactNotSame")
        $addressExtra = $(".contact-address-not-same")
        if($addressIsSame.is(":checked")){
            $addressExtra.show()
        }else{
            $addressExtra.hide()
        }
        $addressIsSame.change(function(){
            if($addressIsSame.is(":checked")){
                $addressExtra.slideDown()
                $addressExtra.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $addressExtra.slideUp()
                $addressExtra.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")


        var $addressCountry = $("#hg_loan_validate_step3_profile_addressCountry")
        $addressCountry.change(function(){
            var $controlGroup = $("#hg_loan_validate_step3_profile_addressRegion").closest(".form-group")
            if($addressCountry.val() == "SK"){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")


        var $addressContactCountry = $("#hg_loan_validate_step3_profile_addressContactCountry")
        $addressContactCountry.change(function(){
            var $controlGroup = $("#hg_loan_validate_step3_profile_addressContactRegion").closest(".form-group")
            if($addressContactCountry.val() == "SK"){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")

        $addressIsSame.trigger("change")



        var $incomeEmploymentType1 = $("#hg_loan_validate_step4_incomeType1_incomeEmploymentType")
        $incomeEmploymentType1.change(function(){
            var $controlGroup = $("#hg_loan_validate_step4_incomeType1_incomeEndAt").closest(".form-group")
            if($incomeEmploymentType1.val() == 2){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")

        var $incomeEmploymentType3 = $("#hg_loan_validate_step4_incomeType3_incomeEmploymentType")
        $incomeEmploymentType3.change(function(){
            var $controlGroup = $("#hg_loan_validate_step4_incomeType3_incomeOther").closest(".form-group")
            if($incomeEmploymentType3.val() == 13){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")

        var $incomeEmploymentType4 = $("#hg_loan_validate_step4_incomeType4_incomeEmploymentType")
        $incomeEmploymentType4.change(function(){
            var $controlGroup = $("#hg_loan_validate_step4_incomeType4_incomeOther").closest(".form-group")
            if($incomeEmploymentType4.val() == 17){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")



        var $incomeType = $("#hg_loan_validate_step4_incomeType")
        $incomeType.change(function(){
            for(var i = 1; i < 11; i++){
                var $group = $("[id^=hg_loan_validate_step4_incomeType" + i + "]:first").closest(".form-group").parent().closest(".form-group")
                $group.find(":required").data("required", true).removeAttr("required")
                $group.hide()
            }
            var id = $incomeType.val()

            var $group = $("[id^=hg_loan_validate_step4_incomeType" + id + "]:first").closest(".form-group").parent().closest(".form-group")
            $group.show()

            $group.find(".form-control:visible").each(function(){
                if($(this).data("required") == true) {
                    $(this).attr("required", "required")
                }
            })

        }).trigger("change")

    }


    if($("#form-auction-validate").size()) {

        var $noObligationsCheckbox = $("#hg_auction_validate_step2_additionalNoObligations")
        var $additionalsInfo = $(".form-additional-obligations-wrapper")

        if($additionalsInfo.find(".collection-items > *").size()){
            $noObligationsCheckbox.removeAttr("checked")
        }else{
            $noObligationsCheckbox.attr("checked", "checked")
        }

        if(!$noObligationsCheckbox.is(":checked")){
            $additionalsInfo.show()
        }else{
            $additionalsInfo.hide()
        }
        $noObligationsCheckbox.change(function(){
            if(!$noObligationsCheckbox.is(":checked")){
                $additionalsInfo.slideDown()
                $additionalsInfo.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $additionalsInfo.slideUp()
                $additionalsInfo.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")

        var $collection = $(".hg_auction_validate_step2_additionalObligations_form_group").closest(".form-group")
        $collection.on("add.mopa-collection-item", function(){
            var $inputs = $collection.find("[type='number']")
            $inputs.spinner({
                step: 10
            })
        })
        $collection.trigger("add.mopa-collection-item")


        var $incomes = $("#hg_auction_validate_step2_additionalIncomes")
        $incomes.spinner({
            step: 10,
            min: $incomes.data("min"),
            max: $incomes.data("max")
        })

        var $others = $("#hg_auction_validate_step2_additionalIncomesOthers")
        $others.spinner({
            step: 10,
            min: $others.data("min"),
            max: $others.data("max")
        })

        var $othersHousehold = $("#hg_auction_validate_step2_additionalOutcomesHousehold")
        $othersHousehold.spinner({
            step: 10,
            min: $othersHousehold.data("min"),
            max: $othersHousehold.data("max")
        })
        var $othersPartner = $("#hg_auction_validate_step2_additionalIncomesPartner")
        $othersPartner.spinner({
            step: 10,
            min: $othersPartner.data("min"),
            max: $othersPartner.data("max")
        })

        var $kids = $("#hg_auction_validate_step2_additionalKids")
        $kids.spinner({
            step: 1,
            min: $kids.data("min"),
            max: $kids.data("max")
        })

        $(".hg_auction_validate_step2_additionalObligations_form_group").on("add.mopa-collection-item", function(){
            $(this).find("select:not(.standard)").select2()
        })
        $(".hg_auction_validate_step2_additionalObligations_form_group").on("add.mopa-collection-item", function(){
            $(this).find("select:not(.standard)").select2()
        })

        var $form = $("#form-auction-validate")
        $form.submit(function(){
            if($noObligationsCheckbox.is(":checked")) {
                $(".form-additional-obligations-wrapper").remove()
            }
        })
    }






    if($("#form-profile-registration").size()){

        var $addressIsSame = $("#hg_profile_registration_step2_addressContactNotSame")
        var $addressExtra = $(".contact-address-not-same")
        if($addressIsSame.is(":checked")){
            $addressExtra.show()
        }else{
            $addressExtra.hide()
        }
        $addressIsSame.change(function(){
            if($addressIsSame.is(":checked")){
                $addressExtra.slideDown()
                $addressExtra.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $addressExtra.slideUp()
                $addressExtra.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")



        var $addressCountry = $("#hg_profile_registration_step2_addressCountry")
        $addressCountry.change(function(){
            var $controlGroup = $("#hg_profile_registration_step2_addressRegion").closest(".form-group")
            if($addressCountry.val() == "SK"){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")


        var $addressContactCountry = $("#hg_profile_registration_step2_addressContactCountry")
        $addressContactCountry.change(function(){
            var $controlGroup = $("#hg_profile_registration_step2_addressContactRegion").closest(".form-group")
            if($addressContactCountry.val() == "SK"){
                $controlGroup.slideDown()
                $controlGroup.find("input.form-control, select.form-control").attr("required", "required")
            }else{
                $controlGroup.slideUp()
                $controlGroup.find("input.form-control, select.form-control").removeAttr("required")
            }
        }).trigger("change")

        $addressIsSame.trigger("change")

    }

    if($("#form-deposit").size()){
        var $amount = $("#hg_deposit_from_amount")
        $amount.spinner({
            step: 5,
            min: $amount.data("min"),
            max: $amount.data("max")
        })
    }


    if($("#form-withdraw").size()){
        var $amount = $("#hg_withdraw_from_amount")
        $amount.spinner({
            step: 5,
            min: $amount.data("min"),
            max: $amount.data("max")
        })

        $("#hg_withdraw_from_validationCode").val("")
    }


})