function initAccessories(){

    $('.tooltip-popup').tooltip({
        trigger: 'hover focus click'
    })


    $('.collected').each(function(){

        var $el = $(this)
        var size = typeof $el.data('size') !== "undefined" ? $el.data('size') : 60
        var color = typeof $el.data('color') !== "undefined" ? $el.data('color') : '#a1c5f9'
        var width = typeof $el.data('width') !== "undefined" ? $el.data('width') : 12


        $el.easyPieChart({
            size: size,
            barColor: color,
            scaleColor: 0,
            trackColor: 0,
            lineWidth: width,
            lineCap: 'square'
            //animate: false
        });

        var circleDimension = size - width;
        $el.find('.circle')
            .css('width', circleDimension )
            .css('height', circleDimension )
            .css('top', width / 2 )
            .css('right', width / 2 )
            .css('line-height', circleDimension + 'px' )
    })

    $(".js-link").unbind().click(function(){
        var $class = $(this).data("link-element")
        var href = $(this).find($class).attr("href")
        window.location.href = href
    })


    $("#filter-result .actions a.watchlist").click(function(){

        var $this = $(this)
        var href = $this.attr("href")

        var opts = {
            lines: 9, // The number of lines to draw
            length: 8, // The length of each line
            width: 4, // The line thickness
            radius: 2, // The radius of the inner circle
            color: "#000"
        };
        $this.spin(opts)
        $.post(href, function (result) {
            $this.toggleClass("active")
            $this.spin(false)
        })
        return false;
    })

}

Globalize.addCultureInfo( "en", "en", {
    numberFormat: {
        ",": "",
        ".": ","
    }
});

jQuery(function ($) {
    initAccessories()

    function sidebarResize(){
        if(!$('.sidebar.left').size()) return

        var height = $('.sidebar.left').next().height()
        if(height > $('.sidebar.left').height()){
          // $('.sidebar.left').removeAttr("style")
            $('.sidebar.left').height(height)
        }
    }


    $(window).on('DOMSubtreeModified',sidebarResize)
    sidebarResize()



    function openFancybox(){
        $.fancybox.open({
            type: 'iframe',
            "fitToView" : true,
            "href" : "//www.youtube.com/embed/wRrCc0grv5Q?rel=0&autoplay=1&wmode=transparent",
            "openEffect" : "elastic",
            "closeEffect" : "elastic",
            "padding" : ["12px","12px","12px","12px"]
        }).click()
        return false
    }
    $("#introduce.video").each(function(){
        $wrapper = $(this)
        $navbar = $wrapper.find("#navbar")
        $buttons = $wrapper.find(".buttons")
        $a = $wrapper.find("a")

        $navbar.click(function(){
            return false
        })
        $buttons.click(function(){
            return false
        })

        $wrapper.click(function(){
            openFancybox()
        })

        $a.click(function(){
            if($(this).hasClass("icon-play")) {
                openFancybox()
            }else{
                window.location.href = $(this).attr("href")
            }
            return false
        })
    })

    $("#video-link").click(openFancybox)
})