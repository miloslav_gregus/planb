jQuery(function ($) {

    var $auctionInvestments = $("#auction-investments")
    if ($auctionInvestments.size()) {

        $auctionInvestments.on("click", ".pagination a", function () {
            var action = $(this).attr("href")
            var data = {
                type: "investments",
                action: action
            }
            History.pushState(data, document.title, action)
            return false
        })

        $auctionInvestments.on("refresh", function () {
            var $wrapper = $(this)
            var action = $wrapper.data("url")

            var data = {
                type: "investments",
                action: action
            }
            History.pushState(data, document.title, action)
            window.onstatechange()
            return false
        })


        $auctionInvestments.on("click", ".remove-investment", function () {
            var $wrapper = $(this)
            var message = $wrapper.data("message")
            var $tr = $wrapper.closest("tr")
            var href = $wrapper.attr("href")

            if (!confirm(message)) return false

            $auctionInvestments.fadeTo(null, 0.3)
            $auctionInvestments.spin()
            $.post(href, function (result) {

                $tr.fadeOut(null, function () {
                    $tr.remove()
                })
                $auctionInvestments.fadeTo(null, 1)
                $auctionInvestments.spin(false)

                $(".auction-detail table.detail").replaceWith(result.template)
                initAccessories()
            })
            return false
        })

        $auctionInvestments.on("click", ".limbo-investment", function () {
            var $wrapper = $(this)
            var $spinHolder = $(this).closest(".spin-holder")
            var $tr = $wrapper.closest("tr")
            var href = $wrapper.attr("href")

            $spinHolder.fadeTo(null, 0.3)
            $spinHolder.spin()
            $.post(href, function (result) {

                $tr.toggleClass("limbo")
                $wrapper.find("*").toggleClass("icon-checkbox-unchecked").toggleClass("icon-checkbox-checked")

                $spinHolder.fadeTo(null, 1)
                $spinHolder.spin(false)

                $("#loan-summary table").replaceWith(result.template)

                $auctionInvestments.find("tfoot").replaceWith(result.tfoot)

                $(".form-notice").slideUp();

                var calculations = result.calculations
                $("#auction-stop").attr("disabled", "disabled")
                if(calculations.percentage > 1){
                    $(".form-notice.too-many").stop().slideDown()
                }else if (calculations.percentage <= 0){
                    $(".form-notice.nothing-selected").stop().slideDown()
                }else {
                    $(".form-notice.make-sure").stop().slideDown()
                    $("#auction-stop").removeAttr("disabled")
                }
                $repaymentCalendar.trigger("calculate")
            })
            return false
        })
    }


    var $auctionInvestmentsAdd = $("#auction-add-investment")
    if ($auctionInvestmentsAdd.size()) {

        $auctionInvestmentsAdd.on("init",function () {

            var $amount = $("#form_investment_add_amount")
            var $interest = $("#form_investment_add_interest")
            var $profit = $("#auction-add-investment .profit .price")
            var maturity = $("#form_investment_add_maturity").val()

            function recalculate(){
                try {
                    var amount = $amount.spinner( "value" );
                    var months = parseInt(maturity)
                    var interest = $interest.spinner( "value" );
                } catch (err) {
                    return
                }

                var monthlyInterest = calculations.monthlyInterest(interest)
                var monthlyRepayment = calculations.pmt(monthlyInterest, months, amount)
                var repayment =  monthlyRepayment * months;
                var profit = repayment - amount;

                $profit.text(round2dec(profit) + " €")
            }


            $amount.spinner({
                step: 5,
                min: $amount.data("min"),
                max: $amount.data("max"),
                create: recalculate,
                spin: recalculate,
                change: recalculate
            })
            $interest.spinner({
                step: 0.2,
                min: $interest.data("min"),
                max: $interest.data("max"),
                numberFormat: "n",
                create: recalculate,
                spin: recalculate,
                change: recalculate
            })

            var $form = $auctionInvestmentsAdd.find("form")
            $form.submit(function () {
                var action = $form.attr("action")
                var data = $form.serialize()

                $auctionInvestmentsAdd.fadeTo(null, 0.3)
                $auctionInvestmentsAdd.spin()
                $.post(action, data, function (result) {
                    $auctionInvestmentsAdd.spin(false)
                    $auctionInvestmentsAdd.html(result)
                    $auctionInvestmentsAdd.stop().fadeTo(null, 1)
                    $auctionInvestmentsAdd.trigger("init")

                    if (!$auctionInvestmentsAdd.find("form").size()) {
                        $auctionInvestments.trigger("refresh")

                        var $detail = $auctionInvestmentsAdd.find("table.detail")
                        $(".auction-detail table.detail").replaceWith($detail)
                        initAccessories()
                    }
                })

                return false
            })



            $("#form_investment_add_validationCode").val("")

        }).trigger("init")

    }

    var $repaymentCalendar = $("#repayment-calendar")
    if ($repaymentCalendar.size()) {

        $repaymentCalendar.on("calculate",function () {
            var url = $repaymentCalendar.data("url")
            var data = $("#form-auction-stop").serialize()

            $repaymentCalendar.spin()
            $repaymentCalendar.fadeTo(null, .3)
            $.post(url, data, function (result) {
                $repaymentCalendar.html(result)
                $repaymentCalendar.spin(false)
                $repaymentCalendar.fadeTo(null, 1)
            })
        }).trigger("calculate")

        $("#form_auction_stop_loanStartAt, #form_auction_stop_repaymentDay").change(function(){
            $repaymentCalendar.trigger("calculate")
        })
    }


        $("#auction-cancel-btn").click(function(){
        var message = $(this).data("message")

        if(confirm(message)) {
            return true
        }
        return false
    })




    var $watchlistUser = $("#watchlist-user")
    if ($watchlistUser.size()) {
        $watchlistUser.on("click", ".remove-watchlist", function () {
            var $this = $(this)
            var $tr = $this.closest("tr")
            var href = $this.attr("href")
            var message = $this.data("message")

            if (!confirm(message)) return false

            var opts = {
                lines: 9, // The number of lines to draw
                length: 8, // The length of each line
                width: 4, // The line thickness
                radius: 2, // The radius of the inner circle
                color: "#000",
                top: 20
            };

            $tr.fadeTo(null, .3)
            $this.spin(opts)
            $.post(href, function (result) {
                $this.spin(false)
                $tr.fadeOut(null, function(){
                    $tr.remove()
                })
            })
            return false;
        })

        $watchlistUser.find(".notifications-change").on("click", function(){
            var $this = $(this)
            var href = $this.attr("href")
            var $p = $this.parent()

            var opts = {
                lines: 10, // The number of lines to draw
                length: 10, // The length of each line
                width: 4, // The line thickness
                radius: 7, // The radius of the inner circle
                color: "#000",
                top: 20
            };

            $p.fadeTo(null, .3)
            $p.spin(opts)

            $.post(href, function (result) {
                $p.spin(false)
                $this.toggleClass("icon-checkbox-checked").toggleClass("icon-checkbox-unchecked")
                $p.fadeTo(null, 1)
            })
            return false;
        })
    }



    var $watchdogUser = $("#watchdog-user")
    if ($watchdogUser.size()) {
        $watchdogUser.on("click", ".remove-watchdog", function () {
            var $this = $(this)
            var $tr = $this.closest("tr")
            var href = $this.attr("href")
            var message = $this.data("message")

            if (!confirm(message)) return false

            var opts = {
                lines: 9, // The number of lines to draw
                length: 8, // The length of each line
                width: 4, // The line thickness
                radius: 2, // The radius of the inner circle
                color: "#000",
                top: 20
            };

            $tr.fadeTo(null, .3)
            $this.spin(opts)
            $.post(href, function (result) {
                $this.spin(false)

                if(result.status){
                    $tr.fadeOut(null, function(){
                        $tr.remove()
                    })
                }else {
                    alert("Hups, chyba!")
                    $tr.fadeTo(null, 1)
                }
            })
            return false;
        })

        $watchdogUser.find(".notifications-change").on("click", function(){
            var $this = $(this)
            var href = $this.attr("href")
            var $p = $this.parent()

            var opts = {
                lines: 10, // The number of lines to draw
                length: 10, // The length of each line
                width: 4, // The line thickness
                radius: 7, // The radius of the inner circle
                color: "#000",
                top: 20
            };

            $p.fadeTo(null, .3)
            $p.spin(opts)

            $.post(href, function (result) {
                $p.spin(false)
                $this.toggleClass("icon-checkbox-checked").toggleClass("icon-checkbox-unchecked")
                $p.fadeTo(null, 1)
            })
            return false;
        })
    }

})