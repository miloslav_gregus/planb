jQuery(function ($) {

    if($("#comments").size()){
        var $wrapper = $("#comments")

        $wrapper.on("click", ".btn-answer", function(){
            var $this = $(this)
            var $comment = $this.closest(".comment")
            var href = $(this).attr("href")

            $comment.fadeTo(null, 0.3)
            $comment.spin()
            $.get(href, function(result){
                $comment.html(result)
                $comment.stop().fadeTo(null, 1)
                $comment.spin(false)
                $comment.find("textarea").focus()
            })

            return false
        })

        $wrapper.on("click", ".btn-question", function(){
            var $this = $(this)
            var $comment = $this.closest(".comment")
            var href = $(this).attr("href")

            $comment.fadeTo(null, 0.3)
            $comment.spin()
            $.get(href, function(result){
                $this.remove()
                $wrapper.append(result)
                $comment.stop().fadeTo(null, 1)
                $comment.spin(false)
                $wrapper.find("textarea").focus()
            })

            return false
        })

        $wrapper.on("submit", "form", function(){
            var $this = $(this)
            var $comment = $this.closest(".comment")
            var $form = $this.closest("form")
            var action = $form.attr("action")
            var data = $form.serialize()

            $comment.fadeTo(null, 0.3)
            $comment.spin()
            $.post(action, data, function(result){
                $(".alert-box.no-comments").fadeOut()
                $comment.stop().fadeTo(null, 1)
                $comment.spin(false)
                $comment.html(result)
            })

            return false
        })
    }

})