jQuery(function ($) {
    var $riskAuctions = $("#risk-auctions")


    $('#filter .slider').each(function () {

        var $slider = $(this)
        var $sliderWrap = $slider.closest('.slider-wrap')
        var valueMin = $sliderWrap.find('input.min-value').val()
        var valueMax = $sliderWrap.find('input.max-value').val()
        var min = $slider.data('min')
        var max = $slider.data('max')


        var func = function (event) {

            var eventType = typeof event != "undefined" ? event.type : "call";

            setTimeout(function () {
                var value = $slider.slider('value')
                $slider.closest('.loan-height').find('.value').text(value)

                valueMin = $slider.slider("values", 0)
                $sliderWrap.find(".min-value").val(valueMin).text(valueMin)
                valueMax = $slider.slider("values", 1)
                $sliderWrap.find(".max-value").val(valueMax).text(valueMax)

                checkRiskAuctions()
                if (eventType == "slide" || eventType == "slidestop") resetWatchdog()

            }, 10)
        }


        $slider.slider({
            range: true,
            min: min,
            max: max,
            values: [valueMin, valueMax],
            animate: true,
            slide: func,
            create: func,
            stop: func
        })
    })


    function resetWatchdog() {
        $("#auction-watchdog").select2("val", 0).trigger("change")
    }

    function checkRiskAuctions() {
        if (!$riskAuctions.size()) return

        var values = $("#interest-average .slider").slider("values");
        if (values[0] >= 20) {
            $riskAuctions.addClass("active")
        } else {
            $riskAuctions.removeClass("active")
        }
    }

    checkRiskAuctions()

    $riskAuctions.click(function () {
        var $slider = $("#interest-average");

        $slider.find(".slider").slider("values", [20, 30]);
        $slider.find(".min-value").text(20).val(20)
        $slider.find(".maz-value").text(30).val(20)
        $(this).addClass("active")

        setTimeout(function () {
            $('#filter.list form').submit()
        }, 100)

        return false
    })

    var $auctionStatus = $("#auction-status")
    $auctionStatus.on("change",function () {
        var $select = $auctionStatus.find("select")
        var $endDays = $("#auction-days-end")
        var val = $select.val()

        if (val == "active") {
            $endDays.fadeTo(null, 1)
        } else {
            $endDays.fadeTo(null, .3)
        }

    }).trigger("change")

    var $watchdog = $("#auction-watchdog")
    $watchdog.change(function () {
        var $this = $(this)
        var val = $this.val()

        if (val == 0) return

        var href = $this.find("option[value=" + val + "]").data("href")

        $.post(href, function (result) {
            if (!result.status) return

            var filter = result.filter

            $("#filter .maturity input:checked").removeAttr("checked")
            for (i in filter.maturity) {
                $("[for=maturity" + filter.maturity[i] + "]").click()
            }


            $("#filter .ratings input:checked").removeAttr("checked")
            for (i in filter.rating) {
                $("[for=filter-rating-" + filter.rating[i] + "]").click()
            }

            var $interestAverage = $("#interest-average").find(".slider")
            $interestAverage.slider("values", [filter.interestAverageMin, filter.interestAverageMax])
            $interestAverage.slider("option", 'slide').call($interestAverage)

            var $collectedPercentage = $("#collected-percentage").find(".slider")
            $collectedPercentage.slider("values", [filter.collectedPercentageMin, filter.collectedPercentageMax])
            $collectedPercentage.slider("option", 'slide').call($collectedPercentage)

            var $auctionAmount = $("#auction-amount").find(".slider")
            $auctionAmount.slider("values", [filter.amountMin, filter.amountMax])
            $auctionAmount.slider("option", 'slide').call($auctionAmount)

            var $auctionDaysEnd = $("#auction-days-end").find(".slider")
            $auctionDaysEnd.slider("values", [filter.daysEndMin, filter.daysEndMax])
            $auctionDaysEnd.slider("option", 'slide').call($auctionDaysEnd)

            $("#auction-status select").select2("val", "active")

            setTimeout(function () {
                $form.trigger("submit")
            }, 20)

        })
        return false
    })

    var $form = $('#filter.list form');
    $form.submit(function () {

        var $form = $(this)
        var action = $form.attr("action") + "?" + $form.serialize()

        var data = {
            type: "filter",
            action: action
        }
        History.pushState(data, document.title, action);
        return false
    })

    $("#filter-result").on("click", ".pagination a", function () {
        var action = $(this).attr("href")
        var data = {
            type: "filter",
            action: action
        }
        History.pushState(data, document.title, action);
        return false
    })
})