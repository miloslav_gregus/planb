

window.onstatechange = function () {
    var data = History.getState().data

    if (data.type == 'filter') {

        var $result = $("#filter-result")
        var $spinner = $("#filter-result-spinner")

        var top = $("#filter-result").offset().top + 60
        $('html, body').stop().animate({scrollTop: top}, 500)

        $result.fadeTo(null, 0.3)
        $spinner.spin()
        $.get(data.action, function(result){
            $spinner.spin(false)
            $result.html(result)
            $result.stop().fadeTo(null, 1)
            initAccessories()
        })
    }

    if (data.type == 'investments') {

        var $result = $("#auction-investments")
        var $spinner = $result

        var top = $result.offset().top
        $('html, body').stop().animate({scrollTop: top}, 500)

        $result.fadeTo(null, 0.3)
        $spinner.spin()
        $.get(data.action, function(result){
            $spinner.spin(false)
            $result.html(result)
            $result.stop().fadeTo(null, 1)
            initAccessories()
        })
    }

}
