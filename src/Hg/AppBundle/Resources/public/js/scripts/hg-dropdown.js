jQuery(function($){
    $('.hg-dropdown').each(function(){

        var $dropdown = $(this)
        var $link = $dropdown.find('> .hg-dropdown-link')
        var $menu = $dropdown.find('> .hg-dropdown-menu')

        $link.on('click', function(){
            $menu.slideToggle()
            setTimeout(function(){
                $dropdown.toggleClass('open')
            }, 300)
            return false
        })
    })

})