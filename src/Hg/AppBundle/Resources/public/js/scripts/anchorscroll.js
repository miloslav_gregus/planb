(function ($) {
    var jump = function (e) {
        if (e) {
            e.preventDefault();
            var target = $(this).attr("href");
        } else {
            var target = location.hash;
        }

        /* fixy */
        if(target == "#") return
        if($(this).closest(".accordion").size()) return

        $('html,body').animate(
            {
                scrollTop: $(target).offset().top
            }, 1000, function () {
                location.hash = target;
            });

    }

    $('html, body').hide()

    $(document).ready(function () {
        $('a[href^=#]').bind("click", jump);

        if (location.hash) {
            $('html, body').scrollTop(0).show()
            setTimeout(function(){


            var target = location.hash;
                location.hash = "";
            $('html,body').animate(
                {
                    scrollTop: $(target).offset().top
                }, 1000, function () {
                    location.hash = target;
                });
            }, 200)
        } else {
            $('html, body').show()
        }
    });

})(jQuery);