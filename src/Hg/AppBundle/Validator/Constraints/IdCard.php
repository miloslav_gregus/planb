<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 30.10.2013
 * Time: 1:24
 */

namespace Hg\AppBundle\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IdCard extends Constraint {

    public $messageUnique = 'ID Card is already used';


    public function validatedBy()
    {
        return 'id_card';
    }

} 