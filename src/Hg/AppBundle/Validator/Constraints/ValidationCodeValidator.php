<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 30.10.2013
 * Time: 1:24
 */

namespace Hg\AppBundle\Validator\Constraints;
use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class ValidationCodeValidator extends ConstraintValidator {

    public function validate($value, Constraint $constraint)
    {
        /** @var GridCardHandler $gridCardHandler */
        $gridCardHandler = $constraint->gridCardHandler;

        if(!$gridCardHandler->checkValue($value)) {
            $this->context->addViolation($constraint->message);
        }
    }
} 