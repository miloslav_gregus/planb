<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 30.10.2013
 * Time: 1:24
 */

namespace Hg\AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class PersonalIdValidator extends ConstraintValidator
{

    protected $em;
    protected $securityContext;

    public function __construct(EntityManager $em, SecurityContext $securityContext)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
    }

    public function validate($value, Constraint $constraint)
    {

        if (!preg_match('#^\s*(\d\d)(\d\d)(\d\d)(\d\d\d)(\d?)\s*$#', $value, $matches)) {
            $this->context->addViolation($constraint->message);
            return FALSE;
        }

        list(, $year, $month, $day, $ext, $c) = $matches;

        // do roku 1954 přidělovaná devítimístná RČ nelze ověřit
        if ($c === '') {
            return $year < 54;
        }

        // kontrolní číslice
        $mod = ($year . $month . $day . $ext) % 11;
        if ($mod === 10) $mod = 0;
        if ($mod !== (int)$c) {
            $this->context->addViolation($constraint->message);
            return FALSE;
        }

        // kontrola data
        $year += $year < 54 ? 2000 : 1900;

        // k měsíci může být připočteno 20, 50 nebo 70
        if ($month > 70 && $year > 2003) $month -= 70;
        elseif ($month > 50) $month -= 50;
        elseif ($month > 20 && $year > 2003) $month -= 20;

        if (!checkdate($month, $day, $year)) {
            $this->context->addViolation($constraint->message);
        }

        /** @var User $user */
        $user = $this->securityContext->getToken()->getUser();


        $user->getType();
        $profiles = $this->em->getRepository("AppBundle:Profile")->findBy(array(
            "personalId" => $value,
        ));


        foreach ($profiles AS $profile) {
            if ($profile->getUser()->getType() == $user->getType() &&
                $profile->getUser() != $user
            ) {
                $this->context->addViolation($constraint->messageUnique);
            }
        }

    }
} 