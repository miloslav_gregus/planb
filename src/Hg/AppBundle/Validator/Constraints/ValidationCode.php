<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 30.10.2013
 * Time: 1:24
 */

namespace Hg\AppBundle\Validator\Constraints;

use Hg\Services\Planb\GridCardHandler;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidationCode extends Constraint {

    public $message = 'Validation code is not valid, please try again.';
    public $gridCardHandler;

    public function __construct($options, GridCardHandler $gridCardHandler) {
        parent::__construct($options);

        $this->gridCardHandler = $gridCardHandler;
    }
} 