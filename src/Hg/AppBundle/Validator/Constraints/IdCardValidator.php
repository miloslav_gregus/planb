<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 30.10.2013
 * Time: 1:24
 */

namespace Hg\AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Hg\AppBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class IdCardValidator extends ConstraintValidator
{

    protected $em;
    protected $securityContext;

    public function __construct(EntityManager $em, SecurityContext $securityContext)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
    }

    public function validate($value, Constraint $constraint)
    {

        /** @var User $user */
        $user = $this->securityContext->getToken()->getUser();

        $user->getType();
        $profiles = $this->em->getRepository("AppBundle:Profile")->findBy(array(
            "idCard" => $value,
        ));

        foreach ($profiles AS $profile) {
            if ($profile->getUser()->getType() == $user->getType() &&
                $profile->getUser() != $user
            ) {
                $this->context->addViolation($constraint->messageUnique);
            }
        }

    }
} 