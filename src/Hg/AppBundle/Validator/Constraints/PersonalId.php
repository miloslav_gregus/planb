<?php
/**
 * Created by PhpStorm.
 * User: PayteR
 * Date: 30.10.2013
 * Time: 1:24
 */

namespace Hg\AppBundle\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PersonalId extends Constraint {

    public $message = 'Nonvalid Personal ID number';
    public $messageUnique = 'Personal ID is already used';


    public function validatedBy()
    {
        return 'personal_id';
    }

} 