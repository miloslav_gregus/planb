<?php


$bundles = __DIR__ . "/bundles/";
$src = __DIR__ . "/../src/";
$vendor = __DIR__ . "/../vendor/";

$link = $bundles . 'app';
$target = $src. 'Hg/AppBundle/Resources/public';
@unlink($link);
symlink($target, $link);

$link = $bundles . 'admin';
$target = $src. 'Hg/AdminBundle/Resources/public';
@unlink($link);
symlink($target, $link);

//$link = $bundles . 'fosjsrouting';
//$target = $vendor. 'friendsofsymfony/jsrouting-bundle/FOS/JsRoutingBundle/Resources/public';
//@unlink($link);
//symlink($target, $link);

$link = $bundles . 'framework';
$target = $vendor. 'symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/public';
@unlink($link);
symlink($target, $link);

$link = $bundles . 'mopabootstrap';
$target = $vendor. 'mopa/bootstrap-bundle/Mopa/Bundle/BootstrapBundle/Resources/public';
@unlink($link);
symlink($target, $link);

//$link = $bundles . 'mopabootstrapsandbox';
//$target = $vendor. 'mopa/bootstrap-sandbox-bundle/Mopa/Bundle/BootstrapSandboxBundle/Resources/public';
//@unlink($link);
//symlink($target, $link);

$link = $bundles . 'sensiodistribution';
$target = $vendor. 'sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/public';
@unlink($link);
symlink($target, $link);


$link = $bundles . 'sonataadmin';
$target = $vendor. 'sonata-project/admin-bundle/Resources/public';
@unlink($link);
symlink($target, $link);

$link = $bundles . 'sonatajquery';
$target = $vendor. 'sonata-project/jquery-bundle/Sonata/jQueryBundle/Resources/public';
@unlink($link);
symlink($target, $link);

echo "ok";