<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--Nečekuj nám tu kódy brácho, šťastie príde v nedeľu :)-->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>Plán B - Férové kolektívne pôžičky, od ľudí ľuďom</title>
    <link
        href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600&amp;subset=latin,latin-ext,cyrillic-ext'
        rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="styles.css" type="text/css" media="all"/>
</head>
<body>

<div id="wrapper">
    <div class="logo"><img src="/bundles/app/img/logo.png"></div>
    <h1>Plán B - Férové kolektívne pôžičky, od ľudí ľuďom</h1>

    <h2>Je nám ľúto, ale Váš prehliadač je zastaralý</h2>

    <p>Máte veľmi starý prehliadač, prosíme, nainštalujte si novšiu verziu prehliadača Internet Explorer, alebo si
        stiahnite iný prehliadač z ponuky nižšie, pre vstup na stránku Plánu B.</p>

    <p>
        <a target="_blank" href="http://www.google.com/intl/sk/chrome/" style="padding-right: 25px;"><img width="50" height="50" alt="chrome"
                                                                                   src="chrome.png" title="chrome"></a>
        <a target="_blank" href="http://www.mozilla.org/firefox/new/" style="padding-right: 25px;"><img width="50" height="50"
                                                                                        alt="firefox" src="firefox.png"
                                                                                        title="firefox"></a>
        <a target="_blank" href="http://windows.microsoft.com/sk-sk/internet-explorer/products/ie/home"
           style="padding-right: 25px;"><img width="50" height="50" alt="Internet Explorer" src="ie.png"
                                             title="Internet Explorer"></a>
        <a target="_blank" href="http://www.opera.com/download/" style="padding-right: 25px;"><img width="50" height="50" alt="opera"
                                                                                   src="opera.png" title="opera"></a>
        <a target="_blank" href="http://www.apple.com/safari/download/"><img width="50" height="50" alt="safari" src="safari.png"
                                                             title="safari"></a>
    </p>

    <h2>Chcete vidieť, čo je to Plán B?</h2>
    <p>Aj keď nemáte s terajším prehliadačom možnosť vstúpiť na stránku Plánu B, môžete si zatiaľ pozrieť aspoň video tu:</p>
    <iframe width="640" height="360" src="//www.youtube.com/embed/4Udk9D5FxJI?rel=0" frameborder="0" allowfullscreen></iframe>
</body>
</html>