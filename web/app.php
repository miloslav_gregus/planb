<?php


$ua = $_SERVER['HTTP_USER_AGENT'];
if (strpos($ua, 'MSIE') != false && strpos($ua, 'Opera') === false) {
    if (strpos($ua, 'Windows NT 5.2') != false) {
        if (strpos($ua, '.NET CLR') === false)
            return;
    }
    if (substr($ua, strpos($ua, 'MSIE') + 5, 1) < 9) {
        //var_dump($ua);exit;
        header('Location: /ie/index.php');
        exit;
    }
}

use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

$loader = require_once __DIR__ . '/../app/bootstrap.php.cache';

// Use APC for autoloading to improve performance.
// Change 'sf2' to a unique prefix in order to prevent cache key conflicts
// with other applications also using APC.
/*
$loader = new ApcClassLoader('sf2', $loader);
$loader->register(true);
*/

if (in_array(@$_SERVER['REMOTE_ADDR'], array(
    '::1',
    '127.0.0.1',
    '192.168.56.1',
//    '212.26.184.65', //milo
//    '88.80.226.29', //martin
//    '88.80.242.223', //miso
))
) {
    Debug::enable();

    require_once __DIR__ . '/../app/AppKernel.php';

    $kernel = new AppKernel('dev', true);
    $kernel->loadClassCache();
    $request = Request::createFromGlobals();
    $response = $kernel->handle($request);
    $response->send();
    $kernel->terminate($request, $response);

    exit;
}


require_once __DIR__ . '/../app/AppKernel.php';
//require_once __DIR__.'/../app/AppCache.php';

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
